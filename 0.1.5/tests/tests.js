var global = this,
    Y;

module('JEO Core');

test('JEO global has been exported and is working', function () {
    expect(5);
    ok(typeof JEO !== 'undefined', 'JEO is available');
    
    Y = JEO();
    ok(Y instanceof JEO, 'JEO() returns an instance of JEO');
    
    var Y2 = JEO({ someConfigOption: true });
    ok(Y2.env.config.someConfigOption, 'JEO({ someConfigOption: true }) returns an instance whose configuration has been extended properly');
    
    var Y3 = JEO();
    ok(!Y.env.config.someConfigOption && !Y3.env.config.someConfigOption, 'Other JEO instances are not affected by the custom configuration');
    ok(!JEO.env.config.someConfigOption, 'The global JEO configuration is not affected by the custom configuration');
});

test('env is available and is reflecting the traits of the current environment', function () {
    expect(4);
    ok(typeof JEO.env !== 'undefined', 'env is available');
    ok(JEO.env.doc === global.document, 'env.doc is the current document by default');
    ok(JEO.env.win === global, 'env.win is the global object by default');
    
    var supports = JEO.env.supports;
    //ok(supports.amd === false, 'env.supports.amd is true in case no `define` function is found on the global object');
    //ok(supports.correctEnumeration
    var strictModeSupported = (function () {'use strict'; return !this; }());
    ok(supports.strictMode === strictModeSupported, 'env.supports.strictMode is true if strict mode is supported by the current environment');
});

test('add and use are available and working', function () {
    expect(11);

    Y = JEO();
    ok(typeof JEO.add !== 'undefined', 'add is available');
    ok(typeof Y.use !== 'undefined', 'use is available');

    JEO.add('blubb', ['bla'], function (Y) {
        ok(Y.mix, 'anonymous add(function ...) imports default library');

        Y.mix({
            Blubb: function Blubb() {},
            plisch: 'platsch'
        });

        Y.BlubbManager = function BlubbManager() {
            
        };
    });

    JEO.add('dep0', [], function (Y) {
        Y.dep0 = true;
        ok(true, 'add("dep", [], function ...) works');
    });

    JEO.add('plisch', function (Y) {
        //Y.info('(no deps) plisch');
        ok(true, 'add("dep", function ...) works');
    });

    JEO.add(function (Y) {
        //Y.info('(anonymous, missing deps)');
        ok(Y.Array && Y.extend, 'anonymous add(function ...) imports default library');
    });

    JEO.add('staticDep', { staticContent: true });
    JEO.use(['staticDep'], function (Y) {
        ok(Y.staticContent, 'add("module", { staticContent: 1 }) works');
    });

    JEO.add([], function (Y) {
        //Y.info('(anonymous, no deps)');
        ok(true, 'anonymous add([], function ...) works without dependencies');
    });

    JEO.add(['dep1'], function (Y) {
        //Y.info('(anonymous, with dep)', Y.Array(arguments));
        ok(true, 'anonymous add(["dep"], function ...) works with dependencies');
    });

    JEO.add('bla', ['dep0', 'dep1'], function (Y) {
        //Y.log('bla is there! [dep0:', Y.dep0, ', dep1:', Y.dep1, ']', { imports: Y });
        ok(Y.dep0 && Y.dep1, 'add("module", ["dep0", "dep1"], function ...) works');
    });

    JEO.add('dep1', [], function (Y) {
        //Y.info('(dep1, missing deps)', Y.Array(arguments));
        Y.dep1 = true;
    });

    Y.use(['blubb'], function (Y) {
        ok(Y.Blubb && Y.plisch && Y.BlubbManager, 'use imports requested dependencies');
    });
    
    //global.Plisch = {};
    JEO.add('Plisch', function () {});
    var fulfilled = false,
        context;
    var result = Y.use(['Plisch'], function () {
        fulfilled = true;
        context = this;
    });
    ok(result === Y, 'use returns the current JEO instance');
    ok(fulfilled, 'use recognizes already defined modules');
    //ok(context === global, 'use\'s `this` is the current global object');
    delete global.Plisch;
    
    fulfilled = false;
    try {
        Y.use(['Blau'], function () {
            fulfilled = true;
        });
    }
    catch (e) {}
    finally {
        ok(!fulfilled, 'use doesn\'t recognize not available dependencies on the global object');
    }
});

asyncTest('Dynamic loading of dependencies is working', 2, function () {
    var loaded = false;
    var contentAvailable = false;
    
    JEO({ loader: { dynamic: true } }).use(['dynamic-dependency'], function (Y) {
        contentAvailable = Y.dynamicDependency;
        loaded = true;
    });
    setTimeout(function () {
        ok(loaded, 'dynamic dependency has been loaded');
        ok(contentAvailable, 'the dynamic dependency\'s content is available');
        start();
    }, 1000); // Timeout if not working
})

test('Conditional dependencies are working', function () {
    expect(2);
    
    JEO.add('polyfills', { polyfills: {} });
    
    JEO.add('can-do-more', ['env:xhr?:polyfills'], function (Y) {
        ok(!Y.polyfills, 'truthy dependency is added if condition is met');
    });
    JEO.add('needs-modern', ['env:someAwesomeFunctionality?:polyfills'], function (Y) {
        ok(Y.polyfills, 'falsy dependency is added if condition is not met');
    });
    
});

asyncTest('Signals can be added and act as virtual dependencies', function () {

    JEO.use(['jeo:someSignal'], function (Y) {
        ok(true, 'signals can act as dependencies');
    });
    
    JEO.add('jeo:someSignal', true);

    var loaded = false;
    JEO({ loader: { dynamic: true } }).use(['jeo:neverGonnaHappen'], function (Y) {
        loaded = true;
    });
    
    setTimeout(function () {
        ok(!loaded, 'signals don\'t invoke the dynamic loader if available');
        start();
    }, 1000);
});

module('Normalization');

test('console is available on the global object', function () {
    expect(1);
    ok(!!global.console, 'console is available');
});

/*test('Array.prototype.forEach is available and working', function () {
    expect(2);
    ok(typeof [].forEach !== 'undefined', 'Array.prototype.forEach available');
    
    var numbers = '';
    [1, 2, 3].forEach(function (value, key) {
        numbers += value + '@' + key + ',';
    });
    ok(numbers === '1@0,2@1,3@2,', 'Array.prototype.forEach executes the given function with proper arguments');
});

test('Array.isArray is available and working', function () {
    expect(3);
    ok(typeof Array.isArray !== 'undefined', 'isArray is available');
    ok(Array.isArray([]), 'isArray recognizes arrays');
    ok(!Array.isArray({}), 'isArray rejects objects');
});*/

test('Date.now is available and working', function () {
    expect(2);
    ok(typeof Date.now !== 'undefined', 'Date.now available');
    ok(typeof Date.now() === 'number', 'Date.now() returns a number');
});

test('Function.prototype.bind is available and working', function () {
    expect(2);
    var ctx = {};
    function f() {
        return this;
    }
    ok(typeof f.bind !== 'undefined', 'Function.prototype.bind available');
    ok(f.bind(ctx)() === ctx, 'The context of a bound function is preserved');
});

test('Object.create is available and working', function () {
    expect(3);
    ok(typeof Object.create !== 'undefined', 'Object.create available');
    
    var archetype = { blubb: 1 },
        delegated = Object.create(archetype);
    ok(typeof delegated.blubb !== 'undefined', 'Object.create({...}) creates an object that has the prototype chain setup properly');
    
    var a = { blubb: 1 },
        b = Object.create(a, {
            bla: {
                value: 'plisch'
            }
        });
    ok(b.bla === 'plisch', 'Object.create({...}, {...}) creates a delegated object with the properties given by the passed property descriptor');
});

test('Object.defineProperty is available and working', function () {
    expect(2);
    ok(typeof Object.defineProperty !== 'undefined', 'Object.defineProperty is available');
    
    var a = {};
    Object.defineProperty(a, 'blubb', {
        value: 'bla'
    });
    ok(a.blubb === 'bla', 'Object.defineProperty({...}, "propertyName", { value: ... }) defines a property named "propertyName" with the given value');
});

test('Object.defineProperties is available and working', function () {
    expect(2);
    ok(typeof Object.defineProperties !== 'undefined', 'Object.defineProperties is available');
    
    var a = {};
    Object.defineProperties(a, {
        blubb: {
            value: 'bla'
        },
        bla: {
            value: 'plisch'
        }
    });
    ok(a.blubb === 'bla' && a.bla === 'plisch', 'Object.defineProperties({...}, {...}) defines the specified properties on the given object');
});

test('Object.freeze is available and working', function () {
    expect(1);//2);
    ok(typeof Object.freeze !== 'undefined', 'Object.freeze is available');
    
    //var a = {};
    //Object.freeze(a);
    //a.blubb = 1;
    //ok(typeof a.blubb === 'undefined', 'Object.freeze actually freezes an object (impossible in ES3 environments)');
});

test('Object.keys is available and working', function () {
    expect(3);
    ok(typeof Object.keys !== 'undefined', 'Object.keys is available');
    
    function F() {}
    var proto = {
        propertyNotToBeListedByObjectKeys: 1
    };
       
    var o = new F;
    o.a = 'a';
    o.b = 'b';
    
    var wantedKeys = ['a', 'b'],
        isOk = true,
        actualKeys = Object.keys(o),
        i;
    
    for (i = 0; i < actualKeys.length; i++) {
        isOk = isOk && wantedKeys.indexOf(actualKeys[i]) >= 0;
    }
     
    ok(isOk, 'Object.keys enumerates all own keys of an object');

    ok(actualKeys.indexOf('propertyNotToBeListedByObjectKeys') < 0, 'Object.keys doesn\'t enumerate keys that belong to objects further down the prototype chain');
});

test('String.prototype.trim is available and working', function () {
    expect(2);
    ok(typeof ''.trim !== 'undefined', 'String.prototype.trim is available');
    
    ok('  abc ef      '.trim() === 'abc ef', 'String.prototype.trim actually trims trailing whitespace');
});

module('JEO Base');

test('Array is available and working', function () {
    expect(4);
    ok(typeof Y.Array !== 'undefined', 'Array is available');
    ok(Y.Array().length === 0, 'Array() returns an empty array');
    
    (function () {
        var args = Y.Array(arguments);
        ok({}.toString.call(args) === '[object Array]', 'Array(arguments) returns a real array');
        ok(args[0] === 'one' && args[1] === 'two', 'Array(arguments) returns an array containing the given arguments');
    }('one', 'two'));
});

test('Object is available and working', function () {
    expect(3);
    ok(typeof Y.Object !== 'undefined', 'Object is available');
    var proto = {
        onProto: true
    };
    var delegated = Y.Object(proto, {
        onDelegated: {
            enumerable: true,
            configurable: false,
            value: 'blubb'
        }
    });
    ok(delegated.onProto, 'Object(original) produces an object that has the original in it\'s prototype chain');
    ok(delegated.onDelegated === 'blubb', 'Object(o, { prop: {...} }) attaches "prop" to the newly created object');
});

/*asyncTest('WebSocket is available and working', 3, function () {
    //expect(2);
    
    var Y = JEO();
    ok(typeof Y.WebSocket !== 'undefined', 'WebSocket is available');
    
    var socket = new Y.WebSocket('ws://ohhhhmygoeeeeed.thisisnothing.serious');
    ok(typeof socket !== 'undefined', 'WebSocket doesn\'t raise an exception if requested to open a websocket that isn\'t available');
    
    ok(false, 'WebSocket is working');
    start();
    //try {
    //    var socket = new Y.WebSocket('ws://echo.websocket.org');
    //}
    //catch (e) {
    //    
    //}
});*/

asyncTest('ajax is available and working', 3, function () {
    //expect(2);
    var Y = JEO();
    
    ok(typeof Y.ajax !== 'undefined', 'ajax is available');
    
    var text = 'this.included = true;';
    var loadedText = '';
    var loaded = false;
    Y.ajax('include.js', function (response) {
        loadedText = response;
        
        //stop();
        //finish();
        loaded = true;
    });
    
    setTimeout(function () {
        ok(loaded, 'ajax request succeeded');
        ok(text === loadedText, 'ajax("some/url.txt", function (response) {...}) hands the contents of the given file to the callback (limited support for local files)');
        start();
    }, 1000);
});

test('clone is available and working', function () {
    expect(3);
    ok(Y.clone, 'clone is available');
    var original = {
            sub: {
                prop0: 'someProperty'
            },
            arr: ['blubb']
        },
        dolly = Y.clone(original);
    ok(original !== dolly, 'clone returns a fresh clone');
    ok(original.sub !== dolly.sub &&
        original.arr !== dolly.arr, 'clone returns a deep copy of the original object');
});

test('define is available and working', function () {
    expect(7);
    var toString = {}.toString;
    
    ok(typeof Y.define !== 'undefined', 'define is available');
    
    function InnerBla() {}
    var shouldBeBla = Y.define({
        constructor: InnerBla
    });
    
    ok(toString.call(shouldBeBla) === '[object Function]' && shouldBeBla === InnerBla, 'define({ constructor: ... }) returns the proper constructor function');
    
    function InnerBlubb() {}
        
    Y.define('Blubb', {
        constructor: InnerBlubb,
        extend: Array,
        prototype: {
            bla: 1
        },
        statics: {
            plisch: 1
        }
    });
    
    ok(typeof global.Blubb !== 'undefined', 'define provides functions that are direct children of the global object');
    ok(toString.call(global.Blubb) === '[object Function]' && global.Blubb === InnerBlubb, 'define("Blubb", { constructor: ... }) returns the proper constructor function');
    ok(new Blubb() instanceof Array, 'define("Blubb", { extend: ... }) produces constructor functions with the prototype chain properly set to the type to extend');
    ok(new Blubb().bla === 1, 'define("Blubb", { prototype: ... }) produces constructor functions with the additional prototype properties');
    ok(Blubb.plisch === 1, 'define("Blubb", { statics: ... }) produces constructor functions with the additional static properties');
    delete global.Blubb;
});

test('each is available and working', function () {
    expect(4);
    function F() {}
    F.prototype = {
        array: [1, 2, 3],
        object: {
            one: 'one',
            two: 'two'
        },
        simple: 'simple'
    };
    var o = new F;
    o.ownArray = [4, 5, 6];
    o.ownObject = { three: 'three', four: 'four' };
    o.ownSimple = 'ownSimple';
    
    var expectedKeys = ['ownArray', 'ownObject', 'ownSimple', 'array', 'object', 'simple'],
        actualKeys = [],
        isOk = true;
    Y.each(o, function (value, key) {
        //Y.log('each:', arguments);
        actualKeys.push(key);
        isOk = isOk && expectedKeys.indexOf(key) >= 0;
    });
    ok(isOk, 'each enumerates the expected properties of an object');
    ok(expectedKeys.length === actualKeys.length, 'each enumerates exactly the expected amount of properties of an object');
    
    var a = [1, 2, 3, 4],
        b = [];
    isOk = true;
    Y.each(a, function (value, key) {
        b.push(value);
        isOk = isOk && a.indexOf(value) >= 0;
    });
    ok(isOk, 'each enumerates the expected items of an array');
    ok(a.length === b.length, 'each enumerates exactly the expected amount of items of an array');
    
});

test('error is available and working', function () {
    expect(1);
    ok(typeof Y.error !== 'undefined', 'error is available');
});

test('extend is available and working', function () {
    expect(9);
    ok(typeof Y.extend !== 'undefined', 'extend is available');
    
    function F() {}
    F.prototype = {
        array: [1, 2, 3],
        object: {
            one: 'one',
            two: 'two'
        },
        simple: 'simple'
    };
    var o = new F;
    o.ownArray = [4, 5, 6];
    o.ownObject = { three: 'three', four: 'four' };
    o.ownSimple = 'ownSimple';
    
    Y.extend(o, {
        notDefinedSimple: 1
    });
    ok(o.notDefinedSimple === 1, 'extend({}, { notDefinedSimple: 1 }) copies simple properties that are undefined on the target');
    
    var undefinedObject = {};
    Y.extend(o, {
        notDefinedObject: undefinedObject
    });
    ok(typeof o.notDefinedObject !== 'undefined', 'extend({}, { notDefinedObject: {} }) creates an object of the same name on the target');
    ok(o.notDefinedObject !== undefinedObject, 'extend({}, { notDefinedObject: {} }) creates a new object instead of referencing the target of the source');
    
    undefinedObject.changed = 1; // Mutating original
    ok(typeof o.notDefinedObject.changed === 'undefined', 'extend({}, { notDefinedObject: {} }) creates a new object that is not affected by mutating the source object');
    
    var undefinedArray = [1];
    Y.extend(o, {
        notDefinedArray: undefinedArray
    });
    ok(typeof o.notDefinedArray !== 'undefined', 'extend({}, { notDefinedArray: {} }) creates an array of the same name on the target');
    notEqual(o.notDefinedArray, undefinedArray, 'extend({}, { notDefinedArray: {} }) creates a new array instead of referencing the target of the source');
    
    undefinedArray.push(2); // Mutating original
    ok(o.notDefinedArray.length === 1, 'extend({}, { notDefinedArray: {} }) creates a new array that is not affected by mutating the source array');
    //Y.log(undefinedArray, o.notDefinedArray, o);
    
    //ok(false, 'extend({}, { notDefinedArray: {} }) creates an array of the same name on the target');
    //ok(false, 'extend({}, { notDefinedArray: {} }) creates a new array instead of referencing the target of the source');
    //ok(false, 'extend({}, { notDefinedArray: {} }) creates a new array that is not affected by mutating the source array');

    var self = {
        extend: Y.extend
    };
    self.extend({ blubb: true });
    ok(self.blubb, 'extend({ ... }) extends it\'s host object');
});

test('gid is available and working', function () {
    expect(3);
    ok(typeof Y.gid !== 'undefined', 'gid is available');
    ok(typeof Y.gid() === 'string', 'gid returns a string');
    var hash = {},
        cnt = 100000,
        collisions = 0,
        id,
        i;
    for (i = 0; i < cnt; i++) {
        id = Y.gid();
        if (typeof hash[id] !== 'undefined') {
            collisions++;
            break;
        }
        hash[id] = id;
    }
    ok(collisions === 0, 'gid produces no colliding values in ' + cnt + ' iterations');
    delete hash;
});

asyncTest('include is available and working', function () {
    expect(4);
    var Y = JEO();
    ok(typeof Y.include !== 'undefined', 'include is available');
    
    var head = document.getElementsByTagName('head')[0];
    // FIXME: head.firstChild is implementation specific!
    
    Y.include('include.js');
    ok(head.firstChild.getAttribute('src') === 'include.js', 'include("file.js") successfully includes javascript files with the help of <script> nodes');
    
    Y.include('include.css');
    ok(head.firstChild.getAttribute('href') === 'include.css', 'include("file.css") successfully includes css files with the help of <link> nodes');
    
    raises(function () {
        Y.include('include.xxx');
    }, 'include("file.xxx") raises an exception when requested to include files of unknown type');
    //ok(false, 'include("file.xxx") raises an exception when requested to include files of unknown type');

    start();
});

test('info is available and working', function () {
    expect(1);
    ok(typeof Y.info !== 'undefined', 'info is available');
});

test('isArray is available and working', function () {
    expect(3);
    ok(typeof Y.isArray !== 'undefined', 'isArray is available');
    ok(Y.isArray([]), 'isArray([]) successfully recognizes plain arrays');
    //ok(!Y.isArray(arguments), 'isArray(arguments) rejects arguments as it is only array like');
    ok(!Y.isArray({}), 'isArray({}) rejects the object for it is no array');
    //?ok(!Y.isArray({ length: 0 }), 'isArray({ length: 0 }) rejects duck typed array like objects');
    //?ok(!Y.isArray(new NodeList()), 'isArray(new NodeList()) rejects a nodelist');
});

test('isArrayLike is available and working', function () {
    ok(typeof Y.isArrayLike !== 'undefined', 'isArrayLike is available');
    ok(Y.isArrayLike([]), 'isArrayLike([]) successfully recognizes plain arrays');
    //ok(Y.isArrayLike(arguments), 'isArrayLike(arguments) recognizes arguments');
    ok(!Y.isArrayLike({}), 'isArrayLike({}) rejects the object');
    //?ok(Y.isArrayLike({ length: 0 }), 'isArrayLike({ length: 0 }) recognizes duck typed array like objects');
    //?ok(!Y.isArray(new NodeList()), 'isArrayLike(new NodeList()) rejects a nodelist');
});

test('isFunction is available and working', function () {
    expect(6);
    ok(typeof Y.isFunction !== 'undefined', 'isFunction is available');
    ok(Y.isFunction(function () {}), 'isFunction(function () {}) recognizes functions');
    ok(Y.isFunction(Array), 'isFunction(Array) recognizes native constructor functions');
    ok(!Y.isFunction({}), 'isFunction({}) rejects objects');
    ok(!Y.isFunction([]), 'isFunction([]) rejects arrays');      
    ok(!Y.isFunction(1), 'isFunction(1) rejects simple objects');  
});

test('isString is available and working', function () {
    expect(7);
    ok(typeof Y.isString !== 'undefined', 'isString is available');
    ok(Y.isString('text'), 'isString("text") recognizes strings');
    ok(!Y.isString(function () {}), 'isString(function () {}) rejects functions');
    ok(!Y.isString(Array), 'isString(Array) rejects native constructor functions');
    ok(!Y.isString({}), 'isString({}) rejects objects');
    ok(!Y.isString([]), 'isString([]) rejects arrays');      
    ok(!Y.isString(1), 'isString(1) rejects simple objects');  
});

test('log is available and working', function () {
    expect(1);
    ok(typeof Y.log !== 'undefined', 'log is available');
});

test('merge is available and working', function () {
    expect(3);
    ok(typeof Y.merge !== 'undefined', 'merge is available');
    var merged = Y.merge([1, 2, 3], [4, 5, 6]),
        expectedArray = [1, 2, 3, 4, 5, 6],
        isOk = true;
    for (var i = 0; i < merged.length; i++) {
        isOk = isOk && expectedArray.indexOf(merged[i]) >= 0;
    }
    ok(isOk, 'merge actually merges all values of the arguments');
    ok(merged.length === expectedArray.length, 'merge creates an array containing the exact amount of items');
});

test('mix is available and working', function () {
    expect(5);
    ok(typeof Y.mix !== 'undefined', 'mix is available');
    
    function F() {}
    F.prototype = {
        array: [1, 2, 3],
        object: {
            one: 'one',
            two: 'two'
        },
        simple: 'simple'
    };
    var o = new F;
    o.mix = Y.mix;
    o.ownArray = [4, 5, 6];
    o.ownObject = { three: 'three', four: 'four' };
    o.ownSimple = 'ownSimple';
    
    var k0 = {};
    var k = Y.mix(k0, {});
    ok(k === k0, 'mix({}, {}) returns the first argument');
        
    Y.mix(k, o);
    deepEqual(k, o, 'mix({}, {}) actually includes all properties from the source (second argument), including those from the prototype chain');
        
    o.mix({ other: 'other' })
    ok(o.other === 'other', 'mix({}) augments it\'s context object in case the second argument is omitted');
    
    Y.mix(k, {
        array: [4, 5, 6]
    });
    ok(k.array !== o.array, 'mix({}, {}) actually overwrites properties of the target instead of augmenting it');
});

test('nop is available and does nothing', function () {
    expect(2);
    ok(typeof Y.nop !== 'undefined', 'nop is available');
    ok(!Y.nop(), 'nop returns nothing');
});

test('one is available and working', function () {
    expect(3);
    ok(typeof Y.one !== 'undefined', 'one is available');
    
    var div = document.createElement('div');
    div.setAttribute('id', 'blubb');
    var body = document.getElementsByTagName('body')[0];
    body.insertBefore(div, body.firstChild);
    ok(Y.one('#blubb') === div, 'one("#blubb") returns the node with the corresponding id');
    
    var html = document.getElementsByTagName('html')[0];
    ok(Y.one('html') === html, 'one("html") returns the first node with the specified tag name');
});

test('own is available and working', function () {
    expect(3);
    ok(typeof Y.own !== 'undefined', 'own is available');
    
    function F() {}
    F.prototype = {
        three: 'three' // prototype property
    };
    var expectedProps = ['one', 'two'],
        actualProps = [],
        o = new F;
    o.one = 'one';
    o.two = 'two';
    
    var isOk = true;
    Y.own(o, function (value, key) {
        actualProps.push(key);
        isOk = isOk && expectedProps.indexOf(key) >= 0;
    });
    ok(isOk, 'own enumerates the expected properties');
    ok(actualProps.length === expectedProps.length, 'own enumerates exactly the proper amount of properties');
});

test('provide is available and working', function () {
    expect(3);
    ok(typeof Y.provide !== 'undefined', 'provide is available');
    
    var innerBla = {};
    Y.provide('Bla', innerBla);
    ok(global.Bla === innerBla, 'provide("Bla", {}) places the given object on the global object');
        
    var subBlubb = {};
    Y.provide('Bla.Blubb', subBlubb);
    ok(global.Bla.Blubb === subBlubb, 'provide("Bla.Blubb") places the given object as a child of "Bla" on the global object');
        
    delete global.Bla;
});

test('raise is available and working', function () {
    expect(2);
    ok(typeof Y.raise !== 'undefined', 'raise is available');
    
    raises(function () {
        Y.raise('Some error');
    }, 'raise raises an exception');
});

test('typeOf is available and working', function () {
    expect(6);
    ok(typeof Y.typeOf !== 'undefined', 'typeOf is available');
    ok(Y.typeOf(1) === 'number', 'typeOf(1) returns "number"');
    ok(Y.typeOf('text') === 'string', 'typeOf("text") returns "string"');
    ok(Y.typeOf(/regexp/) === 'regexp', 'typeOf(/regexp/) returns "regexp"');
    ok(Y.typeOf({}) === 'object', 'typeOf({}) returns "object"');
    ok(Y.typeOf([]) === 'array', 'typeOf([]) returns "array"');
    //ok(Y.typeOf(arguments) === 'arguments', 'typeOf(arguments) returns "arguments"');
});

test('warn is available and working', function () {
    expect(1);
    ok(typeof Y.warn !== 'undefined', 'log is available');
});




























