/*!JEO.js - A lightweight default library for browsers (c)2012 Martin Feineis */
// Credits to jQuery, YUI, PrototypeJS, Closure Library and many more for 
// inspiration. The core library is optimized for source code optimizers like 
// YUICompressor by  providing shorthands for commonly used property names.
// See README.md and LICENSE.md for details and licensing.
(function (global, console, 
    Array, Date, Function, Object, RegExp, String, undefined) {
'use strict';
    
// Common property names will be munged
// throughout the build process

// TODO: Maybe we should encourage a one-global-only approach by
//       enabling the user to easily give the library a custom name
//       to be incorporated into the user library?
// PROS: * This is fresh functionality not provided by any library I know of
// CONS: * Module definitions would be different for every project
//         (is this really a problem?)
// var JEO = { <libName>: 'FunkySite' };
// ...
// console.log(typeof JEO); // -> 'undefined'
// FunkySite.add('module', ['dep0', 'dep1'], ...)
// FunkySite.use(['module'], function (M) { ... }
   
var LIBRARY_NAME = 'JEO',
    APPLY = 'apply',
    ARRAY = 'array',
    ASYNC = 'async',
    BIND = 'bind',
    CALL = 'call',
    CORE_CONFIG = LIBRARY_NAME,
    CONSTRUCTOR = 'constructor',
    CREATE = 'create',
    CREATE_ELEMENT = 'createElement',
    DEFER = 'defer',
    DEFINE_PROPERTIES = 'defineProperties',
    DEFINE_PROPERTY = 'defineProperty',
    ENV = 'env',
    EXTEND = 'extend',
    FALSE = !1,
    FIRST_CHILD = 'firstChild',
    FOR_EACH = 'forEach',
    FREEZE = 'freeze',
    GET_ATTRIBUTE = 'getAttribute',
    HAS = 'hasOwnProperty',
    INSERT_BEFORE = 'insertBefore',
    IS_ARRAY = 'isArray',
    KEYS = 'keys',
    LENGTH = 'length',
    NOW = 'now',
    NULL = null,
    OBJECT = 'object',
    ONE = 'one',
    PROTO = 'prototype',
    PUSH = 'push',
    REPLACE = 'replace',
    SET_ATTRIBUTE = 'setAttribute',
    TO_LOWER_CASE = 'toLowerCase',
    TO_STRING = 'toString',
    TRIM = 'trim',
    TRUE = !0,
    UNDEFINED = 'undefined';
    
var ArrayProto = Array[PROTO];

// Happy console world :)
console = console ? console : { log: nop, info: nop, error: nop, warn: nop };

// Thanks to PrototypeJS
var DONT_ENUMS = [TO_STRING, 'toLocaleString', 'valueOf', HAS, 
        'isPrototypeOf', 'propertyIsEnumerable', CONSTRUCTOR],
    
    // A global configuration for all instances can be supplied by
    // specifying an object with the name <CORE_CONFIG> on the global
    // object before including this library:
    // <script>var <CORE_CONFIG> = { loader: { dynamic: true } };</script>
    // <script src="path/to/this/library.js"></script>
    globalConfig = (CORE_CONFIG in global) ? global[CORE_CONFIG] : {},
    doc = global.document,
    env = {
        // Just for convenience; should not be necessary outside the 
        // core library itself; if so the functionality should most likely be 
        // added to the library base
        doc: doc, 
        // The version of the library being used
        jeo: '0.1.5',
        // The global context we're running in
        win: global,
        // In case we're inside a worker we provide a reference
        worker: (function () {
            var isWorker = 'importScripts' in global &&
                    'postMessage' in global;
            return isWorker ? global : NULL;
        }())
    },
    HEAD_ELEM,

    slice = [].slice, // Native Array.prototype.slice
    toString = {}[TO_STRING]; // Native Object.prototype.toString

// Feature detection for the current environment
var supports = env.supports = {
    // Some versions of JScript fail to enumerate over properties, names of 
    // which correspond to non-enumerable properties in the prototype chain
    correctEnumeration: (function(){
        for (var p in { toString: 1 }) {
            return TRUE;
        }
        return FALSE;
    }()),
    // Detects function decompilation capabilities
    functionDecompilation: /fun/.test(function () {}),
    // Signals if the current environment has native querySelector capabilities
    //querySelector: FALSE, // Delayed until DOM is ready see signal dom:ready
    // Detects whether or not strict mode is available
    strictMode: (function () {
        'use strict';
        return !this;
    }()),
    webWorker: 'Worker' in global,
    webSocket: (function () {
        // Checking for WebSocket implementations
        if ('WebSocket' in global) {
            return TRUE;
        }
        if ('MozWebSocket' in global) {
            return TRUE;
        }
    }()),
    xhr: TRUE // Delayed, see Xhr
};
// Detects ES5 defineProperty capabilities
supports[DEFINE_PROPERTY] = (function () {
    var o = {};
    try {
        Object[DEFINE_PROPERTY](o, ONE, {
            get: function () {
                return 1;
            }
        })
        return 1 === o[ONE];
    } catch(e) {}
}());

/** Environment normalization */

// Detects whether a the property `name` is native
// to the given object `o`; only works properly in
// environments that support function decompilation
// :: {} -> "" -> boolean
function isNative(o, name) {
    var ex = typeof o[name] !== UNDEFINED;
    return ex && isNative.fn(o[name]);
}
isNative.fn = supports.functionDecompilation
  // :: ({} -> {}) -> boolean
  ? function (fn) {
        return /[\s*native\s+code\s*]/i.test(fn);
    }
  // :: () -> boolean
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        // and hope for the best
        // TODO: Maybe we should rely on our implementation?
        return TRUE;
    };

// NOTE: Not shimming forEach on Array.prototype because it would
//       be confusing to have just this one shimmed and others like
//       every, some, etc. are still missing
// :: this[0] -> ({} -> "" -> [0] -> ()) -> {context}? -> ()
var arrayForEach = isNative(ArrayProto, FOR_EACH)
  ? ArrayProto[FOR_EACH]
  // :: ({} -> ""|number -> {}) -> {}? -> ()
  : function (fn/*, ctx*/) {
        var me = this,
            ctx = arguments[1] || me,
            len = me[LENGTH],
            i = 0;
        for (; i < len; i++) {
            //console.log('each:', i, this[i]);
            fn[CALL](ctx, me[i], i, me);
        }
    };

if (!isNative(Date, NOW)) {
    // :: () -> number
    Date[NOW] = function () {
        // Thanks to the Closure library
        // (unary+) calls getTime() on Date
        return +new Date; 
    };
}

if (!isNative(Function[PROTO], BIND)) {
    // :: {} -> function
    Function[PROTO][BIND] = function (ctx) {
        var args = slice[CALL](arguments, 1),
            fn = this;
        return function () {
            var as = merge(args, arguments);
            return fn.apply(ctx, as);
        };
    };
}

if (!isNative(Object, CREATE)) {
    // :: {} -> {}? -> {}
    Object[CREATE] = (function () {
        function F() {}
        return function (proto, props) {
            F[PROTO] = proto;
            var o = new F;
            if (props) {
                Object[DEFINE_PROPERTIES](o, props);
            }
            return o;
        };
    }());
}
var ocreate = Object[CREATE];

if (!supports[DEFINE_PROPERTY]) {
    // :: {} -> "" -> { value: {} } -> IO()
    Object[DEFINE_PROPERTY] = function (o, name, descr) {
        o[name] = descr.value;
    };
    // :: {} -> [{ value: {} }] -> IO()
    Object[DEFINE_PROPERTIES] = function (object, descriptors) {
        own(descriptors, function (descriptor, property) {
            defineProperty(object, property, descriptor);
        });
    };
}
var defineProperty = Object[DEFINE_PROPERTY];

if (!isNative(Object, FREEZE)) {
    // :: {} -> IO{}
    Object[FREEZE] = function (o) { return o; }; // Impossible in ES3
}
var ofreeze = Object[FREEZE];

if (!isNative(Object, KEYS)) {
    // :: {} -> [""]
    Object[KEYS] = function (o) {
        var result = [];
        own(o, function (_, name) {
            result[PUSH](name);
        });
        return result;
    };
}

if (!isNative(String[PROTO], TRIM)) {
    // :: () -> "" 
    String[PROTO][TRIM] = function () {
        return this[REPLACE](/^\s+|\s+$/, '');
    };
}

// :: "" -> (("", ...) -> ())
function wrapConsole(name) {
    var fn = console[name];
    return function () {
        try {
            fn[APPLY](console, arguments);
        }
        catch (e) {
            //fn(arguments.join(''));
        }
    };
}

// X-Browser XmlHttpRequest
// :: new () -> {XHR}
var Xhr = (function () {
    var Xhr,
        legacy = [
            'Msxml2.XMLHTTP.6.0',
            'Msxml2.XMLHTTP.3.0',
            'Msxml2.XMLHTTP',
            'Microsoft.XMLHTTP'
        ],
        len = legacy[LENGTH],
        i;

    // This is for all reasonable browsers
    if (XMLHttpRequest) {
        return XMLHttpRequest;
    } 

    // Testing for legacy http requests
    for (i = 0; i < len; i++) {
        Xhr = function () {
            var id = legacy[i];
            return new ActiveXObject(id);
        };
        try {
            new Xhr;
            return Xhr;
        } 
        catch (e) {} 
        finally {
            Xhr = NULL;
        }
    }
    // O.o - epic fail
    supports.xhr = FALSE;
    raise('XMLHttpRequest not available.');
}());

/** Base library */

// :: []|{ length: number } -> []
var YArray = function (arr) {
    return typeof arr !== UNDEFINED ? slice[CALL](arr) : [];
};

// :: {} -> {}? -> {}
var YObject = function (from, props) {
    return ocreate(from, props);
};

// :: { data?: {}, method?: "", sync: boolean, url: "" }
//      -> ("" -> ()) -> IO boolean
function ajax(config, ok) {
    //console.log('net.get', arguments);
    if (isString(config)) {
        config = { url: config };
    }
    var xhr = new Xhr,
        url = config.url,
        async = !config.sync,
        data = config.data || undefined,
        method = config.method || 'GET',
        params = [];

    own(config.params || {}, function (val, key) {
        //log('param', key, val);
        params[PUSH](escape(key) + '=' + escape(val));
    });
    if (params[LENGTH] > 0) {
        url = url + '?' + params.join('&');
    }
    xhr.onreadystatechange = function (e) {
        //console.log('  xhr.readystatechange', xhr, arguments);
        var status = xhr.status;
        if (xhr.readyState === 4) {
            // Local ok || net ok
            if (status === 0 || (status >= 200 && status < 300)) {
                ok(xhr.responseText);
            }
            else {
                raise('Request to "' + url + '" failed.');
            }
            xhr = NULL;
        }
    };
    try {
        //log(url);
        xhr.open(method.toUpperCase(), url, async);
        xhr.send(data);
        return TRUE;
    }
    catch (_) {}
}

// :: {} -> {}
function clone(o) {
    if (isFunction(o)) {
        ;;; warn('Cloning a function not supported.');
        return NULL;
    }
    return extend({}, o);
}

// :: ""? -> { 
//      constructor: new (...) -> {},
//      extend: function,
//      prototype: {},
//      statics: {}
// } -> IO function
function define(name, o) {
    if (!o) {
        o = name;
        name = '';
    }
    var Type = o[HAS](CONSTRUCTOR) 
            ? o[CONSTRUCTOR] 
            : function () {},
        Base = o[EXTEND] || Object,
        proto = o[PROTO] || {},
        statics = o.statics || {};

    Type[PROTO] = ocreate(Base[PROTO]);
    extend(Type[PROTO], proto);
    Type[PROTO][CONSTRUCTOR] = Type;
    extend(Type, Base);
    extend(Type, statics);

    if (name[LENGTH] > 0) {
        provide(name, Type);
    }
    return Type;
}

// {} -> ({} -> "" -> ()) -> {}? -> ()
function each(o, fn, ctx) {
    ctx = ctx || o;
    var i, j, propName, 
        justOwn = !!arguments[3]; // internal justOwn arg
    
    if (isArray(o) || isArrayLike(o)) {
        arrayForEach[CALL](o, fn, ctx);
    }
    else {
        for (i in o) {
            if (!justOwn || o[HAS](i)) {
                fn[CALL](ctx, o[i], i, o);
            }
        }
        if (!supports.correctEnumeration) {
            for (j = DONT_ENUMS[LENGTH]-1; j >= 0; j--) {
                propName = DONT_ENUMS[j];
                if (o[propName] && (!justOwn || o[HAS](j))) {
                    fn[CALL](ctx, o[propName], propName, o);
                }
            }
        }
    }
}

var error = wrapConsole('error');

// :: {0} -> {} -> IO{0}
function extend(to, from) {
    //if (arguments[LENGTH] < 2) {
    if (from === undefined) {
        from = to;
        to = this;
    }
    //var level = arguments[2] || 0;
    //var tab = '';
    //for (var j = 0; j < level; j++) {
    //    tab += '  ';
    //}
    each(from, function (val, i) {
        //console.log(tab, 'extend: ', i, ': ', val, '=>', to[i]);
        var deep = FALSE;
        switch (typeOf(val)) {
        case ARRAY:
            //console.log(tab, 'extend.array: ', i);
            if (!to[i]) {
                //console.log(tab, 'extend.array new: ', i, 
                //    ': ', val, '=>', to[i]);
                to[i] = [];
            }
            //else {
            //    console.log(tab, 'extend.array extend: ', i, 
            //        ': ', val, '=>', to[i]);
            //}
            deep = (val[LENGTH] > 0); // No magic, just in case ...
            break;
        case OBJECT:
            if (!to[i]) {
                to[i] = {};
            }
            deep = TRUE;
            break;
        }
        if (deep) {
            //console.log(tab, 'extend.deep: ', i, ': ', val, '=>', to[i]);
            extend(to[i], val);//, level+1);
        }
        else {
            to[i] = val;
        }
    });
    //console.log(tab, 'ok:', to);
    return to;
}

var gidSeed = 0,
    expando = LIBRARY_NAME + '?' + ((Math.random() + 1) << 30);
// :: "" -> IO""
function gid(prefix) {
    return expando + ':' + (prefix || 'gid') + (gidSeed++);
}

// :: { sync?: boolean, url: "" }|"" -> IO()
function include(opt) {
    if (isString(opt)) {
        opt = { url: opt };
    }
    if (!HEAD_ELEM) {
        HEAD_ELEM = one('head'); // Lazy initialization
    }
    var resType = 'js', // Defaulting to JavaScript files
        async = !opt.sync,
        url = opt.url,
        node;
    // Searching for actual file extension
    url[REPLACE](/\.([^?\/]+)(?:\?.*)?$/, function (m, ext) {
        resType = ext[TO_LOWER_CASE]();
    });
    switch (resType) {
    case 'js':
        node = doc[CREATE_ELEMENT]('script');
        if (async) {
            node[SET_ATTRIBUTE](ASYNC, ASYNC);
            node[SET_ATTRIBUTE](DEFER, DEFER);
        }
        node[SET_ATTRIBUTE]('src', url);
        
        //node.onload = node.onreadystatechange = function (e, aborted) {
        //    node.onload = node.onreadystatechange = null; // IE memory leaks
        //    console.log('included:', url);
        //};
        
        HEAD_ELEM[INSERT_BEFORE](node, HEAD_ELEM[FIRST_CHILD]);
        return;
    case 'css':
        node = doc[CREATE_ELEMENT]('link');
        node[SET_ATTRIBUTE]('rel', 'stylesheet');
        node[SET_ATTRIBUTE]('href', url);
        HEAD_ELEM[INSERT_BEFORE](node, HEAD_ELEM[FIRST_CHILD]);
        return;
    default:
        raise('Resource type "' + resType + '" not supported.');
        return;
    }
}

var info = wrapConsole('info');

// :: {} -> boolean
var isArray = isNative(Array, IS_ARRAY)
  ? Array[IS_ARRAY]
  : // :: {} -> boolean
    function (o) {
        return typeOf(o) === ARRAY;
    };

// :: {} -> boolean
function isArrayLike(o) {
    if (o && o[LENGTH] && o[PUSH]) {
        return TRUE;
    }
    switch (typeOf(o)) {
    case 'arguments': // Fall through
    case ARRAY: // Fall through
        return TRUE;
    }
}

// :: {} -> boolean
function isFunction(o) {
    // (typeof function () {}) is 'object' in some versions of WebKit
    return typeOf(o) === 'function';
}

// :: {} -> boolean
function isString(o) {
    return typeof o === 'string';
}

var log = wrapConsole('log');

// :: [] -> [] -> IO[]
function merge(as, bs) {
    var ls = slice[CALL](as);
    own(bs, function (b) {
        ls[PUSH](b);
    });
    return ls;
}

// :: {0} -> {} -> IO{0}
function mix(to, from) {
    if (!from) {
        from = to;
        to = this;
    }
    each(from, function (value, i) {
        to[i] = value;
    });
    return to;
}

// :: () -> ()
function nop() {}

// :: "" -> {dom}
function one(id) {
    switch (id[0]) {
    case '#':
        return doc.getElementById(id[REPLACE]('#', ''));
    default:
        return doc.getElementsByTagName(id)[0];
    }
}

// :: {} -> ({} -> ()) -> {}? -> ()
function own(o, fn, ctx) {
    return each[CALL](this, o, fn, ctx, 1);
}

// :: "" -> {0} -> IO{0}
function provide(name, o) {
    o = o || {};
    var host = global,
        ls = name.split('.'),
        len = ls[LENGTH],
        i;
    for (i = 0; i < len-1; i++) {
        host = host[ls[i]];
    }
    return (host[ls[i]] = o);
}

// :: "" -> IO()
function raise(message) {
    error[APPLY](console, arguments);
    throw message;
}

// :: {} -> IO""
var typeOf = (function () {
    var cache = {};   

    function extractType(t) {
        // Caching types
        var i = cache[t] = t[REPLACE](
            /\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1')[TO_LOWER_CASE]();
        return i;
    }
    
    own([{}, [], 1, '1', /1/, new Date, nop], function (o) {
        // Extracting type from "[object ...]"
        extractType(toString[CALL](o)); 
    });        
        
    return function typeOf(o) {
        if (o === NULL) {
            return 'null';
        }
        if (o === undefined) {
            return UNDEFINED;
        }
        var t = toString[CALL](o);
        return (t in cache)
            ? cache[t] 
            : extractType(t);
    };
}());

var warn = wrapConsole('warn');

/** Library core */

var DependencyManager = (function () {

/*
*  The DependencyManager for our modules
*/
// :: new ("" -> (() -> IO())) -> IO{}
function Dependency(id, ready) {
    var me = this;

    me.done = 0;
    me.deps = {};
    me.id = id;
    me.circularMap = {};

    // :: () -> IO()
    me.ready = function () {
        if (!me.done) {
            me.done = 1;
            ready();
        }
    };
}
Dependency.prototype = {
    // :: "" -> IO()
    addDependency: function (id) {
        this.deps[id] = id;
    },
    // :: "" -> IO()
    resolveDependency: function (id) {
        //console.log('    resolve:', module);
        var me = this,
            deps = me.deps,
            newDeps = {},
            len = deps.length,
            newCount = 0;

        own(deps, function (dep) {
            if (dep !== id) {
                newDeps[dep] = dep;
                newCount++;
            }
        });
        
        me.deps = newDeps;
        if (newCount < 1) { 
            me.ready();
        }
    },
    // :: () -> IO()
    tryResolveCycles: function () {
        var me = this,
            circular = me.circularMap;
        own(circular, function (_, j) {
            me.resolveDependency(j);
        });
    }
};

// :: new () -> IO{}
function DependencyManager() {
    var me = this;
    me.available = {}; // id -> boolean
    me.bottomUp = {}; // dependencies -> dependency
    me.topDown = {}; // id -> dependencies
}
DependencyManager.prototype = {
    // :: "" -> [""] -> (() -> IO()) -> ("" -> IO()) -> IO()
    add: function add(id, deps, ready, requestDependency) {
        requestDependency = requestDependency || nop;
        var me = this,
            isReady = FALSE;
        if (me.available[id]) {
            ready();
            return; // Early termination if already available
        }

        function resolved() {
            me.resolve(id);
        }

        // Before we get the whole machinary going we check
        // if it is really necessary ...
        if (deps.length < 1) {
            //log('   <no deps> ', id);
            // No dependencies, so we mark it as resolved
            ready(resolved);
            //me.resolve(id);
            return;
        }
        else {
            // Checking for already fullfilled dependencies
            isReady = TRUE;
            each(deps, function (dep) {
                isReady = isReady && me.available[dep];
            });
            if (isReady) {
                ready(resolved);
                return;
            }
        }
        var thisDep = new Dependency(id, ready),
            toRequest = [],
            bottomUp,
            cycle;
        
        // Building bottom up map (dependencies -> this)
        each(deps, function (dep) {
            thisDep.addDependency(dep);
            
            // NOTE: Moved the request to end of loop due to IE9
            //       loading local resources instantly which means
            //       that the requested resource arrived before this
            //       dependency mapping finished properly.
            //requestDependency(dep); 

            // Checking for freshly introduced
            // cyclic dependencies
            cycle = me.detectCycle(dep, id);
            if (cycle) {
                thisDep.circularMap[cycle.begin] = cycle.begin;
            }

            bottomUp = me.bottomUp[dep]; 
            if (!bottomUp) {
                bottomUp = (me.bottomUp[dep] = {});
            }
            bottomUp[id] = thisDep;
            
            requestDependency(dep);
        });

        // In case we introduced dependency cycles
        // we force to resolve them for being able
        // to proceed normally
        thisDep.tryResolveCycles();

        me.topDown[id] = thisDep;
    },
    // :: "" -> "" -> { begin: "", end: ""}
    detectCycle: function (dep, id) {
        var tree = this.topDown,
            root = dep;
        return (function detect(dep, id) {
            if (dep === id) {
                return { begin: dep, end: id }; // Just found a cycle
            }
            var elem = dep,
                deps,
                sub;

            while (1) {
                elem = tree[elem]
                if (!elem) {
                    break;
                }
                deps = elem.deps;
                if (deps[id]) {
                    ;;; Y.warn('Circular dependency between modules', root, 'and', id, 'detected');
                    // Cycle deeper in the tree
                    return { begin: root, end: id }; 
                }
                own(deps, function (_, sub) {
                    return detect(sub, id); // Dig down the tree
                });
            } 
        }(dep, id));
    },
    // :: "" -> boolean
    isTracked: function (id) {
        return this.available[id] || !!this.topDown[id];
    },
    // :: "" -> IO()
    resolve: function (id) {
        var me = this,
            bottomUp = me.bottomUp[id];
        
        if (bottomUp) {
            own(bottomUp, function (dep) {
                dep.resolveDependency(id);
            });
        }
        me.available[id] = me.topDown[id] || TRUE;
    }
};
return DependencyManager;

}());

// Including the default configuration
var defaultConfig = {
    //debug: false,
    loader: {
        baseUrl: '',//'./'
        dynamic: TRUE
        //sync: false
    }
};

// Extending the default configuration in case a global
// configuration has been supplied before the library
// has been loaded
env = extend(env, { config: extend(defaultConfig, globalConfig) });

// Dependency management ...
var depManager = new DependencyManager(),
    alreadyRequestedDependencies = {},
    repo = {};
        
// [""] -> [""]
function patchDependencies(deps) {
    var patched = [];
    //log('patchDeps:', YArray(deps));
    each(deps, function (dep) {
        var newDep = dep;
        dep.replace(/env\:([\w_$]+)\?([\w_$-]*)\:([\w_$-]*)/, 
            function (m, test, yes, no) {
                //log('conditional "', dep, '"');//, YArray(arguments));
                newDep = supports[test] ? yes : no;
                //log('  patched to "', newDep, '"', YArray(arguments));
            });
        // Patching a dependency to the empty string means 
        // it has been discarded
        if (newDep.length > 0) {
            patched.push(newDep);
        }
    });
    //log('patchedDeps:', patched);
    return patched;
}

// :: "" -> boolean
function testForSignal(id) {
    return /^\w+:/.test(id);
}

var core; // <- This default instance of the Core will be defined later

// :: "" -> [""] -> (({}, ...) -> ()) -> IO{JEO}
function add(id, deps, factory) {
    if (!isString(id)) {
        // Supporting anonymous definitions
        factory = deps;
        deps = id;
        id = '';
    }
    var isAnonymous = id.length < 1,
        instance = (this === Core)
            ? core // Core.add, Core.use are using the default instance
            : this; // Core().use uses the custom instance
    
    // Checking for already available modules
    if (!isAnonymous && (id in repo || depManager.isTracked(id))) {
        ;;; raise('Multiple definitions of module "' + id + '"; please check your dependencies.');
        return instance;
    }
    if (!isArray(deps)) {
        // add('module', function|{})
        factory = deps;
        deps = [];
    }
    if (!isAnonymous && deps.length < 1 && !isFunction(factory)) {
        // add('module1', { contents: {} })
        repo[id] = factory;
        depManager.resolve(id);
        return instance;
    }
    if (isAnonymous) {
        // We need a valid id so we just generate one since
        // coping with empty ids overly complicates things
        id = gid('anon');
    }
    if (!(isString(id) && isArray(deps) && isFunction(factory))) {
        // At this stage the arguments should be normalized
        raise('Error while adding module (', 
            id, ', ', deps, ', ', factory, ')');
    }
    
    // Any rewrites to be done on the dependencies go here;
    // the original purpose is to enable conditional loading
    deps = patchDependencies(deps);
    
    //log('JEO.add:', id, deps, factory);
    function ready(then) {
        //;;; log('  added', id, ':', Array(arguments));
        var imports = ocreate(instance),
            mod = {};

        // Importing the dependencies
        own(deps, function (dep) {
            //mix(imports, repo[dep]);
            //log('    <importing>', dep);  
            var subRepo = repo[dep];
            //for (var i in subRepo) {
            own(subRepo, function (val, key) {
                ;;; if (imports[key]) {
                ;;;     warn('multiple definitions of "', dep, '" in module "' + id + '"!');
                ;;; }
                imports[key] = val;
            });
        });
        
        // After adding dependencies we create a delegated
        // object for being able to tell imports from exports
        var exports = ocreate(imports);

        // Gathering the exports of this module
        // FIXME: Is it a good idea to hand in the global
        //        object as the context of the definition?
        factory[CALL](/*instance.env.win*/{}, exports);
            
        // Only tracking non-anonymous exports
        if (!isAnonymous) {
            //;;; log('JEO.add: returned =', returned, ', exports =', exports);
            own(exports, function (prop, name) {
                //;;; log('    ' + name + ' = ' + prop);
                mod[name] = prop;
            });

            repo[id] = mod;
            depManager.resolve(id);
        }
        
        // Running continuation if any
        if (isFunction(then)) {
            then();
        }
    }
    
    // The dependency manager now takes care of the request
    depManager.add(id, deps, ready, function (id) {
        // Enabling dynamic loading of dependencies ...
        // The loader respects the configuration of the current instance
        var config = instance.env.config.loader,
            isDynamic = config.dynamic,
            isSignal = testForSignal(id), // e.g.: 'dom:ready'
            isSync = config.sync,
            url = config.baseUrl + id + '.js';
        
        // In case dynamic loading is enabled and the
        // dependency has not been requested before
        // we attempt to load it if it is not a signal
        if (!isSignal && isDynamic && !alreadyRequestedDependencies[id]) {
            alreadyRequestedDependencies[id] = TRUE;
            //log('depManager.requestDependency:', YArray(arguments));
            include({ sync: isSync, url: url });
        }
    });
    return instance;
}
;;; add.dependencies = depManager;
;;; add.repo = repo;

// :: ["", ...]? -> ({JEO} -> {JEO}) -> IO{JEO}
function use(deps, factory) {
    //log('use:', YArray(arguments));
    return add[CALL](this, deps, factory);
}

// :: {config} -> IO{}
var Core = mix(function (config) {
    // The new instance inherits from the base library;
    // the global configuration will be extended using the
    // given custom one
    var instance = ocreate(base),
        baseConfig = clone(env.config),
        cfg = extend(baseConfig, config || {});
    instance[ENV] = ofreeze(mix(ocreate(env), { config: cfg }));
    return instance;
}, {
    add: add,
    // Including the default configuration with the
    // current environment
    env: env,//ofreeze(env),
    // :: ["", ...]? -> ({JEO} -> {JEO}) -> IO{JEO}
    use: function () {
        return use[APPLY](core, arguments);
    }
});

defineProperty(Core, TO_STRING, {
    configurable: FALSE,
    enumerable: FALSE,
    // :: () -> ""
    value: function () {
        return 'You are running ' + LIBRARY_NAME + ' v' + env.jeo;
    }
});

// Adding the base library that will always be included;
// including the Core.prototype in the prototype chain
// for being able to enable (base instanceof Core === true)
var base = ofreeze(mix(ocreate(Core[PROTO]), {
    Array: YArray,
    Object: YObject,
    ajax: ajax,
    clone: clone,
    define: define,
    each: each,
    error: error,
    extend: extend,
    gid: gid,
    include: include,
    info: info,
    isArray: isArray,
    isArrayLike: isArrayLike,
    isFunction: isFunction,
    isString: isString,
    log: log,
    merge: merge,
    mix: mix,
    nop: nop,
    one: one,
    own: own,
    provide: provide,
    raise: raise,
    typeOf: typeOf,
    use: use,
    warn: warn
}));

var core = Core(),
    DOM_READY = 'dom:ready'; // Default instance

// Just for convenience while debugging
;;; Core.add('jeo', { core: Core, base: base });

// Adding support tests as signals that can be required
each(supports, function (val, key) {
    if (val) {
        Core.add(ENV + ':' + key, val);
    }
});

// Adding support tests that are only safe after the DOM is ready
Core.use([DOM_READY], function () {
    var qs = 'querySelector',
        ok = supports[qs] = isNative(doc, qs + 'All');
    if (ok) {
        Core.add(ENV + ':' + qs, TRUE);
    }
});

//ofreeze(Core); // TODO: Really use the Core as a namespace?

// Defining a console; IE doesn't define it outside webdev mode
provide('console', console);
provide(LIBRARY_NAME, Core); // Exposing the core

// x-browser "DOMContentLoaded"
(function check() {
    // readyState === 'complete'
    if (doc && /^co/.test(doc.readyState)) {
        return Core.add(DOM_READY, TRUE);
    }
    setTimeout(check, 1);
}());

//var customName = env.config.aka;
//if (env.config.experimental && isString(customName)) {
//    provide(customName, Core); // Exposing the core
//}

// Handing in common language and host objects to easily enable minimization
}(this, 'console' in this ? console : null, 
    Array, Date, Function, Object, RegExp, String));

































