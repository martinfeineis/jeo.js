JEO.add('experimental/lambda', function (Y) {
    Y.info('lambda', arguments);

    var cache = {};
    Y.lambda = function (body) {
        var original = body,
            cached = cache[body];
        if (!Y.isString(body)) {
            return body;
        }
        if (cached) {
            return cached;
        }
        var vars = body.match(/\$[a-zA-Z_][\w_]*/g),
            names = {},
            args = [],
            s;
        Y.each(vars, function (x) {
            if (
                //!/typeof|true|false|string|number|boolean|function/g.test(x) && 
                //!/"\w+"/g.test(x) &&
                !names[x]) {
                names[x] = true;
                args.push(x);
            }
        });
        if (args.length < 1) {
            s = body.trim();
            if (/^\W/.test(s)) {
                // '+5'
                args.push('x');
                body = 'x' + s;
            }
            else if (/\W$/.test(s)) {
                args.push('x');
                body = s + 'x';
            }
        }
        //Y.log('args:', args, 'body:"', 'return ' + body + '"');
        // TODO: Check the body for security issues
        var fn = new Function(args.join(','), 'return ' + body);
        cache[original] = fn;
        Y.log('lambda: (', args, ') -> { return ', body, '}');
        return fn;
    };

});
