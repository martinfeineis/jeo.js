/*! JEO(experimental-workers) */
// * Core knows if it has been loaded within a WebWorker since v0.0.26
// * Maybe we add an API for easy parallelization of algorithms?

JEO.use(['env:webWorker'], function (Y) {
Y.info('working', Y.Array(arguments));

    var worker = new Worker('working-worker.js');
    worker.onmessage = function (ev) {
        Y.log('<-', ev.data);
    };
    worker.postMessage({ blubb: 'bla' });
});
