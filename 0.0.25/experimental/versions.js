//JEO.use(function (Y) {
//;;; Y.info('versions', Y.Array(arguments));
//    
//var CALL = 'call',
//    HAS = 'hasOwnProperty',
//    ocreate = Object.create,
//    Core = JEO,
//    log = Y.log,
//    own = Y.own;
//    
//var DependencyManager = (function () {
//
///*
//*  The DependencyManager for our modules
//*/
//// :: "" -> (() -> IO()) -> IO{}
//function Dependency(id, ready) {
//    var me = this;
//
//    me.done = 0;
//    me.deps = {};
//    me.id = id;
//    me.circularMap = {};
//
//    // :: () -> IO()
//    me.ready = function () {
//        if (!me.done) {
//            me.done = 1;
//            ready();
//        }
//    };
//}
//Dependency.prototype = {
//    // :: "" -> IO()
//    addDependency: function (id) {
//        this.deps[id] = id;
//    },
//    // :: "" -> IO()
//    resolveDependency: function (id) {
//        //console.log('    resolve:', module);
//        var me = this,
//            i,
//            dep,
//            deps = me.deps,
//            newDeps = {},
//            len = deps.length,
//            newCount = 0;
//
//        for (i in deps) {
//            if (deps[HAS](i)) {
//                dep = deps[i];
//                if (dep !== id) {
//                    newDeps[dep] = dep;
//                    newCount++;
//                }
//            }
//        }
//        me.deps = newDeps;
//
//        if (newCount === 0) { 
//            me.ready();
//        }
//    },
//    // :: () -> IO()
//    tryResolveCycles: function () {
//        var me = this,
//            circular = me.circularMap,
//            j;
//        for (j in circular) {
//            if (circular[has](j)) {
//                me.resolveDependency(j);
//            }
//        }
//    }
//};
//
//// :: new () -> IO{}
//function DependencyManager() {
//    var me = this;
//    me.available = {};
//    me.bottomUp = {}; // dependencies -> dependency
//    me.topDown = {}; // id -> dependencies
//}
//DependencyManager.prototype = {
//    // :: "" -> [""] -> (() -> IO()) -> IO()
//    add: function add(id, deps, ready) {
//        var me = this,
//            isReady = false;
//        // Before we get the whole machinary going we check
//        // if it is really necessary ...
//        if (deps.length < 1) {
//            // No dependencies, so we mark it as resolved
//            me.resolve(id);
//            ready();
//            return;
//        }
//        else {
//            // Checking for already fullfilled dependencies
//            isReady = true;
//            Y.each(deps, function (dep) {
//                isReady = isReady && me.available[dep];
//            });
//            if (isReady) {
//                me.resolve(id);
//                ready();
//                return;
//            }
//        }
//        if (me.available[id]) {
//            ready();
//            return; // Early termination if already available
//        }
//        var thisDep = new Dependency(id, ready),
//            len = deps.length,
//            dep,
//            bottomUp,
//            cycle,
//            i;
//        // Building bottom up map (dependencies -> this)
//        for (i = 0; i < len; i++) {
//            dep = deps[i];
//            thisDep.addDependency(dep);
//
//            // Checking for freshly introduced
//            // cyclic dependencies
//            cycle = me.detectCycle(dep, id);
//            if (cycle) {
//                thisDep.circularMap[cycle.begin] = cycle.begin;
//            }
//
//            bottomUp = me.bottomUp[dep]; 
//            if (!bottomUp) {
//                bottomUp = (me.bottomUp[dep] = {});
//            }
//            bottomUp[id] = thisDep;
//        }
//
//        // In case we introduced dependency cycles
//        // we force to resolve them for being able
//        // to proceed normally
//        thisDep.tryResolveCycles();
//
//        me.topDown[id] = thisDep;
//    },
//    // :: "" -> "" -> { begin: "", end: ""}
//    detectCycle: function (dep, id) {
//        var tree = this.topDown,
//            root = dep;
//        return (function detect(dep, id) {
//            if (dep === id) {
//                return { begin: dep, end: id }; // Just found a cycle
//            }
//            var elem = dep,
//                deps,
//                sub;
//
//            while (elem = tree[elem]) {
//                deps = elem.deps;
//                if (deps[id]) {
//                    ;;; Y.warn('Circular dependency between modules', root, 'and', id, 'detected');
//                    return { begin: root, end: id }; // Cycle deeper in the tree
//                }
//                for (sub in deps) {
//                    if (deps[has](sub)) {
//                        return detect(sub, id); // Dig down the tree
//                    }
//                }
//            }
//
//            return false;
//        }(dep, id));
//    },
//    // :: "" -> IO()
//    resolve: function (id) {
//        //console.log('  Dependency.resolve: `', module, '` is now available');
//        var me = this,
//            bottomUp = me.bottomUp[id],
//            i;
//
//        if (bottomUp) {
//            for (i in bottomUp) {
//                if (bottomUp[HAS](i)) {
//                    bottomUp[i].resolveDependency(id);
//                }
//            }
//        }
//        me.available[id] = me.topDown[id] || true;
//    }
//};
//return DependencyManager;
//
//}());
//
//var depManager = new DependencyManager(),
//    repo = {};
//
//// :: string -> [] -> {} -> ()
//function add(id, deps, factory) {
//    if (!Y.isString(id)) {
//        factory = deps;
//        deps = id;
//        id = '';
//    }
//    if (Y.isFunction(deps)) {
//        factory = deps;
//        deps = [];
//    }
//    if (!(Y.isString(id) && Y.isArray(deps) && Y.isFunction(factory))) {
//        Y.raise('Error while adding module (', id, ', ', deps, ', ', factory, ')');
//    }
//    var isAnonymous = id.length < 1,
//        core = Core();
//    if (isAnonymous) {
//        // We need a valid id so we just generate one
//        // since using an empty one would only 
//        // overly complicate things
//        id = Y.gid('anon');
//    }
//    //log('JEO.add:', arguments);
//    depManager.add(id, deps, function () {
//        //;;; log('  added', id, ':', Y.Array(arguments));
//        var imports = ocreate(core),
//            mod = {},
//            returned;
//
//        // Importing the dependencies
//        Y.own(deps, function (dep) {
//            //Y.mix(imports, repo[dep]);
//            var subRepo = repo[dep];
//            for (var i in subRepo) {
//                if (imports[i]) {
//                    ;;; Y.warn('multiple definitions of "', dep, '" in module "' + id + '"!');
//                }
//                imports[i] = subRepo[i];
//            }
//        });
//
//        // After adding dependencies we create a delegated
//        // object for being able to tell imports from exports
//        var exports = ocreate(imports);
//
//        returned = factory[CALL](core.env.win, exports)
//        exports = returned ? returned : exports;
//
//        if (!isAnonymous) {
//            //;;; log('JEO.add: returned =', returned, ', exports =', exports);
//            own(exports, function (prop, name) {
//                //;;; log('    ' + name + ' = ' + prop);
//                mod[name] = prop;
//            });
//
//            repo[id] = mod;
//            depManager.resolve(id);
//        }
//    });
//    return core;
//}
//;;; add.dependencies = depManager;
//;;; add.repo = repo;
//
//function use(deps, factory) {
//    return add[CALL](this, deps, factory);
//}
//
//Y.provide('JEO.add', add);
//Y.provide('JEO.use', use);
// 
//});

/** Tests */

JEO.add('blubb', ['bla'], function (Y) {

    Y.mix({
        Blubb: function Blubb() {},
        plisch: 'platsch'
    });

    Y.BlubbManager = function BlubbManager() {
        
    };
});

JEO.add('dep0', [], function (Y) {
    Y.dep0 = true;
});

JEO.add('plisch', function (Y) {
    Y.info('(no deps) plisch');
});

JEO.add([], function (Y) {
    Y.info('(anonymous, no deps)');
});

JEO.add(function (Y) {
    Y.info('(anonymous, missing deps)');
});

JEO.add(['dep1'], function (Y) {
    Y.info('(anonymous, with dep)', Y.Array(arguments));
});

JEO.add('bla', ['dep0', 'dep1'], function (Y) {
    Y.log('bla is there! [dep0:', Y.dep0, ', dep1:', Y.dep1, ']', { imports: Y });
});

JEO.add('dep1', [], function (Y) {
    Y.info('(dep1, missing deps)', Y.Array(arguments));
    Y.dep1 = true;
});

/*
JEO.add
JEO.cls
JEO.define
JEO.entity
JEO.fun
JEO.ifc
JEO.Interface
JEO.Module
JEO.Namespace
JEO.Package
JEO.pkg
JEO.Protocol
JEO.run
JEO.trait
JEO.use
*/
























