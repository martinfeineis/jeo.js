JEO.use(function (Y) {
Y.info('sandboxing', Y.Array(arguments));

    var doc = Y.env.doc,
        iframe = doc.createElement('iframe'),
        body = Y.one('body');
    iframe.setAttribute('id', 'jeo-testframe');
    body.insertBefore(iframe, body.firstChild);

    var ifr = Y.one('#jeo-testframe');
    var doc2 = ifr.contentDocument;
    var win2 = ifr.contentWindow;
    var s = doc2.createElement('script');
    var head = doc2.getElementsByTagName('head')[0];
    head.insertBefore(s, head.firstChild);

    var Arr = win2.Array,
        Boo = win2.Boolean,
        Dat = win2.Date,
        Fun = win2.Function,
        Num = win2.Number,
        Obj = win2.Object,
        Reg = win2.RegExp,
        Str = win2.String;

    Arr.prototype.blubb = function blubb() {
        return 'bla';
    };

    Y.provide('JEO.Array', Arr);

    Y.log('(new Arr).blubb:', typeof (new Arr).blubb);
    Y.log('[].blubb:\t', typeof [].blubb);
});


















