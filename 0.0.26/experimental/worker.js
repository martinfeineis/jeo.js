importScripts('../jeo-dev.js');

JEO.use(function (Y) {

if (Y.env.worker) {
    this.onmessage = function (ev) {
        //Y.log('worker<-', ev.data);
        this.postMessage(ev.data);
    };
}

});
