JEO.use(function (Y) {
Y.info('promise', arguments);

var Callbacks = Y.define({
    constructor: function () {
        var me = this;
        me.list_ = [];
        me.push.apply(me, arguments);
    },
    prototype: {
        call: function (ctx, a) {
            if (arguments.length < 2) {
                a = ctx;
                ctx = null;
            }
            Y.each(this.list_, function (fn) {
                fn.call(ctx, a);
            });
        },
        push: function () {
            var me = this;
            Y.each(arguments, function (fn) {
                me.list_.push(fn);
            });
        }
    }
});

var Promise = Y.define({
    constructor: function (ok, error, progress) {
        var me = this,
            completed = me.onCompleted_ = new Callbacks,
            rejected = me.onRejected_ = new Callbacks;
        Y.mix(me, {
            resolved: false,
            rejected: false
        });
        if (ok) {
            completed.push(ok);
        }
        if (error) {
            rejected.push(error);
        }
    },
    prototype: {
        reject: function (reason) {
            //Y.log('promise.reject:', arguments);
            var me = this;
            if (!me.resolved && !me.rejected) {
                me.rejected = true;
                me.reason_ = reason;
                me.onRejected_.call(me, reason);
            }
            return me;
        },
        resolve: function (value) {
            //Y.log('promise.resolve:', arguments);
            var me = this;
            if (!me.resolved && !me.rejected) {
                me.resolved = true;
                me.result_ = value;
                me.onCompleted_.call(me, value);
            }
            return me;
        },
        then: function (promise) {
            //Y.log('promise.then:', arguments);
            var me = this,
                p = (promise instanceof Promise)
                    ? promise
                    : new Promise(promise);
            if (me.resolved) {
                p.resolve(me.result_);
                return me;
            }
            if (me.rejected) {
                p.reject(me.reason_);
                return me;
            }
            me.onCompleted_.push(p.resolve.bind(p));
            me.onRejected_.push(p.reject.bind(p));
            return p;
        }
    }
});

var p = new Promise(
function ok(value) {
    Y.log('  init:', arguments);
},
function error(value) {
    Y.warn('  init.error:', arguments);
},
function progress(value) {

});

p.then(function () {
    Y.log('  then:', arguments);
});

p
.resolve({ value: 'someValue' })
//.reject('Ohmygoddd they killed Kenny')
.then(function () { 
    Y.log('  then2:', arguments);
});

});
