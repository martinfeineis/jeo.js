/*! JEO.js - A lightweight default environment for browsers. (c)2012 Martin Feineis */
// Credits to jQuery, YUI, PrototypeJS, Closure Library and many more for inspiration.
// The core library is optimized for source code optimizers like YUICompressor by 
// providing shorthands for commonly used property names
(function (global, console, Array, Date, Function, Object, RegExp, String, undefined) {
'use strict';

// Common property names will be munged
// throughout the build process
var APPLY = 'apply',
    ARRAY = 'array',
    ASYNC = 'async',
    BIND = 'bind',
    CALL = 'call',
    CORE_CONFIG = 'JEO',
    CONSTRUCTOR = 'constructor',
    CREATE = 'create',
    CREATE_ELEMENT = 'createElement',
    DEFER = 'defer',
    DEFINE = 'define',
    DEFINE_PROPERTIES = 'defineProperties',
    DEFINE_PROPERTY = 'defineProperty',
    ENV = 'env',
    EXTEND = 'extend',
    FALSE = !1,
    FIRST_CHILD = 'firstChild',
    FOR_EACH = 'forEach',
    FREEZE = 'freeze',
    GET_ATTRIBUTE = 'getAttribute',
    GET_OWN_PROPERTY_DESCRIPTOR = 'getOwnPropertyDescriptor',
    HAS = 'hasOwnProperty',
    INSERT_BEFORE = 'insertBefore',
    IS_ARRAY = 'isArray',
    KEYS = 'keys',
    LENGTH = 'length',
    MIXINS = 'mixins',
    MOZ_WEB_SOCKET = 'MozWebSocket',
    NOW = 'now',
    NULL = null,
    OBJECT = 'object',
    ONE = 'one',
    PROTO = 'prototype',
    PUSH = 'push',
    REPLACE = 'replace',
    SET_ATTRIBUTE = 'setAttribute',
    TO_LOWER_CASE = 'toLowerCase',
    TO_STRING = 'toString',
    TRIM = 'trim',
    TRUE = !0,
    UNDEFINED = 'undefined',
    WEB_SOCKET = 'WebSocket',
    WORKER = 'Worker';

// Happy console world :)
console = (!!console) ? console : { log: nop, info: nop, error: nop, warn: nop };

// Thanks to PrototypeJS
var DONT_ENUMS = [TO_STRING, 'toLocaleString', 'valueOf', HAS, 
        'isPrototypeOf', 'propertyIsEnumerable', CONSTRUCTOR],
    
    defaultConfig = (CORE_CONFIG in global) ? global[CORE_CONFIG] : {},
    doc = global.document,
    env = {
        // Just for convenience; should not be necessary outside the core library itself
        doc: doc, 
        win: global,
        worker: (function () {
            var isWorker = 'importScripts' in global &&
                    'postMessage' in global;
            return isWorker ? global : NULL;
        }())
    },
    HEAD_ELEM,

    slice = [].slice, // Native Array.prototype.slice
    toString = {}[TO_STRING]; // Native Object.prototype.toString
        
//// Feature detection for the current environment
var supports = env.supports = {
    amd: (function () {
        var define = global[DEFINE];
        if (define && define[LENGTH] && typeof define['amd'] === OBJECT) {
            return TRUE;
        }
        return FALSE;
    }()),
    // Some versions of JScript fail to enumerate over properties, names of which 
    // correspond to non-enumerable properties in the prototype chain
    correctEnumeration: (function(){
        for (var p in { toString: 1 }) {
            // check actual property name, so that it works with augmented Object.prototype
            if (p === TO_STRING) {
                return TRUE;
            }
        }
        return FALSE;
    }()),
    // Detects function decompilation capabilities
    functionDecompilation: /xyz/.test(function(){xyz}),
    // Detects whether or not strict mode is available
    strictMode: (function () {
        "use strict";
        return !this;
    }()),
    websocket: TRUE,
    xhr: TRUE
};
// Detects ES5 defineProperty capabilities
supports[DEFINE_PROPERTY] = (function () {
    var o = {};
    try {
        Object[DEFINE_PROPERTY](o, ONE, { get: function () { return 1; } })
        return 1 === o[ONE];
    } catch(e) {
        return FALSE;
    }
}());
// IE8 implements Object.defineProperty and Object.getOwnPropertyDescriptor
// only for DOM objects. These methods don't work on plain objects.
// Hence, we need a more elaborate feature-test to see whether the
// browser truly supports these methods:
supports[GET_OWN_PROPERTY_DESCRIPTOR] = (function () {
    try {
        if (Object[GET_OWN_PROPERTY_DESCRIPTOR]) {
            var test = {x:0};
            return !!Object[GET_OWN_PROPERTY_DESCRIPTOR](test,'x');        
        }
    } 
    catch(e) {}
    return FALSE;
}());


/** Environment normalization */

// Detects whether a the property `name` is native
// to the given object `o`; only works properly in
// environments that support function decompilation
// :: {} -> "" -> boolean
function isNative(o, name) {
    var ex = typeof o[name] !== UNDEFINED;
    return ex && isNative.fn(o[name]);
}
isNative.fn = supports.functionDecompilation
  // :: ({} -> {}) -> boolean
  ? function (fn) {
        return /[\s*native\s+code\s*]/i.test(fn);
    }
  // :: () -> boolean
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        // and hope for the best
        // TODO: Maybe we should rely on our implementation?
        return TRUE;
    };

if (!isNative(Array[PROTO], FOR_EACH)) {
    // :: ({} -> ""|number -> {}) -> {}? -> ()
    Array[PROTO][FOR_EACH] = function (fn/*, ctx*/) {
        var ctx = arguments[1] || this,
            len = this[LENGTH];
        for (var i = 0; i < len; i++) {
            //console.log('each:', i, this[i]);
            fn[CALL](ctx, this[i], i, this);
        }
    };
}

if (!isNative(Array, IS_ARRAY)) {
    // :: {} -> boolean
    Array[IS_ARRAY] = function (o) {
        return typeOf(o) === ARRAY;
    };
}

if (!isNative(Function[PROTO], BIND)) {
    // :: {} -> function
    Function[PROTO][BIND] = function (ctx) {
        var args = slice[CALL](arguments, 1),
            fn = this;
        return function () {
            var as = merge(args, arguments);
            return fn.apply(ctx, as);
        };
    };
}

if (!isNative(Object, CREATE)) {
    // :: {} -> {}? -> {}
    Object[CREATE] = (function () {
        function F() {}
        return function (proto, props) {
            F[PROTO] = proto;
            var o = new F;
            if (props) {
                Object[DEFINE_PROPERTIES](o, props);
            }
            return o;
        };
    }());
}
var ocreate = Object[CREATE];

if (!isNative(Date, NOW)) {
    // :: () -> number
    Date[NOW] = function () {
        // Thanks to the Closure library
        // (unary+) calls getTime() on Date
        return +new Date; 
    };
}

if (!supports[DEFINE_PROPERTY]) {
    // :: {} -> "" -> { value: {} } -> ()
    Object[DEFINE_PROPERTY] = function (o, name, descr) {
        o[name] = descr.value;
    };
    // :: {} -> [{ value: {} }] -> ()
    Object[DEFINE_PROPERTIES] = function (object, descriptors) {
        own(descriptors, function (descriptor, property) {
            defineProperty(object, property, descriptor);
        });
    };
}
var defineProperty = Object[DEFINE_PROPERTY];

if (!isNative(Object, FREEZE)) {
    // :: {} -> {}
    Object[FREEZE] = function (o) { return o; }; // Impossible in ES3
}
var ofreeze = Object[FREEZE];

if (!supports[GET_OWN_PROPERTY_DESCRIPTOR]) {
    // :: {} -> "" -> { value: {}, enumerable, writable, configurable: boolean }
    Object[GET_OWN_PROPERTY_DESCRIPTOR] = function (o, name) {
        return {
            value: o[name],
            enumerable: TRUE,
            writable: TRUE,
            configurable: TRUE
        };
    };
}

if (!isNative(Object, KEYS)) {
    // :: {} -> [""]
    Object[KEYS] = function (o) {
        var result = [];
        own(o, function (_, name) {
            result[PUSH](name);
        });
        return result;
    };
}

if (!isNative(String[PROTO], TRIM)) {
    // :: () -> "" 
    String[PROTO][TRIM] = function () {
        return this[REPLACE](/^\s+|\s+$/, '');
    };
}

// :: "" -> (("", ...) -> ())
function wrapConsole(name) {
    var fn = console[name];
    return function () {
        try {
            fn[APPLY](console, arguments);
        }
        catch (e) {
            // Nothing ...
        }
    };
}

// X-Browser XmlHttpRequest
// :: new () -> {}
var Xhr = (function () {
    var Xhr,
        legacy = [
            'Msxml2.XMLHTTP.6.0',
            'Msxml2.XMLHTTP.3.0',
            'Microsoft.XMLHTTP',
            'Msxml2.XMLHTTP'
        ],
        len = legacy[LENGTH],
        i;

    // This is for all reasonable browsers
    if (XMLHttpRequest) {
        return XMLHttpRequest;
    } 

    // Testing for legacy http requests
    for (i = 0; i < len; i++) {
        Xhr = function () {
            var id = legacy[i];
            return new ActiveXObject(id);
        };
        try {
            new Xhr;
            return Xhr;
        } 
        catch (e) {} 
        finally {
            Xhr = NULL;
        }
    }
    // O.o - epic fail
    supports.xhr = FALSE;
    raise('XMLHttpRequest not available.');
}());

/** Base library */

// :: []|{ length: number } -> []
var YArray = function (arr) {
    return typeof arr !== UNDEFINED ? slice[CALL](arr) : [];
};

// :: new ("") -> {}
var YWebSocket = (function () {
    // Checking for WebSocket implementations
    if (WEB_SOCKET in global) {
        return global[WEB_SOCKET];
    }
    if (MOZ_WEB_SOCKET in global) {
        return global[MOZ_WEB_SOCKET];
    }
    // OMG it's ancient :)
    supports.websocket = FALSE;
    return function NotSupported() {
        // Providing a dummy to remind the user of
        // checking for support in the first place
        raise('WebSocket not supported by this browser.');
    };
}());

// :: { data?: {}, method?: "", sync: boolean, url: "" } -> ("" -> ())
function ajax(config, ok) {
    //console.log('net.get', arguments);
    if (isString(config)) {
        config = { url: config };
    }
    var xhr = new Xhr,
        url = config.url,
        async = !config.sync,
        data = config.data || undefined,
        method = config.method || 'GET',
        params = [];

    own(config.params || {}, function (val, key) {
        //log('param', key, val);
        params[PUSH](escape(key) + '=' + escape(val));
    });
    if (params[LENGTH] > 0) {
        url = url + '?' + params.join('&');
    }
    xhr.onreadystatechange = function (e) {
        //console.log('  xhr.readystatechange', xhr, arguments);
        var status = xhr.status;
        if (xhr.readyState === 4) {
            // Local ok || net ok
            if (status === 0 || (status >= 200 && status < 300)) {
                ok(xhr.responseText);
            }
            else {
                raise('Request to "' + url + '" failed.');
            }
            xhr = NULL;
        }
    };
    try {
        //log(url);
        xhr.open(method.toUpperCase(), url, async);
        xhr.send(data);
        return TRUE;
    }
    catch (e) {
        return FALSE;
    }
}

// :: this[0] -> ({} -> "" -> [0] -> ()) -> {context}? -> ()
var arrayForEach = Array[PROTO][FOR_EACH];

// :: ""? -> { 
//      constructor: new (...) -> {},
//      extend: function,
//      prototype: {},
//      statics: {}
// } -> function
function define(name, o) {
    if (!o) {
        o = name;
        name = '';
    }
    var Type = o[HAS](CONSTRUCTOR) ? o[CONSTRUCTOR] : function () {},
        Base = o[EXTEND] || Object,
        proto = o[PROTO] || {},
        statics = o.statics || {};
    Type[PROTO] = ocreate(Base[PROTO]);
    extend(Type[PROTO], proto);
    Type[PROTO][CONSTRUCTOR] = Type;
    //each(o[MIXINS], function (m) {
    //    extend(Type[PROTO], m);
    //});
    extend(Type, Base);
    extend(Type, statics);
    if (name[LENGTH] > 0) {
        provide(name, Type);
    }
    return Type;
}

// {} -> ({} -> "" -> ()) -> {}? -> ()
function each(o, fn, ctx) {
    ctx = ctx || o;
    var i, j, propName, 
        justOwn = !!arguments[3]; // internal justOwn arg
    
    if (isArray(o) || isArrayLike(o)) {
        arrayForEach[CALL](o, fn, ctx);
    }
    else {
        for (i in o) {
            if (!justOwn || o[HAS](i)) {
                fn[CALL](ctx, o[i], i, o);
            }
        }
        if (!supports.correctEnumeration) {
            for (j = DONT_ENUMS[LENGTH]-1; j >= 0; j--) {
                propName = DONT_ENUMS[j];
                if (o[propName] && (!justOwn || o[HAS](j))) {
                    fn[CALL](ctx, o[propName], propName, o);
                }
            }
        }
    }
}

var error = wrapConsole('error');

// :: {0} -> {} -> {0}
function extend(to, from) {
    if (arguments[LENGTH] < 2) {
        from = to;
        to = this;
    }
    //var level = arguments[2] || 0;
    //var tab = '';
    //for (var j = 0; j < level; j++) {
    //    tab += '  ';
    //}
    each(from, function (val, i) {
        //console.log(tab, 'extend: ', i, ': ', val, '=>', to[i]);
        var deep = FALSE;
        switch (typeOf(val)) {
        case ARRAY:
            //console.log(tab, 'extend.array: ', i);
            if (!to[i]) {
                //console.log(tab, 'extend.array new: ', i, ': ', val, '=>', to[i]);
                to[i] = [];
            }
            //else {
            //    console.log(tab, 'extend.array extend: ', i, ': ', val, '=>', to[i]);
            //}
            deep = (val[LENGTH] > 0); // No magic, just in case ...
            break;
        case OBJECT:
            if (!to[i]) {
                to[i] = {};
            }
            deep = TRUE;
            break;
        }
        if (deep) {
            //console.log(tab, 'extend.deep: ', i, ': ', val, '=>', to[i]);
            extend(to[i], val);//, level+1);
        }
        else {
            to[i] = val;
        }
    });
    //console.log(tab, 'ok:', to);
    return to;
}

var gidSeed = 0,
    expando = 'JEO?' + ((Math.random() + 1) << 30);
// :: "" -> ""
function gid(prefix) {
    return expando + ':' + (prefix || 'gid') + (gidSeed++);
}

// :: { sync?: boolean, url: "" }|"" -> ()
function include(opt) {
    if (isString(opt)) {
        opt = { url: opt };
    }
    if (!HEAD_ELEM) {
        HEAD_ELEM = one('head'); // Lazy initialization
    }
    var resType = 'js', // Defaulting to JavaScript files
        async = !opt.sync,
        url = opt.url;
    // Searching for actual file extension
    url[REPLACE](/\.([^?\/]+)(?:\?.*)?$/, function (m, ext) {
        resType = ext[TO_LOWER_CASE]();
    });
    switch (resType) {
    case 'js':
        var script = doc[CREATE_ELEMENT]('script');
        if (async) {
            script[SET_ATTRIBUTE](ASYNC, ASYNC);
            script[SET_ATTRIBUTE](DEFER, DEFER);
        }
        script[SET_ATTRIBUTE]('src', url);
        HEAD_ELEM[INSERT_BEFORE](script, HEAD_ELEM[FIRST_CHILD]);
        return;
    case 'css':
        var style = doc[CREATE_ELEMENT]('link');
        style[SET_ATTRIBUTE]('rel', 'stylesheet');
        style[SET_ATTRIBUTE]('href', url);
        HEAD_ELEM[INSERT_BEFORE](style, HEAD_ELEM[FIRST_CHILD]);
        return;
    default:
        raise('Resource type "' + resType + '" not supported.');
        return;
    }
}

var info = wrapConsole('info');

// :: {} -> boolean
var isArray = Array[IS_ARRAY];

// :: {} -> boolean
function isArrayLike(o) {
    if (o && o[LENGTH] && o[PUSH]) {
        return TRUE;
    }
    switch (typeOf(o)) {
    case 'arguments': // Fall through
    case ARRAY: // Fall through
        return TRUE;
    }
    return FALSE;
}

// :: {} -> boolean
function isFunction(o) {
    return typeOf(o) === 'function';
}

// :: {} -> boolean
function isString(o) {
    return typeof o === 'string';
}

var log = wrapConsole('log');

// :: [] -> [] -> []
function merge(as, bs) {
    var ls = slice[CALL](as);
    own(bs, function (b) {
        ls[PUSH](b);
    });
    return ls;
}

// :: {0} -> {} -> {0}
function mix(to, from) {
    if (!from) {
        from = to;
        to = this;
    }
    each(from, function (value, i) {
        to[i] = value;
    });
    return to;
}

// :: () -> ()
function nop() {}

// :: "" -> {dom}
function one(id) {
    switch (id[0]) {
    case '#':
        return doc.getElementById(id[REPLACE]('#', ''));
    default:
        return doc.getElementsByTagName(id)[0];
    }
}

// :: {} -> ({} -> ()) -> {}? -> ()
function own(o, fn, ctx) {
    return each[CALL](this, o, fn, ctx, 1);
}

// :: "" -> {0} -> {0}
function provide(name, o) {
    o = o || {};
    var host = global,
        ls = name.split('.'),
        len = ls[LENGTH],
        i;
    for (i = 0; i < len-1; i++) {
        host = host[ls[i]];
    }
    return (host[ls[i]] = o);
}

// :: "" -> ()
function raise(message) {
    error[APPLY](console, arguments);
    throw message;
}

var typeOf = (function () {
    var cache = {};   

    function extractType(t) {
        // Caching types
        var i = cache[t] = t[REPLACE](/\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1')[TO_LOWER_CASE]();
        return i;
    }
    
    own([{}, [], 1, '1', /1/, new Date, nop], function (o) {
        // Extracting type from "[object ...]"
        extractType(toString[CALL](o)); 
    });        
        
    // :: {} -> ""
    return function typeOf(o) {
        if (o === NULL) {
            return 'null';
        }
        if (o === undefined) {
            return UNDEFINED;
        }
        var t = toString[CALL](o);
        return (t in cache)
            ? cache[t] 
            : extractType(t);
    };
}());

var warn = wrapConsole('warn');

/** Library core */
   
// Including the default configuration
env = extend(env, defaultConfig);

var DependencyManager = (function () {

/*
*  The DependencyManager for our modules
*/
// :: "" -> (() -> IO()) -> IO{}
function Dependency(id, ready) {
    var me = this;

    me.done = 0;
    me.deps = {};
    me.id = id;
    me.circularMap = {};

    // :: () -> IO()
    me.ready = function () {
        if (!me.done) {
            me.done = 1;
            ready();
        }
    };
}
Dependency.prototype = {
    // :: "" -> IO()
    addDependency: function (id) {
        this.deps[id] = id;
    },
    // :: "" -> IO()
    resolveDependency: function (id) {
        //console.log('    resolve:', module);
        var me = this,
            i,
            dep,
            deps = me.deps,
            newDeps = {},
            len = deps.length,
            newCount = 0;

        for (i in deps) {
            if (deps[HAS](i)) {
                dep = deps[i];
                if (dep !== id) {
                    newDeps[dep] = dep;
                    newCount++;
                }
            }
        }
        me.deps = newDeps;

        if (newCount === 0) { 
            me.ready();
        }
    },
    // :: () -> IO()
    tryResolveCycles: function () {
        var me = this,
            circular = me.circularMap,
            j;
        for (j in circular) {
            if (circular[has](j)) {
                me.resolveDependency(j);
            }
        }
    }
};

// :: new () -> IO{}
function DependencyManager() {
    var me = this;
    me.available = {};
    me.bottomUp = {}; // dependencies -> dependency
    me.requestDependency = nop; // Hook for dynamic dependencies
    me.topDown = {}; // id -> dependencies
}
DependencyManager.prototype = {
    // :: "" -> [""] -> (() -> IO()) -> IO()
    add: function add(id, deps, ready) {
        var me = this,
            isReady = false;
        // Before we get the whole machinary going we check
        // if it is really necessary ...
        if (deps.length < 1) {
            // No dependencies, so we mark it as resolved
            me.resolve(id);
            ready();
            return;
        }
        else {
            // Checking for already fullfilled dependencies
            isReady = true;
            each(deps, function (dep) {
                isReady = isReady && me.available[dep];
            });
            if (isReady) {
                me.resolve(id);
                ready();
                return;
            }
        }
        if (me.available[id]) {
            ready();
            return; // Early termination if already available
        }
        var thisDep = new Dependency(id, ready),
            len = deps.length,
            dep,
            bottomUp,
            cycle,
            i;
        // Building bottom up map (dependencies -> this)
        for (i = 0; i < len; i++) {
            dep = deps[i];
            thisDep.addDependency(dep);
            me.requestDependency(dep);

            // Checking for freshly introduced
            // cyclic dependencies
            cycle = me.detectCycle(dep, id);
            if (cycle) {
                thisDep.circularMap[cycle.begin] = cycle.begin;
            }

            bottomUp = me.bottomUp[dep]; 
            if (!bottomUp) {
                bottomUp = (me.bottomUp[dep] = {});
            }
            bottomUp[id] = thisDep;
        }

        // In case we introduced dependency cycles
        // we force to resolve them for being able
        // to proceed normally
        thisDep.tryResolveCycles();

        me.topDown[id] = thisDep;
    },
    // :: "" -> "" -> { begin: "", end: ""}
    detectCycle: function (dep, id) {
        var tree = this.topDown,
            root = dep;
        return (function detect(dep, id) {
            if (dep === id) {
                return { begin: dep, end: id }; // Just found a cycle
            }
            var elem = dep,
                deps,
                sub;

            while (elem = tree[elem]) {
                deps = elem.deps;
                if (deps[id]) {
                    ;;; Y.warn('Circular dependency between modules', root, 'and', id, 'detected');
                    return { begin: root, end: id }; // Cycle deeper in the tree
                }
                for (sub in deps) {
                    if (deps[has](sub)) {
                        return detect(sub, id); // Dig down the tree
                    }
                }
            }

            return false;
        }(dep, id));
    },
    // :: "" -> IO()
    resolve: function (id) {
        //console.log('  Dependency.resolve: `', module, '` is now available');
        var me = this,
            bottomUp = me.bottomUp[id],
            i;

        if (bottomUp) {
            for (i in bottomUp) {
                if (bottomUp[HAS](i)) {
                    bottomUp[i].resolveDependency(id);
                }
            }
        }
        me.available[id] = me.topDown[id] || true;
    }
};
return DependencyManager;

}());

var depManager = new DependencyManager(),
    repo = {};

// Enabling dynamic loading of dependencies
(function () {
    var alreadyRequested = {};

    function isDynamic() {
        return 'loader' in env && env.loader.dynamic;
    }
    function mapPath(id) {
        return (env.loader.baseUrl || './') + id + '.js';
    }
    depManager.requestDependency = function (id) {
        if (isDynamic() && !alreadyRequested[id]) {
            alreadyRequested[id] = true;
            //log('depManager.requestDependency:', YArray(arguments));
            include(mapPath(id));
        }
    };
}());

// :: "" -> [""] -> (({}, ...) -> ()) -> {JEO}
function add(id, deps, factory) {
    if (!isString(id)) {
        factory = deps;
        deps = id;
        id = '';
    }
    if (isFunction(deps)) {
        factory = deps;
        deps = [];
    }
    if (!(isString(id) && isArray(deps) && isFunction(factory))) {
        raise('Error while adding module (', id, ', ', deps, ', ', factory, ')');
    }
    var isAnonymous = id.length < 1;

    if (isAnonymous) {
        // We need a valid id so we just generate one
        // since using an empty one would only 
        // overly complicate things
        id = gid('anon');
    }
    //log('JEO.add:', arguments);
    depManager.add(id, deps, function () {
        ;;; //log('  added', id, ':', Y.Array(arguments));
        var imports = ocreate(core),
            mod = {};

        // Importing the dependencies
        own(deps, function (dep) {
            //Y.mix(imports, repo[dep]);
            var subRepo = repo[dep];
            for (var i in subRepo) {
                if (imports[i]) {
                    ;;; warn('multiple definitions of "', dep, '" in module "' + id + '"!');
                }
                imports[i] = subRepo[i];
            }
        });

        // After adding dependencies we create a delegated
        // object for being able to tell imports from exports
        var exports = ocreate(imports);

        //var returned = 
        factory[CALL](core.env.win, exports);
        //exports = returned ? returned : exports;

        if (!isAnonymous) {
            ;;; //log('JEO.add: returned =', returned, ', exports =', exports);
            own(exports, function (prop, name) {
                ;;; //log('    ' + name + ' = ' + prop);
                mod[name] = prop;
            });

            repo[id] = mod;
            depManager.resolve(id);
        }
    });
    return core;
}
;;; //add.dependencies = depManager;
;;; //add.repo = repo;

// :: ["", ...]? -> ({JEO} -> {JEO}) -> {JEO}
function use(deps, factory) {
    return add[CALL](this, deps, factory);
}

// :: () -> {}
var Core = mix(function (config) {
    var me = ocreate(base);
    me[ENV] = ofreeze(extend(ocreate(env), config));
    return me;
}, {
    add: add,
    // Including the default configuration with the
    // current environment
    env: env,//ofreeze(env),
    // :: ["", ...]? -> ({JEO} -> {JEO}) -> {JEO}
    use: function () {
        return use[APPLY](core, arguments);
    },
    version: '0.0.26'
});

defineProperty(Core, TO_STRING, {
    configurable: FALSE,
    enumerable: FALSE,
    // :: () -> ""
    value: function () {
        return 'You are running JEO v' + this.version;
    }
});

// Adding the base library that will always be included;
// including the Core.prototype in the prototype chain
// for being able to enable (base instanceof Core === true)
var base = ofreeze(mix(ocreate(Core[PROTO]), {
    Array: YArray,
    WebSocket: YWebSocket,
    ajax: ajax,
    define: define,
    each: each,
    error: error,
    extend: extend,
    gid: gid,
    include: include,
    info: info,
    isArray: isArray,
    isArrayLike: isArrayLike,
    isFunction: isFunction,
    isString: isString,
    log: log,
    merge: merge,
    mix: mix,
    nop: nop,
    one: one,
    own: own,
    provide: provide,
    raise: raise,
    typeOf: typeOf,
    use: use,
    warn: warn
}));

var core = Core(); // Default instance

//ofreeze(Core); // TODO: Really use JEO as a namespace?

provide('console', console); // Defining a console; IE doesn't define it outside webdev mode
provide('JEO', Core); // Exposing the core

// Handing in common language and host objects to easily enable minimization
}(this, ('console' in this) ? console : null, Array, Date, Function, Object, RegExp, String));

































