/*! JEO.js - A lightweight default library for browsers 
(c) 2012 Martin Feineis released under MIT license */
// Credits to jQuery, YUI, PrototypeJS, Closure Library and many more for 
// inspiration.
// See README.md and LICENSE.md for details and licensing.
(function (global,
    Array, Date, Function, Object, RegExp, String, undefined) {
'use strict';
     
// The munger will inject shorthand definitions into
// this first declaration statement of the file without being
// aware of any scope, so be sure not to move this
// too far off!

var LIBRARY_NAME = 'JEO',
    CORE_CONFIG = LIBRARY_NAME,

    // Thanks to PrototypeJS
    DONT_ENUMS = ['toString', 'toLocaleString', 'valueOf', 'hasOwnProperty', 
        'isPrototypeOf', 'propertyIsEnumerable', 'constructor'],
    
    // A global configuration for all instances can be supplied by
    // specifying an object with the name <CORE_CONFIG> on the global
    // object before including this library:
    // <script>var <CORE_CONFIG> = { loader: { dynamic: true } };</script>
    // <script src="path/to/this/library.js"></script>
    globalConfig = (CORE_CONFIG in global) ? global[CORE_CONFIG] : {},
    doc = global.document,
    env = {
        // Just for convenience; should not be necessary outside the 
        // core library itself; if so the functionality should most likely be 
        // added to the library base
        doc: doc, 
        // The version of the library being used
        jeo: '0.2.0',
        // The global context we're running in
        win: global,
        // In case we're inside a worker we provide a reference
        worker: (function () {
            return 'importScripts' in global ? global : null;
        }())
    },
    HEAD_ELEM,
    loc = global.location,

    // Happy console world :)
    console = 'console' in global 
        ? global['console'] 
        : { log: nop, info: nop, error: nop, warn: nop },

    ArrayProto = Array.prototype,
    slice = [].slice, // Native Array.prototype.slice
    toString = {}.toString; // Native Object.prototype.toString

// TODO: Maybe we should encourage a one-global-only approach by
//       enabling the user to easily give the library a custom name
//       to be incorporated into the user library?
// PROS: * This is fresh functionality not provided by any library I know of
// CONS: * Module definitions would be different for every project
//         (is this really a problem?)
// var JEO = { <libName>: 'FunkySite' };
// ...
// console.log(typeof JEO); // -> 'undefined'
// FunkySite.add('module', ['dep0', 'dep1'], ...)
// FunkySite.use(['module'], function (M) { ... }


// Feature detection for the current environment
var supports = env.supports = {
    // Some versions of JScript fail to enumerate over properties, names of 
    // which correspond to non-enumerable properties in the prototype chain
    correctEnumeration: (function(){
        for (var p in { toString: 1 }) {
            if (p === 'toString') return true;
        }
        return false;
    }()),
    // Detects function decompilation capabilities
    functionDecompilation: /fun/.test(function () {}),
    // Signals if the current environment has native querySelector capabilities
    //querySelector: false, // Delayed until DOM is ready see signal dom:ready
    // Detects whether or not strict mode is available
    strictMode: (function () {
        'use strict';
        return !this;
    }()),
    webWorker: 'Worker' in global,
    webSocket: (function () {
        // Checking for WebSocket implementations
        if ('WebSocket' in global) {
            return true;
        }
        if ('MozWebSocket' in global) {
            return true;
        }
    }()),
    xhr: true // Delayed, see Xhr
};
// Detects ES5 defineProperty capabilities
supports.defineProperty = (function (o) {
    try {
        Object.defineProperty(o, 'a', {
            get: function () {
                return 1;
            }
        })
        return 1 === o.a;
    } catch(e) {}
}({}));

/** Environment normalization */

// Detects whether a the property `name` is native
// to the given object `o`; only works properly in
// environments that support function decompilation
// :: {} -> "" -> boolean
function isNative(o, name) {
    var ex = typeof o[name] !== 'undefined';
    return ex && isNative.fn(o[name]);
}
isNative.fn = supports.functionDecompilation
  // :: ({} -> {}) -> boolean
  ? function (fn) {
        return /[\s*native\s+code\s*]/i.test(fn);
    }
  // :: () -> boolean
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        // and hope for the best
        // TODO: Maybe we should rely on our implementation?
        return true;
    };

// NOTE: Not shimming forEach on Array.prototype because it would
//       be confusing to have just this one shimmed and others like
//       every, some, etc. are still missing
// :: this[0] -> ({} -> "" -> [0] -> ()) -> {context}? -> ()
var arrayForEach = isNative(ArrayProto, 'forEach')
  ? ArrayProto.forEach
  // :: ({} -> ""|number -> {}) -> {}? -> ()
  : function (fn/*, ctx*/) {
        var me = this,
            ctx = arguments[1] || me,
            len = me.length,
            i = 0;
        for (; i < len; i++) {
            //console.log('each:', i, this[i]);
            fn.call(ctx, me[i], i, me);
        }
    };
    
if (!isNative(ArrayProto, 'indexOf')) {
    // OMG, we even have to shim this for IE8 ...
    // :: {} -> number
    ArrayProto.indexOf = function (el) {
        var index = -1;
        arrayForEach.call(this, function (o, i) {
            if (el === o && index < 0) {
                index = i;
            }
        });
        return index;
    };
}

if (!isNative(Date, 'now')) {
    // :: () -> number
    Date.now = function () {
        // Thanks to the Closure library
        // (unary+) calls getTime() on Date
        return +new Date; 
    };
}

if (!isNative(Function.prototype, 'bind')) {
    // :: {} -> function
    Function.prototype.bind = function (ctx) {
        var args = slice.call(arguments, 1),
            fn = this;
        return function () {
            var as = merge(args, arguments);
            return fn.apply(ctx, as);
        };
    };
}

if (!isNative(Object, 'create')) {
    // :: {} -> {}? -> {}
    Object.create = (function () {
        function F() {}
        return function (proto, props) {
            F.prototype = proto;
            var o = new F;
            if (props) {
                Object.defineProperties(o, props);
            }
            return o;
        };
    }());
}
var ocreate = Object.create;

if (!supports.defineProperty) {
    // :: {} -> "" -> { value: {} } -> IO()
    Object.defineProperty = function (o, name, descr) {
        o[name] = descr.value;
    };
    // :: {} -> [{ value: {} }] -> IO()
    Object.defineProperties = function (object, descriptors) {
        own(descriptors, function (descriptor, property) {
            defineProperty(object, property, descriptor);
        });
    };
}
var defineProperty = Object.defineProperty;

if (!isNative(Object, 'freeze')) {
    // :: {} -> IO{}
    Object.freeze = function (o) { return o; }; // Impossible in ES3
}
var ofreeze = Object.freeze;

if (!isNative(Object, 'keys')) {
    // :: {} -> [""]
    Object.keys = function (o) {
        var result = [];
        own(o, function (_, name) {
            result.push(name);
        });
        return result;
    };
}

if (!isNative(String.prototype, 'trim')) {
    // :: () -> "" 
    String.prototype.trim = function () {
        return this.replace(/^\s+/, '').replace(/\s+$/, '');
    };
}

// :: "" -> (("", ...) -> ())
function wrapConsole(name) {
    var fn = console[name];
    return function () {
        try {
            fn.apply(console, arguments);
        }
        catch (e) {
            //fn(arguments.join(''));
        }
    };
}

// X-Browser XmlHttpRequest
// :: new () -> {XHR}
var Xhr = (function () {
    var Xhr,
        legacy = [
            'Msxml2.XMLHTTP.6.0',
            'Msxml2.XMLHTTP.3.0',
            'Msxml2.XMLHTTP',
            'Microsoft.XMLHTTP'
        ],
        len = legacy.length,
        i;

    // This is for all reasonable browsers
    if (XMLHttpRequest) {
        return XMLHttpRequest;
    } 

    // Testing for legacy http requests
    for (i = 0; i < len; i++) {
        Xhr = function () {
            var id = legacy[i];
            return new ActiveXObject(id);
        };
        try {
            new Xhr;
            return Xhr;
        } 
        catch (e) {} 
        finally {
            Xhr = null;
        }
    }
    // O.o - epic fail
    supports.xhr = false;
    raise('XMLHttpRequest not available.');
}());

/** Base library */

// :: []|{ length: number } -> []
var YArray = function (arr, from, to) {
    return typeof arr !== 'undefined' ? slice.call(arr, from, to) : [];
};

// :: {} -> {}? -> {}
var YObject = function (from, props) {
    return ocreate(from, props);
};

// :: { data?: {}, method?: "", sync: boolean, url: "" }
//      -> ("" -> ()) -> IO boolean
function ajax(config, ok) {
    //console.log('net.get', arguments);
    if (isString(config)) {
        config = { url: config };
    }
    var xhr = new Xhr,
        url = config.url,
        async = !config.sync,
        data = config.data || undefined,
        method = config.method || 'GET',
        params = [];

    own(config.params || {}, function (val, key) {
        //log('param', key, val);
        params.push(escape(key) + '=' + escape(val));
    });
    if (params.length > 0) {
        url = url + '?' + params.join('&');
    }
    xhr.onreadystatechange = function (e) {
        //console.log('  xhr.readystatechange', xhr, arguments);
        var status = xhr.status;
        if (xhr.readyState === 4) {
            // Local ok || net ok
            if (status === 0 || (status >= 200 && status < 300)) {
                ok(xhr.responseText);
            }
            else {
                raise('Request to "' + url + '" failed.');
            }
            xhr = null;
        }
    };
    try {
        //log(url);
        xhr.open(method.toUpperCase(), url, async);
        xhr.send(data);
        return true;
    }
    catch (_) {}
}

// :: '' -> [{DOM}]
function all(id) {
    return doc.querySelectorAll(id);
}

// :: {} -> {}
function clone(o) {
    if (isFunction(o)) {
        ;;; warn('Cloning a function not supported.');
        return null;
    }
    return extend({}, o);
}

// :: [] -> []
function dedupe(a) {
    var o = {},
        res = [];
    each(a, function (el) {
        if (!o.hasOwnProperty(el)) {
            o[el] = 1;
            res.push(el);
        }
    });
    return res;
}

// :: ""? -> { 
//      constructor: new (...) -> {},
//      extend: function,
//      prototype: {},
//      statics: {}
// } -> IO function
function define(name, o) {
    if (!o) {
        o = name;
        name = '';
    }
    var Type = o.hasOwnProperty('constructor') 
            ? o.constructor
            : function () {},
        Base = o.extend || Object,
        proto = o.prototype || {},
        statics = o.statics || {};

    Type.prototype = ocreate(Base.prototype);
    extend(Type.prototype, proto);
            
    Type.prototype.constructor = Type;
    extend(Type, Base);
    extend(Type, statics);

    if (name.length > 0) {
        provide(name, Type);
    }
    return Type;
}

// {} -> ({} -> "" -> ()) -> {}? -> ()
var each = (function () {
    var enumExtras = supports.correctEnumeration 
      ? nop
      : function (o, fn, ctx) {
            for (var propName, i = DONT_ENUMS.length-1; i >= 0; i--) {
                propName = DONT_ENUMS[i];
                if (Object.prototype.hasOwnProperty.call(o, propName)) {
                    fn.call(ctx, o[propName], propName, o);
                }
            }
        }
      
    return function (o, fn, ctx, /*internal*/own) {
        ctx = ctx || o;
        
        if (isArray(o) || isArrayLike(o)) {
            arrayForEach.call(o, fn, ctx);
        }
        else {
            for (var i in o) {
                if (!own || o.hasOwnProperty(i)) {
                    fn.call(ctx, o[i], i, o);
                }
            }
            enumExtras(o, fn, ctx, own);
        }
    };
}());

var error = wrapConsole('error');

// :: {0} -> {} -> IO{0}
function extend(to, from) {
    //if (arguments.length < 2) {
    if (!from) {
        from = to;
        to = this;
    }
    //var level = arguments[2] || 0;
    //var tab = '';
    //for (var j = 0; j < level; j++) {
    //    tab += '  ';
    //}
    each(from, function (val, i) {
        //console.log(tab, 'extend: ', i, ': ', val, '=>', to[i]);
        var deep = false;
        switch (typeOf(val)) {
        case 'array':
            //console.log(tab, 'extend.array: ', i);
            if (!to[i]) {
                //console.log(tab, 'extend.array new: ', i, 
                //    ': ', val, '=>', to[i]);
                to[i] = [];
            }
            //else {
            //    console.log(tab, 'extend.array extend: ', i, 
            //        ': ', val, '=>', to[i]);
            //}
            deep = (val.length > 0); // No magic, just in case ...
            break;
        case 'object':
            if (!to[i]) {
                to[i] = {};
            }
            deep = true;
            break;
        }
        if (deep) {
            //console.log(tab, 'extend.deep: ', i, ': ', val, '=>', to[i]);
            extend(to[i], val);//, level+1);
        }
        else {
            to[i] = val;
        }
    });
    //console.log(tab, 'ok:', to);
    return to;
}

var gidSeed = 0,
    expando = LIBRARY_NAME + '?' + ((Math.random() + 1) << 30);
// :: "" -> IO""
function gid(prefix) {
    return expando + ':' + (prefix || 'gid') + (gidSeed++);
}

// :: { sync?: boolean, url: "" }|"" -> IO()
function include(opt) {
    if (isString(opt)) {
        opt = { url: opt };
    }
    if (!HEAD_ELEM) {
        HEAD_ELEM = one('head'); // Lazy initialization
    }
    var me = this,
        resType = 'js', // Defaulting to JavaScript files
        cfg = (me.env ? me.env.config.loader : {}) || {},
        async = !opt.sync,
        url = opt.url,
        node;
    // Searching for actual file extension
    url.replace(/\.([^?\/]+)(?:\?.*)?$/, function (m, ext) {
        resType = ext.toLowerCase();
    });

    if (cfg.token) {
        url += '?' + cfg.token;
    }

    switch (resType) {
    case 'js':
        node = doc.createElement('script');
        if (async) {
            node.setAttribute('async', 'async');
            node.setAttribute('defer', 'defer');
        }
        node.setAttribute('src', url);
        
        //node.onload = node.onreadystatechange = function (e, aborted) {
        //    node.onload = node.onreadystatechange = null; // IE memory leaks
        //    console.log('included:', url);
        //};
        
        HEAD_ELEM.insertBefore(node, HEAD_ELEM.firstChild);
        return;
    case 'css':
        node = doc.createElement('link');
        node.setAttribute('rel', 'stylesheet');
        node.setAttribute('href', url);
        HEAD_ELEM.insertBefore(node, HEAD_ELEM.firstChild);
        return;
    default:
        raise('Resource type "' + resType + '" not supported.');
        return;
    }
}

var info = wrapConsole('info');

// :: {} -> boolean
var isArray = isNative(Array, 'isArray')
  ? Array.isArray
  : // :: {} -> boolean
    function (o) {
        return typeOf(o) === 'array';
    };

// :: {} -> boolean
function isArrayLike(o) {
    if (o && o.length && o.push) {
        return true;
    }
    switch (typeOf(o)) {
    case 'arguments': // Fall through
    case 'array': // Fall through
        return true;
    }
}

// :: {} -> boolean
function isFunction(o) {
    // (typeof function () {}) is 'object' in some versions of WebKit
    return typeOf(o) === 'function';
}

// :: {} -> boolean
function isString(o) {
    return typeof o === 'string';
}

var log = wrapConsole('log');

// :: [] -> [] -> IO[]
function merge(as, bs) {
    var ls = slice.call(as);
    own(bs, function (b) {
        ls.push(b);
    });
    return ls;
}

// :: {0} -> {} -> IO{0}
function mix(to, from) {
    if (!from) {
        from = to;
        to = this;
    }
    each(from, function (value, i) {
        to[i] = value;
    });
    return to;
}

// :: () -> ()
function nop() {}
    
// :: ({dom}, '', function) -> IO{ dispose: () -> IO() }
var on = (function () {
    var addEvent = 'addEventListener' in doc
            ? function (el, name, handler) {
                el.addEventListener(name, handler, false);
            }
            : function (el, name, handler) {
                el.attachEvent('on' + name, handler);
            },
        removeEvent = 'addEventListener' in doc
            ? function (el, name, handler) {
                el.removeEventListener(name, handler, false);
            }
            : function (el, name, handler) {
                el.detachEvent('on' + name, handler);
            };
    return function (name, handler, el, ctx) {
        var fn = handler.bind(ctx || el);
        addEvent(el, name, fn);
        return {
            dispose: function () {
                removeEvent(el, name, fn);
            }
        };
    };
}());

// :: "" -> {dom}
function one(id) {
    return doc.querySelector(id);
    //switch (id[0]) {
    //case '#':
    //    return doc.getElementById(id.replace('#', ''));
    //default:
    //    return doc.getElementsByTagName(id)[0];
    //}
}

// :: {} -> ({} -> ()) -> {}? -> ()
function own(o, fn, ctx) {
    return each.call(this, o, fn, ctx, 1);
}

// :: "" -> {0} -> IO{0}
function provide(name, o) {
    o = o || {};
    var host = global,
        ls = name.split('.'),
        len = ls.length,
        i;
    for (i = 0; i < len-1; i++) {
        host = host[ls[i]];
    }
    return (host[ls[i]] = o);
}

// :: "" -> IO()
function raise(message) {
    error.apply(console, arguments);
    throw message;
}

// :: {} -> IO""
var typeOf = (function (cache) {

    function extractType(t) {
        // Caching types
        var i = cache[t] = t.replace(
            /\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1').toLowerCase();
        return i;
    }
    
    // We don't use each/own since they are using typeOf themselves
    arrayForEach.call([{}, [], 1, '1', /1/, new Date, nop], function (o) {
        // Extracting type from "[object ...]"
        extractType(toString.call(o)); 
    });        
        
    return function typeOf(o) {
        if (o === null) {
            return 'null';
        }
        if (o === undefined) {
            return 'undefined';
        }
        var t = toString.call(o);
        return (t in cache)
            ? cache[t] 
            : extractType(t);
    };
}({}));

var warn = wrapConsole('warn');

/** Client side routing */
// TODO: For now we stick to hashchange implementation
//       I don't really see the benefit of pushState
//       right now; on top of that: to support real urls
//       the backend needs to be involved - not exactly
//       what I would call a clean separation of concerns
// :: new ({}) -> { dispatch,route: function }
var Router = (function () {

var routers = [],
    history = [],
    subscribers = [];

function toRoute(route, fn) {
    //log('toRoute', route);
    var s, re = route; // Route is by default a RegExp ...
    if (isString(route)) {
        // ... but for simple matching a string will also do
        s = route.replace(/\([^\)]*\)/g, '\([^\)]*\)');
        re = new RegExp('^' + s + '$');
    }
    //log(s);
    //log(re.toString());
    return function (hash) {
        var match = hash.match(re);
        //log('  match:', match);
        if (match) {
            // Handing the overall match as the last argument
            fn.apply(null, match.slice(1).concat([match[0]]));
        }
    };
}

function Router(config) {
    //info('Router', YArray(arguments));
    var me = this;
    me.routes = [];
    own(config, function (fn, hash) {
        me.route(hash, fn);
    });
    routers.push(this);
    setTimeout(dispatch, 1); // Matching the current route
}
Router.prototype = {
    go: function (hash) {
        each(this.routes, function (route) {
            route(hash);
        });
    },
    route: function (hash, fn) {
        //log('::', hash, '->', fn);
        this.routes.push(toRoute(hash, fn));
    }
};
;;; Router.history = history;

Router.go = function (route) {
    if (isString(route)/* && (loc.hash !== '#' + route)*/) {
        // Manual hash change to trigger Router.dispatch
        loc.hash = '#' + route;
    }
    else {
        dispatch();
    }
};
Router.listen = function (fn) {
    subscribers.push(fn);
};
Router.peek = function () {
    return history[history.length-1];
};

function dispatch() {
    //info('Router.dispatch:', YArray(arguments));
    
    var route = loc.hash.substring(1) || '/'; // Defaulting to root

    // Guarding route
    if (!/^[\/a-zA-Z0-9\s\(\)\?=&\-_\,]+$/.test(route)) {
        ;;; warn('Route corrupted:', route);
        loc.hash = '#404';
        return;
    }
    
    //log('  ', route);

    //// Even with the route been guarded it's still a
    //// good idea to escape it anyway
    //route = escape(route);

    history.push(route);
    //log('hash', hash);
    each(routers, function (router) {
        router.go(route);
    });
    each(subscribers, function (subscriber) {
        subscriber(route);
    });
}

// Listening for global hash changes
on('hashchange', dispatch, global);

return Router;

}());

/** Library core */

var DependencyManager = (function () {

/** The DependencyManager for our modules */
// :: new ("" -> (() -> IO())) -> IO{}
function Dependency(id, ready) {
    var me = this;

    me.done = 0;
    me.deps = {};
    me.id = id;
    me.circularMap = {};

    // :: () -> IO()
    me.ready = function () {
        if (!me.done) {
            me.done = 1;
            ready();
        }
    };
}
Dependency.prototype = {
    // :: "" -> IO()
    addDependency: function (id) {
        this.deps[id] = id;
    },
    // :: "" -> IO()
    resolveDependency: function (id) {
        //console.log('    resolve:', module);
        var me = this,
            deps = me.deps,
            newDeps = {},
            len = deps.length,
            newCount = 0;

        own(deps, function (dep) {
            if (dep !== id) {
                newDeps[dep] = dep;
                newCount++;
            }
        });
        
        me.deps = newDeps;
        if (newCount < 1) { 
            me.ready();
        }
    },
    // :: () -> IO()
    tryResolveCycles: function () {
        var me = this,
            circular = me.circularMap;
        own(circular, function (_, j) {
            me.resolveDependency(j);
        });
    }
};

// :: new () -> IO{}
function DependencyManager() {
    var me = this;
    me.available = {}; // id -> boolean
    me.bottomUp = {}; // dependencies -> dependency
    me.topDown = {}; // id -> dependencies
}
DependencyManager.prototype = {
    // :: "" -> [""] -> (() -> IO()) -> ("" -> IO()) -> IO()
    add: function add(id, deps, ready, requestDependency) {
        requestDependency = requestDependency || nop;
        var me = this,
            isReady = false;
        if (me.available[id]) {
            ready();
            return; // Early termination if already available
        }

        function resolved() {
            me.resolve(id);
        }

        // Before we get the whole machinary going we check
        // if it is really necessary ...
        if (deps.length < 1) {
            //log('   <no deps> ', id);
            // No dependencies, so we mark it as resolved
            ready(resolved);
            //me.resolve(id);
            return;
        }
        else {
            // Checking for already fullfilled dependencies
            isReady = true;
            each(deps, function (dep) {
                isReady = isReady && me.available[dep];
            });
            if (isReady) {
                ready(resolved);
                return;
            }
        }
        var thisDep = new Dependency(id, ready),
            toRequest = [],
            bottomUp,
            cycle;
        
        // Building bottom up map (dependencies -> this)
        each(deps, function (dep) {
            thisDep.addDependency(dep);
            
            // NOTE: Moved the request to end of loop due to IE9
            //       loading local resources instantly which means
            //       that the requested resource arrived before this
            //       dependency mapping finished properly.
            //requestDependency(dep); 

            // Checking for freshly introduced
            // cyclic dependencies
            cycle = me.detectCycle(dep, id);
            if (cycle) {
                thisDep.circularMap[cycle.begin] = cycle.begin;
            }

            bottomUp = me.bottomUp[dep]; 
            if (!bottomUp) {
                bottomUp = (me.bottomUp[dep] = {});
            }
            bottomUp[id] = thisDep;
            
            requestDependency(dep);
        });

        // In case we introduced dependency cycles
        // we force to resolve them for being able
        // to proceed normally
        thisDep.tryResolveCycles();

        me.topDown[id] = thisDep;
    },
    // :: "" -> "" -> { begin: "", end: ""}
    detectCycle: function (dep, id) {
        var tree = this.topDown,
            root = dep;
        return (function detect(dep, id) {
            if (dep === id) {
                return { begin: dep, end: id }; // Just found a cycle
            }
            var elem = dep,
                deps,
                sub;

            while (1) {
                elem = tree[elem]
                if (!elem) {
                    break;
                }
                deps = elem.deps;
                if (deps[id]) {
                    ;;; warn('Circular dependency between modules', root, 'and', id, 'detected');
                    // Cycle deeper in the tree
                    return { begin: root, end: id }; 
                }
                own(deps, function (_, sub) {
                    return detect(sub, id); // Dig down the tree
                });
            } 
        }(dep, id));
    },
    // :: "" -> boolean
    isTracked: function (id) {
        return this.available[id] || !!this.topDown[id];
    },
    // :: "" -> IO()
    resolve: function (id) {
        var me = this,
            bottomUp = me.bottomUp[id];
        
        if (bottomUp) {
            own(bottomUp, function (dep) {
                dep.resolveDependency(id);
            });
        }
        me.available[id] = me.topDown[id] || true;
    }
};
return DependencyManager;

}());

// Including the default configuration
var defaultConfig = {
    //debug: false,
    loader: {
        baseUrl: '',//'./'
        dynamic: true
        //sync: false
    }
};

// Extending the default configuration in case a global
// configuration has been supplied before the library
// has been loaded
env = extend(env, { config: extend(defaultConfig, globalConfig) });

// Dependency management ...
var depManager = new DependencyManager(),
    alreadyRequestedDependencies = {},
    repo = {};
        
// [""] -> [""]
function patchDependencies(deps) {
    var patched = [];
    //log('patchDeps:', YArray(deps));
    each(deps, function (dep) {
        var newDep = dep;
        dep.replace(/env\:([\w_$]+)\?([\w_$-]*)\:([\w_$-]*)/, 
            function (m, test, yes, no) {
                //log('conditional "', dep, '"');//, YArray(arguments));
                newDep = supports[test] ? yes : no;
                //log('  patched to "', newDep, '"', YArray(arguments));
            });
        // Patching a dependency to the empty string means 
        // it has been discarded
        if (newDep.length > 0) {
            patched.push(newDep);
        }
    });
    //log('patchedDeps:', patched);
    return patched;
}

// :: "" -> boolean
function testForSignal(id) {
    return /^\w+:/.test(id);
}

var core; // <- This default instance of the Core will be defined later

// :: "" -> [""] -> (({}, ...) -> ()) -> IO{JEO}
function add(id, deps, factory) {
    if (!isString(id)) {
        // Supporting anonymous definitions
        factory = deps;
        deps = id;
        id = '';
    }
    var me = this,
        isAnonymous = id.length < 1,
        // The current JEO instance being used;
        // note that if calling JEO.add this will
        // not pollute the cached default core
        // instance but a delegated object.
        Y = (!me || me === core || me === Core)
            ? ocreate(core) // Core.add, Core.use are using the default instance
            : me; // Core().use uses the custom instance
    
    // Checking for already available modules
    if (!isAnonymous && (id in repo || depManager.isTracked(id))) {
        ;;; raise('Multiple definitions of module "' + id + '"; please check your dependencies.');
        return Y;
    }
    if (!isArray(deps)) {
        // add('module', function|{})
        factory = deps;
        deps = [];
    }
    if (!isAnonymous && deps.length < 1 && !isFunction(factory)) {
        // add('module1', { contents: {} })
        repo[id] = factory || 1;
        depManager.resolve(id);
        return Y;
    }
    if (isAnonymous) {
        // We need a valid id so we just generate one since
        // coping with empty ids overly complicates things
        id = gid('anon');
    }
    if (!(isString(id) && isArray(deps) && isFunction(factory))) {
        // At this stage the arguments should be normalized
        raise('Error while adding module (', 
            id, ', ', deps, ', ', factory, ')');
    }
    
    // Any rewrites to be done on the dependencies go here;
    // the original purpose is to enable conditional loading
    deps = patchDependencies(deps);
    
    //log('JEO.add:', id, deps, factory);
    function ready(then) {
        //;;; log('  added', id, ':', Array(arguments));
        // Importing the requested dependencies into 
        // the current instance of JEO
        var imports = Y,
            mod = {};

        // Importing the dependencies
        own(deps, function (dep) {
            //mix(imports, repo[dep]);
            //log('    <importing>', dep);  
            var subRepo = repo[dep];
            //for (var i in subRepo) {
            own(subRepo, function (val, key) {
                ;;; if (imports[key]) {
                ;;;     warn('multiple definitions of "', dep, '" in module "' + id + '"!');
                ;;; }
                imports[key] = val;
            });
        });
        
        // After adding dependencies we create a delegated
        // object for being able to tell imports from exports
        var exports = ocreate(imports);

        // Gathering the exports of this module
        // FIXME: Is it a good idea to hand in the global
        //        object as the context of the definition?
        factory.call(/*instance.env.win*/{}, exports);
            
        // Only tracking non-anonymous exports
        if (!isAnonymous) {
            //;;; log('JEO.add: returned =', returned, ', exports =', exports);
            own(exports, function (prop, name) {
                //;;; log('    ' + name + ' = ' + prop);
                mod[name] = prop;
            });

            repo[id] = mod;
            depManager.resolve(id);
        }
        
        // Running continuation if any
        if (isFunction(then)) {
            then();
        }
    }
    
    // The dependency manager now takes care of the request
    depManager.add(id, deps, ready, function (id) {
        // Enabling dynamic loading of dependencies ...
        // The loader respects the configuration of the current instance
        var config = Y.env.config.loader,
            isDynamic = config.dynamic,
            isSignal = testForSignal(id), // e.g.: 'dom:ready'
            isSync = config.sync,
            url = config.baseUrl + id + '.js';
        
        // In case dynamic loading is enabled and the
        // dependency has not been requested before
        // we attempt to load it if it is not a signal
        if (!isSignal && isDynamic && !alreadyRequestedDependencies[id]) {
            alreadyRequestedDependencies[id] = true;
            //log('depManager.requestDependency:', YArray(arguments));
            Y.include({ sync: isSync, url: url });
        }
    });
    return Y;
}
;;; add.dependencies = depManager;
;;; add.repo = repo;

// :: ["", ...]? -> ({JEO} -> {JEO}) -> IO{JEO}
function use(deps, factory) {
    //log('use:', YArray(arguments));
    return add.call(this, deps, factory || nop);
}

// :: {config} -> IO{}
var Core = mix(function (config) {
    // The new instance inherits from the base library;
    // the global configuration will be extended using the
    // given custom one
    var instance = ocreate(base),
        baseConfig = clone(env.config),
        cfg = extend(baseConfig, config || {});
    instance.env = ofreeze(mix(ocreate(env), { config: cfg }));
    return instance;
}, {
    add: add,
    // Including the default configuration with the
    // current environment
    env: env,//ofreeze(env),
    // :: ["", ...]? -> ({JEO} -> {JEO}) -> IO{JEO}
    use: function () {
        return use.apply(core, arguments);
    }
});

defineProperty(Core, 'toString', {
    configurable: false,
    enumerable: false,
    // :: () -> ""
    value: function () {
        return 'You are running ' + LIBRARY_NAME + ' v' + env.jeo;
    }
});

// Adding the base library that will always be included;
// including the Core.prototype in the prototype chain
// for being able to enable (base instanceof Core === true)
var base = ofreeze(mix(ocreate(Core.prototype), {
    Array: YArray,
    Object: YObject,
    Router: Router,
    ajax: ajax,
    all: all,
    clone: clone,
    dedupe: dedupe,
    define: define,
    each: each,
    error: error,
    extend: extend,
    gid: gid,
    include: include,
    info: info,
    isArray: isArray,
    isArrayLike: isArrayLike,
    isFunction: isFunction,
    isString: isString,
    log: log,
    merge: merge,
    mix: mix,
    nop: nop,
    on: on,
    one: one,
    own: own,
    provide: provide,
    raise: raise,
    ready: function (fn) {
        core.use(['dom:ready'], fn);
    },
    typeOf: typeOf,
    use: use,
    warn: warn
}));

var core = Core(); // Default instance

// Just for convenience while debugging
;;; Core.add('jeo', { core: Core, base: base });

// Adding support tests as signals that can be required
each(supports, function (val, key) {
    if (val) {
        Core.add('env:' + key, val);
    }
});

//// Adding support tests that are only safe after the DOM is ready
//core.ready(function () {
//    var qs = 'querySelector',
//        ok = supports[qs] = isNative(doc, qs);
//    if (ok) {
//        Core.add('env:' + qs, true);
//    }
//});

//ofreeze(Core); // TODO: Really use the Core as a namespace?

// Defining a console; IE doesn't define it outside webdev mode
provide('console', console);
provide(LIBRARY_NAME, Core); // Exposing the core

// x-browser "DOMContentLoaded"
(function check() {
    // readyState === 'complete'
    if (doc && /^co/.test(doc.readyState)) {
        return Core.add('dom:ready');
    }
    setTimeout(check, 1);
}());

//var customName = env.config.aka;
//if (env.config.experimental && isString(customName)) {
//    provide(customName, Core); // Exposing the core
//}

// Handing in common language and host objects for easy minimization
}(this, Array, Date, Function, Object, RegExp, String));

;;; var Y = JEO();
;;; var R = Y.Router;































