# JEO.js #
A lightweight library for browsers (c)2012 Martin Feineis.

## Motivation ##

As of November 2012, almost every site on the web uses a fairly big library like 
jQuery for client side scripting. Aware of the fact that only a small percentage 
of the provided functionality is actually needed by frontend engineers led to the 
development of many micro-libraries tailored to specific tasks to reduce the 
necessary overall fingerprint of a website.

This micro-library is an attempt to unify the need for smaller download sizes
with the ability to easily use and scale client applications on demand.
The core API of this library is strongly influenced by YUI while trying to retain
easy accessability of libraries like jQuery. 
It strongly discourages the use of global objects like YUI does but has 
convenient functionality to provide them in a controlled way.
Only the most frequently used functionality is provided by the base library
while having a powerfull dependency management for custom modules at hand
for being able to cope with a larger codebase at the same time.

The library provides a module system similar to YUI and analogue to AMD 
(Asynchronous Module Definition) libraries like `require.js` and the likes. It is 
specifically tailored for usage with browsers in order to reduce file size due to 
unnecessary abstractions.
While mainly being borrowed from YUI's module system it is stripped of the need
of a static module map in favor of a 1-to-1 relationship of modules with 
the filesystem hierarchy also being used by most AMD loaders. Another major
decission is to include imported modules into one single imports object 
like YUI while providing development-time warnings in case of collisions.
This avoids the verbose nature of dependency declaration in most AMD systems.
An obvious trade-off related to this is that for now, we don't support modules being 
functions which is actually kind of odd (acknowledging the functional nature of
JavaScript).

## Goals ##
* A small fingerprint is one of the main goals of the project, the core library
  should only leverage the functionality that is frequently needed.
* No external dependencies.
* Some ECMAScript5 normalization for common functionality like [].forEach that
  is needed for the library base.
* Avoid some common issues with IE, especially usage of the global console should
  be safe even when not in web-dev mode.
* Support for recent Chrome/Chromium, Firefox, Safari, Opera and IE
* Legacy support only for IE8 (default browser of Windows7)
* An easy to use but solid module system to leverage large scale applications.
* Sandboxing of functionality due to the API design; it should be more difficult
  to use global variables and tight coupling - hopefully leading to better code
  quality.
* The library's root object can be used as a namespace to avoid global pollution.
* Usage of feature detection within the current environment is emphasized by 
  explicitly not providing library hooks to specific browsers.
* Easy configuration of sandboxed instances.
* Minimal public API regarding to the library root - library functionality
  is encouraged to be used within a sandbox.
* Full unit-testing-coverage in modern browsers.
* Gracefull degradation for older browsers; we only include polyfills for very
  necessary stuff like some ES5 shims that make life much easier.
* Unexpected overloading should be kept to a minimum - in contrast to the vast 
  shorthand possibilities to use the jQuery root function for example.

## Unforseen thingys ##
* With the coding style being pretty messy up until version 0.1.7 an attempt has
  been made to build a preprocessor that should do exactly what had been done
  by hand. The outcome of this experiment, the munger.js, does pretty well. While
  we're able to code in a normal js way the script will count chars trying to
  minimize the overall footprint of the file. It looks at property access and
  string literals and estimates the amount of chars being saved by munging
  specific properties into string literals being reused all over the file.
  However it is specifically tailored to this library, some preconditions must
  be met for being able to use the munger successfully:
  + The first _var_ statement has to be within the enclosing closure
  + Additions to string literals have to be avoided because they could be
    munged into one shared instance messing the functionality up
  + No code should be outside the main closure to avoid scope issues
* DOM traversing using querySelector API does work in IE8 so we can take advantage
  of advanced css selectors, at least up until CSS2.1

## Intentional trade-offs ##
* The library is not a one-fits-all solution like YUI - it is not meant to cover 
  all possible functionality commonly exposed by other big libaries; for being able 
  to do some advanced things it might be necessary to include additional libraries.
* The library is meant to be used with some kind of build tool like YUICompressor
  so if your toolchain doesn't contain something like that it might not be as
  lightweight as it claims to be :).
* Modules can not be functions due to the way imports and exports are handled.
* The usage of MVC/MVP/MVVM libraries is encouraged by not duplicating functionality 
  that is very likely to be incorporated by external libraries like Backbone.js,
  jQuery or KnockoutJS.





















