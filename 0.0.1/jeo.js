// JEO library
// - ES5 compatibility inspired by traits.js
(function (global, undefined) {
    
//////// Environment setup
    
var env = {
    isAMD: typeof define !== 'undefined' && define.length && define.amd,
    isBrowser: typeof window !== 'undefined',
    isCommonJS: typeof module !== 'undefined' && module.exports,
    undefined: undefined
};

env.doc = global.document;
env.win = global;

env.SUPPORTS_STRICTMODE = (function () {
    'use strict';
    return !this;
}());
env.SUPPORTS_DECOMPILATION = /z/.test(function(){z});
env.SUPPORTS_DEFINEPROPERTY = (function () {
    var o = {};
    try {
        Object.defineProperty(o, 'one', { get: function () { return 1; } })
        return o.one === 1;
    } catch(_) {
        return false;
    }
}());
// IE8 implements Object.defineProperty and Object.getOwnPropertyDescriptor
// only for DOM objects. These methods don't work on plain objects.
// Hence, we need a more elaborate feature-test to see whether the
// browser truly supports these methods:
env.SUPPORTS_GETOWNPROPERTY = (function () {
    try {
        if (Object.getOwnPropertyDescriptor) {
            var test = { x: 0 };
            return !!Object.getOwnPropertyDescriptor(test,'x');        
        }
    } catch(e) {}
    return false;
}());


//////// ES5 compatibility polyfills

// :: {} -> "" -> bool
function isNative(o, name) {
    var ex = typeof o[name] !== undefined;
    return ex && isNative.fn(o[name]);
}
isNative.fn = env.SUPPORTS_DECOMPILATION ? function (fn) {
        return /\s*[\s*native\s+code\s*]\s*/i.test(fn);
    }
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        return true;
    };
    
var AProto = Array.prototype;

if (!isNative(AProto, 'forEach')) {
    AProto.forEach = function (fn/*, ctx*/) {
        ctx = arguments[1] || this;
        var len = this.length;
        for (var i = 0; i < len; i++) {
            fn.call(ctx, this[i], i, ctx);
        }
    };
}
    
if (!isNative(Function.prototype, 'bind')) {
    Function.prototype.bind = function bind(ctx) {
        var fn = this;
        return function () {
            fn.apply(ctx, arguments);
        };
    };
}

if (!isNative(Object, 'create')) {
    Object.create = (function () {
        function F() {}
        return function create(proto, props) {
            F.prototype = proto;
            var o = new F;
            if (props) {
                Object.defineProperties(o, props);
            }
            return o;
        };
    }());
}

if (!isNative(Date, 'now')) {
    Date.now = function () {
        return (new Date).getTime();
    };
}

if (!env.SUPPORTS_DEFINEPROPERTY) {
    Object.defineProperty = function defineProperty(o, name, descr) {
        o[name] = descr.value;
    };
}

if (!env.SUPPORTS_DEFINEPROPERTY) {
    Object.defineProperties = function defineProperties(object, descriptors) {
        each(map, function (descriptor, property) {
            Object.defineProperty(object, property, descriptor);
        });
    };
}

if (!isNative(Object, 'freeze')) {
    Object.freeze = function (o) { return o; }; // Impossible in ES3
}

if (!env.SUPPORTS_GETOWNPROPERTY) {
    Object.getOwnPropertyDescriptor = function getOwnPropertyDescriptor(o, name) {
        return {
            value: o[name],
            enumerable: true,
            writable: true,
            configurable: true
        };
    };
}

if (!isNative(Object, 'keys')) {
    Object.keys = function keys(o) {
        var result = [];
        each(o, function (_, name) {
            result.push(name);
        });
        return result;
    };
}

if (!isNative(String.prototype, 'trim')) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/, '');
    };
}


//////// Base functionality

var YArray = function Array(arr) {
    return Array.from((arguments.length > 1) ? arguments : arr);
};
YArray.from = function from(arr) {
    return AProto.slice.call(arr);
};

var console = (typeof global.console !== 'undefined') ? global.console : {
    error: nop,
    info: nop,
    log: nop,
    warn: nop
};

function each(o, fn, ctx) {
    ctx = ctx || o;
    var i;
    if (isArray(o)) {
        o.forEach(fn, ctx);
    }
    else {
        for (i in o) {
            fn.call(ctx, o[i], i, o);
        }
    }
}
    
function introduce(id, o) {

    if (env.isAMD) {
        // AMD compliant define
        define(id.toLowerCase(), [], function () { return o; });
    }
    else if (env.isCommonJS) {
        // CommonJS environment
        module.exports = o;
    }
    else if (env.isBrowser) {
        // Browser
        global[id] = o;
    }
    else {
        raiseError('Unknown environment.');
    }
}

function isArray(o) {
    return typeOf(o) === 'array';
}

function isFunction(o) {
    return typeOf(o) === 'function';
}

function isString(o) {
    return typeof o === 'string';
}

function log() {
    internal.syslog.push({ time: Date.now(), message: AProto.slice.call(arguments) });
    console.log.apply(console, arguments);
}

function mix(to, from) {
    if (from === undefined) {
        from = to;
        to = this;
    }
    for (var i in from) {
        to[i] = from[i];
    }
    return to;
}

function nop() {}

function raiseError(msg) {
    console.error(msg);
    throw msg;
}

function typeOf(o) {
    if (o === null) {
        return 'null';
    }
    if (o === undefined) {
        return 'undefined';
    }
    
    var t = (typeof o)
        .replace(/\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1')
        .toLowerCase();
    
    if (t === 'object' && o.constructor === RegExp) {
        return 'regexp';
    }
    return t;
}


//////// JEO core

var config = {
    defaultModules: []
};

var modules = {};

var internal = {
    config: config,
    modules: modules,
    syslog: []
};

function JEO(config) {
    var jeo = Object.create(JEO.prototype),
        userConfig = config;
    // ... apply configuration
    config = Object.create(internal.config);
    if (userConfig) {
        config = mix(config, userConfig);
    }
    jeo.config = config;
    return jeo;
}
JEO.prototype = {
    env: Object.create(env),
    use: use
};

var BASE_REV = '$';
var HEAD_REV = 'HEAD';
var VERSION_SEP = '@';

function decompose(id) {
    var s = id.split(VERSION_SEP);
    return { name: s[0], version: s[1] || HEAD_REV };
}

function add(id, module) {
    var meta = decompose(id);
    name = meta.name;
    
    if (!modules[name]) {
        modules[name] = function getModule(version) {
            version = (version === HEAD_REV) ? getModule[HEAD_REV] : version;
            var module = getModule[version];
            if (!module) {
                raiseError('Module ' + name + ' not ready in version ' + version + '.');
            }
            return module;
        };
        modules[name][HEAD_REV] = BASE_REV;
    }
    
    var ver = (meta.version === HEAD_REV) ? modules[name][HEAD_REV] : meta.version;
    modules[name][ver] = module;
    if (ver > modules[name].HEAD) {
        modules[name][HEAD_REV] = ver;
    }
}

function use(factory) {
    var args = AProto.slice.call(arguments),
        len = args.length,
        i;
    
    // Including base library 
    var baseImport = Object.create(this);
        
    each(config.defaultModules, function (module) {
        var mod = decompose(module);
        if (!modules[mod.name]) {
            raiseError('Module ' + mod.name + ' unknown.');
        }
        mix(baseImport, modules[mod.name](mod.version));
    });
    
    // Importing requested modules
    var imports = [baseImport];
    
    factory = args[len-1];
    factory.apply(null, imports);
}

mix(JEO, {
    _: internal,
    add: add,
    use: function use(factory) {
        return JEO().use.apply(null, arguments);
    },
    env: env
});

JEO.add('jeo@0.0.1', JEO);


//////// Base library

JEO.add('jeo-base@0.0.1', {
    Array: YArray,
    each: each,
    introduce: introduce,
    isArray: isArray,
    isFunction: isFunction,
    isString: isString,
    log: log,
    mix: mix,
    nop: nop,
    raise: raiseError,
    typeOf: typeOf
});
config.defaultModules.push('jeo-base');


//////// Exposing library core

introduce('JEO', JEO);


}(typeof window !== 'undefined' ? window : global));

























