/*! JEO.js - A lightweight default environment for browsers. (c)2012 Martin Feineis */
// Credits to jQuery, YUI, PrototypeJS, Closure Library and many more for inspiration.
// The core library is optimized for source code optimizers like YUICompressor by providing
// shorthands for commonly used property names
(function (global, console, Array, Date, Function, Object, RegExp, String, undefined) {
'use strict';

// Common property names will be munged
// throughout the build process
var APPLY = 'apply',
    ARRAY = 'array',
    ASYNC = 'async',
    BIND = 'bind',
    CALL = 'call',
    CONSTRUCTOR = 'constructor',
    CREATE = 'create',
    CREATEELEMENT = 'createElement',
    DEFER = 'defer',
    DEFINE = 'define',
    DEFINEPROPERTIES = 'defineProperties',
    DEFINEPROPERTY = 'defineProperty',
    EXTEND = 'extend',
    FIRSTCHILD = 'firstChild',
    FOREACH = 'forEach',
    FREEZE = 'freeze',
    GETOWNPROPERTYDESCRIPTOR = 'getOwnPropertyDescriptor',
    HAS = 'hasOwnProperty',
    INSERTBEFORE = 'insertBefore',
    ISARRAY = 'isArray',
    KEYS = 'keys',
    LENGTH = 'length',
    MIXINS = 'mixins',
    MOZWEBSOCKET = 'MozWebSocket',
    NOW = 'now',
    OBJECT = 'object',
    PROTO = 'prototype',
    SETATTRIBUTE = 'setAttribute',
    GETATTRIBUTE = 'getAttribute',
    TOSTRING = 'toString',
    TRIM = 'trim',
    UNDEFINED = 'undefined',
    WEBSOCKET = 'WebSocket';

// Happy console world :)
console = (!!console) 
    ? console
    : { log: nop, info: nop, error: nop, warn: nop };
    
//// Detecting Asynchronous Module Definition compliant resource loaders
//var amdDefine = typeof global[DEFINE] !== UNDEFINED
//    ? global[DEFINE]
//    : null;

// Thanks to PrototypeJS
var DONT_ENUMS = [TOSTRING, 'toLocaleString', 'valueOf', HAS, 
        'isPrototypeOf', 'propertyIsEnumerable', CONSTRUCTOR],
       
    doc = global.document,
    env = {
        // Just for convenience; should not be necessary outside the core library itself
        doc: doc, 
        win: global
//        isAMD: !!(amdDefine && amdDefine['length'] && amdDefine['amd']),
//        isBrowser: (typeof window !== UNDEFINED),
//        isCommonJS: !!(typeof module !== UNDEFINED && module['exports'] && require),
        
    },
    HEAD_ELEM = one('head'), // TODO: Will IE accept this?
    sandbox = {},
    slice = [].slice, // Native Array.prototype.slice
    toString = {}.toString; // Native Object.prototype.toString
        
//// Feature detection for the current environment
var supports = env.supports = {
    // Some versions of JScript fail to enumerate over properties, names of which 
    // correspond to non-enumerable properties in the prototype chain
    correctEnumeration: (function(){
        for (var p in { toString: 1 }) {
            // check actual property name, so that it works with augmented Object.prototype
            if (p === TOSTRING) {
                return true;
            }
        }
        return false;
    }()),
    // Detects function decompilation capabilities
    functionDecompilation: /xyz/.test(function(){xyz}),
    // Detects whether or not strict mode is available
    strictMode: (function () {
        'use strict';
        return !this;
    }()),
    websocket: true,
    xhr: true
};
// Detects ES5 defineProperty capabilities
supports[DEFINEPROPERTY] = (function () {
    var o = {};
    try {
        Object[DEFINEPROPERTY](o, 'one', { get: function () { return 1; } })
        return 1 === o.one;
    } catch(e) {
        return false;
    }
}());
// IE8 implements Object.defineProperty and Object.getOwnPropertyDescriptor
// only for DOM objects. These methods don't work on plain objects.
// Hence, we need a more elaborate feature-test to see whether the
// browser truly supports these methods:
supports[GETOWNPROPERTYDESCRIPTOR] = (function () {
    try {
        if (Object[GETOWNPROPERTYDESCRIPTOR]) {
            var test = {x:0};
            return !!Object[GETOWNPROPERTYDESCRIPTOR](test,'x');        
        }
    } 
    catch(e) {}
    return false;
}());


//// Environment normalization

// Detects whether a the property `name` is native
// to the given object `o`; only works properly in
// environments that support function decompilation
// :: {} -> string -> boolean
function isNative(o, name) {
    var ex = typeof o[name] !== UNDEFINED;
    return ex && isNative.fn(o[name]);
    //return false;
}
isNative.fn = supports.functionDecompilation
  // :: ({} -> {}) -> boolean
  ? function (fn) {
        return /[\s*native\s+code\s*]/i.test(fn);
    }
  // :: () -> boolean
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        // and hope for the best
        // TODO: Maybe we should rely on our implementation?
        return true;
    };

if (!isNative(Array[PROTO], FOREACH)) {
    // :: ({} -> string|number -> {}) -> {}? -> ()
    Array[PROTO][FOREACH] = function (fn/*, ctx*/) {
        var ctx = arguments[1] || this,
            len = this[LENGTH];
        for (var i = 0; i < len; i++) {
            //console.log('each:', i, this[i]);
            fn[CALL](ctx, this[i], i, this);
        }
    };
}
var arrayForEach = Array[PROTO][FOREACH];

if (!isNative(Array, ISARRAY)) {
    Array[ISARRAY] = function isArray(o) {
        return typeOf(o) === ARRAY;
    };
}

if (!isNative(Function[PROTO], BIND)) {
    // :: {} -> function
    Function[PROTO][BIND] = function bind(ctx) {
        var args = slice[CALL](arguments, 1),
            fn = this;
        return function () {
            var as = merge(args, arguments);
            return fn.apply(ctx, as);
        };
    };
}

if (!isNative(Object, CREATE)) {
    // :: {} -> {} -> {}
    Object[CREATE] = (function () {
        function F() {}
        return function create(proto, props) {
            F[PROTO] = proto;
            var o = new F;
            if (props) {
                Object[DEFINEPROPERTIES](o, props);
            }
            return o;
        };
    }());
}

if (!isNative(Date, NOW)) {
    // :: () -> number
    Date[NOW] = function now() {
        // Thanks to the Closure library
        // (unary+) calls getTime() on Date
        return +new Date; 
    };
}

if (!supports[DEFINEPROPERTY]) {
    // :: {} -> string -> { value: {} } -> ()
    Object[DEFINEPROPERTY] = function (o, name, descr) {
        o[name] = descr.value;
    };
    // :: {} -> [{ value: {} }] -> ()
    Object[DEFINEPROPERTIES] = function (object, descriptors) {
        own(descriptors, function (descriptor, property) {
            Object[DEFINEPROPERTY](object, property, descriptor);
        });
    };
}

if (!isNative(Object, FREEZE)) {
    // :: {} -> {}
    Object[FREEZE] = function (o) { return o; }; // Impossible in ES3
}

if (!supports[GETOWNPROPERTYDESCRIPTOR]) {
    // :: {} -> string -> { value: {}, enumerable, writable, configurable: boolean }
    Object[GETOWNPROPERTYDESCRIPTOR] = function (o, name) {
        return {
            value: o[name],
            enumerable: true,
            writable: true,
            configurable: true
        };
    };
}

if (!isNative(Object, KEYS)) {
    // :: {} -> [string]
    Object[KEYS] = function (o) {
        var result = [];
        own(o, function (_, name) {
            result.push(name);
        });
        return result;
    };
}

if (!isNative(String[PROTO], TRIM)) {
    // :: () -> string
    String[PROTO][TRIM] = function () {
        return this.replace(/^\s+|\s+$/, '');
    };
}

// :: string -> ((string...) -> ())
function wrapConsole(name) {
    var fn = console[name];
    return function () {
        try {
            fn[APPLY](console, arguments);
        }
        catch (e) {
            // Nothing ...
        }
    };
}

// X-Browser XmlHttpRequest
// :: new () -> {}
var Xhr = (function () {
    var Xhr,
        legacy = [
            'Msxml2.XMLHTTP.6.0',
            'Msxml2.XMLHTTP.3.0',
            'Microsoft.XMLHTTP',
            'Msxml2.XMLHTTP'
        ],
        len = legacy[LENGTH],
        i;

    // This is for all reasonable browsers
    if (XMLHttpRequest) {
        return XMLHttpRequest;
    } 

    // Testing for legacy http requests
    for (i = 0; i < len; i++) {
        Xhr = function () {
            var id = legacy[i];
            return new ActiveXObject(id);
        };
        try {
            new Xhr;
            return Xhr;
        } 
        catch (e) {} 
        finally {
            Xhr = null;
        }
    }
    // O.o - epic fail
    supports.xhr = false;
    raise('XMLHttpRequest not available.');
}());

// Base library

// :: []|{ length: number } -> []
var YArray = function (arr) {
    return typeof arr !== UNDEFINED ? slice[CALL](arr) : [];
};

// :: new (string) -> {}
var YWebSocket = (function () {
    // Checking for WebSocket implementations
    if (WEBSOCKET in global) {
        return global[WEBSOCKET];
    }
    if (MOZWEBSOCKET in global) {
        return global[MOZWEBSOCKET];
    }
    // OMG it's ancient :)
    supports.websocket = false;
    return function NotSupported() {
        // Providing a dummy to remind the user of
        // checking for support in the first place
        raise('WebSocket not supported by this browser.');
    };
}());

// :: { data?: {}, method?: string, sync: boolean, url: string } -> (string -> ())
function ajax(config, ok) {
    //console.log('net.get', arguments);
    if (isString(config)) {
        config = { url: config };
    }
    var xhr = new Xhr,
        url = config.url,
        async = !config.sync,
        data = config.data || null,
        method = config.method || 'GET',
        params = [];

    own(config.params || {}, function (val, key) {
        //log('param', key, val);
        params.push(escape(key) + '=' + escape(val));
    });
    if (params[LENGTH] > 0) {
        url = url + '?' + params.join('&');
    }
    xhr.onreadystatechange = function (e) {
        //console.log('  xhr.readystatechange', xhr, arguments);
        var status = xhr.status;
        if (xhr.readyState === 4) {
            if (status === 0 || (status >= 200 && status < 300)) {
                ok(xhr.responseText);
            }
            else {
                raise('Request to "' + url + '" failed.');
            }
            xhr = null;
        }
    };
    try {
        //log(url);
        xhr.open(method.toUpperCase(), url, async);
        xhr.send(data);
        return true;
    }
    catch (e) {
        return false;
    }
}

// :: string -> boolean
function available(s) {
    return !!(global[s] || provide(s, null, 1));
}

// :: string? -> { 
//      constructor: new (...) -> {},
//      extend: function,
//      prototype: {},
//      statics: {}
// } -> function
function define(name, o) {
    if (!o) {
        o = name;
        name = '';
    }
    var Type = o[CONSTRUCTOR] || function () {},
        Base = o[EXTEND] || Object,
        proto = o[PROTO] || {},
        statics = o.statics || {};
    Type[PROTO] = Object[CREATE](Base[PROTO]);
    extend(Type[PROTO], proto);
    Type[PROTO][CONSTRUCTOR] = Type;
    //each(o[MIXINS], function (m) {
    //    extend(Type[PROTO], m);
    //});
    extend(Type, Base);
    extend(Type, statics);
    if (name[LENGTH] > 0) {
        provide(name, Type);
    }
    return Type;
}

// {} -> ({} -> string -> ()) -> {}? -> ()
function each(o, fn, ctx/*, internal justOwn*/) {
    ctx = ctx || o;
    var i, j, propName, 
        justOwn = arguments[3];
    
    //if (!o) {
    //    //Y.info('each', arguments);
    //    return;
    //}
    if (isArray(o) || isArrayLike(o)) {
        //o[FOREACH](fn, ctx);
        arrayForEach[CALL](o, fn, ctx);
    }
    else {
        for (i in o) {
            if (!justOwn || o[HAS](i)) {
                fn[CALL](ctx, o[i], i, o);
            }
        }
        if (!supports.correctEnumeration) {
            for (j = DONT_ENUMS[LENGTH]-1; j >= 0; j--) {
                propName = DONT_ENUMS[j];
                if (o[propName] && (!justOwn || o[HAS](j))) {
                    fn[CALL](ctx, o[propName], propName, o);
                }
            }
        }
    }
}

var error = wrapConsole('error');

// :: {0} -> {} -> {0}
function extend(to, from/*,internal level*/) {
    if (arguments[LENGTH] < 2) {
        from = to;
        to = this;
    }
    //var level = arguments[2] || 0;
    //var tab = '';
    //for (var j = 0; j < level; j++) {
    //    tab += '  ';
    //}
    each(from, function (val, i) {
        //console.log(tab, 'extend: ', i, ': ', val, '=>', to[i]);
        var deep = false;
        switch (typeOf(val)) {
        case ARRAY:
            //console.log(tab, 'extend.array: ', i);
            if (!to[i]) {
                //console.log(tab, 'extend.array new: ', i, ': ', val, '=>', to[i]);
                to[i] = [];
            }
            //else {
            //    console.log(tab, 'extend.array extend: ', i, ': ', val, '=>', to[i]);
            //}
            deep = (val[LENGTH] > 0); // No magic, just in case ...
            break;
        case OBJECT:
            if (!to[i]) {
                to[i] = {};
            }
            deep = true;
            break;
        }
        if (deep) {
            //console.log(tab, 'extend.deep: ', i, ': ', val, '=>', to[i]);
            extend(to[i], val);//, level+1);
        }
        else {
            to[i] = val;
        }
    });
    //console.log(tab, 'ok:', to);
    return to;
}

var gidSeed = 0;
// :: string -> string
function gid(prefix) {
    return '' + (prefix || 'gid') + (gidSeed++);
}

// :: { sync?: boolean, url: string }|string -> ()
function include(opt) {
    if (isString(opt)) {
        opt = { url: opt };
    }
    var resType = 'js', // Defaulting to JavaScript files
        async = !opt.sync,
        url = opt.url;
    // Searching for actual file extension
    url.replace(/\.([^?]+)(?:\?.*)?$/, function (m, ext) {
        resType = ext.toLowerCase();
    });
    switch (resType) {
    case 'js':
        var script = doc[CREATEELEMENT]('script');
        if (async) {
            script[SETATTRIBUTE](ASYNC, ASYNC);
            script[SETATTRIBUTE](DEFER, DEFER);
        }
        script[SETATTRIBUTE]('src', url);
        HEAD_ELEM[INSERTBEFORE](script, HEAD_ELEM[FIRSTCHILD]);
        return;
    case 'css':
        var style = doc[CREATEELEMENT]('link');
        style[SETATTRIBUTE]('rel', 'stylesheet');
        style[SETATTRIBUTE]('href', url);
        HEAD_ELEM[INSERTBEFORE](style, HEAD_ELEM[FIRSTCHILD]);
        return;
    default:
        raise('Resource type "' + resType + '" not supported.');
        return;
    }
}

var info = wrapConsole('info');

// :: {} -> boolean
var isArray = Array[ISARRAY];

// :: {} -> boolean
function isArrayLike(o) {
    switch (typeOf(o)) {
    case 'arguments': // Fall through
    case 'array': // Fall through
    case 'nodelist':
        return true;
    default:
        return false;
    }
}

// :: {} -> boolean
function isFunction(o) {
    return typeOf(o) === 'function';
}

// :: {} -> boolean
function isString(o) {
    return typeof o === 'string';
}

var log = wrapConsole('log');

// :: [] -> [] -> []
function merge(as, bs) {
    var ls = slice[CALL](as);
    Y.own(bs, function (b) {
        ls.push(b);
    });
    return ls;
}

// :: {0} -> {} -> {0}
function mix(to, from) {
    if (arguments[LENGTH] < 2) {
        from = to;
        to = this;
    }
    //for (var i in from) {
    each(from, function (value, i) {
        to[i] = value;
    });
    return to;
}

// :: () -> ()
function nop() {}

// :: string -> {dom}
function one(id) {
    switch (id[0]) {
    case '#':
        return doc.getElementById(id.replace('#', ''));
    default:
        return doc.getElementsByTagName(id)[0];
    }
}

// :: {} -> ({} -> ()) -> {}? -> ()
function own(o, fn, ctx) {
    return each[CALL](this, o, fn, ctx, 1);
}

// :: string -> {0} -> {0}
function provide(name, o/*, internal justDetect*/) {
    o = o || {};
    var host = global,
        ls = name.split('.'),
        justDetect = arguments[2],
        failed = false,
        len = ls[LENGTH],
        i;
    for (i = 0; i < len-1; i++) {
        host = host[ls[i]];
    }
    if (justDetect) {
        //log('failed:', failed, ', host[ls[i]]:', !!host[ls[i]]);
        if (!host) {
            return false;
        }
        return failed || !!host[ls[i]];
    }
    if (failed) {
        raise('"' + name + '" is unknown.');
    }
    sandbox[name] = o;
    return (host[ls[i]] = o);
}

// :: string -> ()
function raise(message) {
    error[APPLY](console, arguments);
    throw message;
}

// :: {} -> string
function typeOf(o) {
    if (o === null) {
        return 'null';
    }
    if (o === undefined) {
        return UNDEFINED;
    }
    // Extracting type from "[object ...]"
    var t = toString[CALL](o)
        .replace(/\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1')
        .toLowerCase();
    
    return t;
}

// :: ({JEO} -> ()) -> ()
function use(fn) {
    var args = arguments,
        len = args[LENGTH],
        deps = slice[CALL](args, 0, len-1);

    deps[FOREACH](function (id) {
        if (!available(id)) {
            raise('Dependency "' + id + '" not met!');
        }
    });
    args[len-1][CALL](null, this);
}

var warn = wrapConsole('warn');

// Library core

// :: () -> {}
var Core = mix(function (config) {
    var me = Object[CREATE](base);
    me.config = extend(Object[CREATE](Core.config), config);
    return me;
}, {
    /** The default configuration */
    config: {
        a: 'b'
    }, 
    // :: () -> string
    toString: function () {
        return 'You are running JEO v' + this.version;
    },
    // :: ({JEO} -> ()) -> ()
    use: function () {
        return core.use[APPLY](core, arguments);
    },
    // :: string
    version: '0.0.18'
});

// Adding the base library that will always be included
var base = Object[FREEZE](mix(Object[CREATE](Core[PROTO]), {
    Array: YArray,
    WebSocket: YWebSocket,
    ajax: ajax,
    available: available,
    define: define,
    each: each,
    env: Object[FREEZE](env),
    error: error,
    extend: extend,
    gid: gid,
    include: include,
    info: info,
    isArray: isArray,
    isArrayLike: isArrayLike,
    isFunction: isFunction,
    isString: isString,
    log: log,
    merge: merge,
    mix: mix,
    nop: nop,
    one: one,
    own: own,
    provide: provide,
    raise: raise,
    typeOf: typeOf,
    use: use,
    warn: warn
}));

//Object[FREEZE](Core); // TODO: Really use JEO as a namespace?

var core = Core(); // Default core instance

provide('console', console); // Defining a console; IE doesn't define it outside webdev mode
provide('JEO', Core); // Exposing the core

}(this, typeof console !== 'undefined' ? console : null, Array, Date, Function, Object, RegExp, String));
// Handing in common language and host objects to easily enable minimization
































