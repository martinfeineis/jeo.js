/*! JEO library (c) 2012 Martin Feineis */
// ES5 compatibility partly taken from traits.js (www.traitsjs.org)
// DONT_ENUMS stuff taken from PrototypeJS
// isNumber taken from jQuery
(function (global, undefined) {
'use strict';
    
//////// Environment setup
    
var globalDefine = global['define'];
var UNDEFINED = 'undefined';
    
var env = {
    doc: global.document,
    isAMD: !!(typeof globalDefine !== UNDEFINED && globalDefine['length'] && globalDefine['amd']),
    isBrowser: (typeof window !== UNDEFINED),
    isCommonJS: !!(typeof module !== UNDEFINED && module['exports'] && require),
    win: global
};

// Console shim
var console = (typeof global.console !== UNDEFINED) ? global.console : {
    assert: nop,
    error: nop,
    info: nop,
    log: nop,
    trace: nop,
    warn: nop
};

var NativeArray = global.Array,
    NativeObject = global.Object;

// Thanks to PrototypeJS
var DONT_ENUMS = ['toString', 'toLocaleString', 'valueOf',
   'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable', 'constructor'];

// Some versions of JScript fail to enumerate over properties, names of which 
// correspond to non-enumerable properties in the prototype chain
env.IS_DONTENUM_BUGGY = (function(){
    for (var p in { toString: 1 }) {
        // check actual property name, so that it works with augmented Object.prototype
        if (p === 'toString') return false;
    }
    return true;
}());

env.SUPPORTS_STRICTMODE = (function () {
    'use strict';
    return !this;
}());
env.SUPPORTS_DECOMPILATION = /z/.test(function(){z});
env.SUPPORTS_DEFINEPROPERTY = (function () {
    var o = {};
    try {
        NativeObject.defineProperty(o, 'one', { get: function () { return 1; } })
        return o.one === 1;
    } catch(_) {
        return false;
    }
}());
// IE8 implements Object.defineProperty and Object.getOwnPropertyDescriptor
// only for DOM objects. These methods don't work on plain objects.
// Hence, we need a more elaborate feature-test to see whether the
// browser truly supports these methods:
env.SUPPORTS_GETOWNPROPERTY = (function () {
    try {
        if (NativeObject.getOwnPropertyDescriptor) {
            var test = { x: 0 };
            return !!NativeObject.getOwnPropertyDescriptor(test, 'x');        
        }
    } catch(e) {}
    return false;
}());


//////// ES5 compatibility polyfills

// :: {} -> "" -> bool
function isNative(o, name) {
    var ex = typeof o[name] !== UNDEFINED;
    return ex && isNative.fn(o[name]);
}
isNative.fn = env.SUPPORTS_DECOMPILATION ? function (fn) {
        return /\s*[\s*native\s+code\s*]\s*/i.test(fn);
    }
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        return true;
    };
    
if (!isNative(NativeArray.prototype, 'forEach')) {
    NativeArray.prototype.forEach = function (fn/*, ctx*/) {
        var ctx = arguments[1] || this;
        var len = this.length;
        for (var i = 0; i < len; i++) {
            //console.log('each:', i, this[i]);
            fn.call(ctx, this[i], i, ctx);
        }
    };
}
    
if (!isNative(Function.prototype, 'bind')) {
    Function.prototype.bind = function bind(ctx) {
        var fn = this;
        return function () {
            fn.apply(ctx, arguments);
        };
    };
}

if (!isNative(NativeObject, 'create')) {
    NativeObject.create = (function () {
        function F() {}
        return function create(proto, props) {
            F.prototype = proto;
            var o = new F();
            if (props) {
                NativeObject.defineProperties(o, props);
            }
            return o;
        };
    }());
}

if (!isNative(Date, 'now')) {
    Date.now = function () {
        return (new Date).getTime();
    };
}

if (!env.SUPPORTS_DEFINEPROPERTY) {
    NativeObject.defineProperty = function defineProperty(o, name, descr) {
        o[name] = descr.value;
    };

    NativeObject.defineProperties = function defineProperties(object, descriptors) {
        own(descriptors, function (descriptor, property) {
            NativeObject.defineProperty(object, property, descriptor);
        });
    };
}

if (!isNative(NativeObject, 'freeze')) {
    NativeObject.freeze = function (o) { return o; }; // Impossible in ES3
}

if (!env.SUPPORTS_GETOWNPROPERTY) {
    NativeObject.getOwnPropertyDescriptor = function getOwnPropertyDescriptor(o, name) {
        return {
            value: o[name],
            enumerable: true,
            writable: true,
            configurable: true
        };
    };
}

if (!isNative(NativeObject, 'keys')) {
    NativeObject.keys = function keys(o) {
        var result = [];
        own(o, function (_, name) {
            result.push(name);
        });
        return result;
    };
}

if (!isNative(String.prototype, 'trim')) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/, '');
    };
}

var logMessage = {
    create: function (type, message) {
        var o = NativeObject.create(logMessage);
        o.message = message;
        o.time = Date.now();
        o.type = type;
        internal.syslog.push(o);
        return o;
    },
    toString: function toString() {
        return this.type + ': ' + this.message;
    }
};

var toString = {}.toString;

//////// Base functionality

var SELF_NAME = 'constructor',
    BASE_NAME = 'base';

function define(name, opt) {
    var asNamespace = true;
    if (arguments.length < 2) {
        opt = name;
        name = '';
        asNamespace = false;
    }
    var Self = opt[SELF_NAME];
    if (!Self || Self === NativeObject) {
        Self = function () {};
    }
        
    var proto = NativeObject.create(isFunction(opt[BASE_NAME]) ? opt[BASE_NAME].prototype : null),
        plugins = define;
        
    own(opt, function (val, key) {
        var plugin = plugins[key];
        if (plugin) {
            plugin(Self, proto, val, opt);
        }
    });
    proto[SELF_NAME] = Self;
    Self.prototype = proto;
    
    if (asNamespace) {
        //namespace(global, name, Self);
        introduce(name, Self);
    }
    return Self;
}
mix(define, {
    //alias: function (Self, proto, cmd, all) {
    //    var ls = isArray(cmd) ? cmd : [cmd];
    //    each(ls, function (val) {
    //        introduce(val, Self);
    //    });
    //},
    members: function (Self, proto, cmd, all) {
        (isArray(cmd) ? cmd : [cmd]).forEach(function (o) {
            extend(proto, o);
        });
    },
    statics: function (Self, proto, cmd, all) {
        (isArray(cmd) ? cmd : [cmd]).forEach(function (o) {
            extend(Self, o);
        });
    }
});
define[BASE_NAME] = nop; // Already included
define[SELF_NAME] = nop; // Already included

function each(o, fn, ctx) {
    ctx = ctx || o;
    var i, j, propName;
    if (isArray(o)) {
        o.forEach(fn, ctx);
    }
    else {
        for (i in o) {
            fn.call(ctx, o[i], i, o);
        }
        if (env.IS_DONTENUM_BUGGY) {
            for (j = DONT_ENUMS.length-1; j >=0; j--) {
                propName = DONT_ENUMS[j];
                if (o[propName]) {
                    fn.call(ctx, o[propName], propName, o);
                }
            }
        }
    }
}

function error() {
    logMessage.create('error', YArray(arguments));
    if (console.error.apply) {
        console.error.apply(console, arguments);
    }
}

function extend(to, from/*,internal level*/) {
    if (arguments.length < 2) {
        from = to;
        to = this;
    }
    //var level = arguments[2] || 0;
    //var tab = '';
    //for (var j = 0; j < level; j++) {
    //    tab += '  ';
    //}
    each(from, function (val, i) {
        //console.log(tab, 'extend: ', i, ': ', val, '=>', to[i]);
        var deep = false;
        switch (typeOf(val)) {
        case 'array':
            if (!to[i]) {
                to[i] = [];
            }
            deep = (val.length > 0);
            break;
        case 'object':
            if (!to[i]) {
                to[i] = {};
            }
            deep = true;
            break;
        }
        if (deep) {
            extend(to[i], val);//, level+1);
        }
        else {
            to[i] = val;
        }
    });
    //console.log(tab, 'ok:', to);
    return to;
}

function info() {
    logMessage.create('info', YArray(arguments));
    if (console.info.apply) {
        console.info.apply(console, arguments);
    }
}
    
function introduce(id, o) {
    var defID;
    if (env.isAMD) {
        // AMD compliant define
        defID = id.replace(/\W/g, '/')
            .replace(/([a-z])([A-Z])/g, '$1-$2')
            .toLowerCase();
        globalDefine(defID, [], function () { return o; });
    }
    if (env.isBrowser) {
        // Browser
        namespace(global, id, o);
    }
}

function isArray(o) {
    return typeOf(o) === 'array';
}

function isFunction(o) {
    return typeOf(o) === 'function';
}

function isNumber(o) {
    return typeof o === 'number';
}

function isNumeric(o) {
    // Thanks to jQuery.isNumeric
    return !isNaN(parseFloat(o)) && isFinite(o);
}

function isString(o) {
    return typeof o === 'string';
}

function log() {
    logMessage.create('log', YArray(arguments));
    if (console.log.apply) {
        console.log.apply(console, arguments);
    }
}

function mix(to, from) {
    if (arguments.length < 2) {
        from = to;
        to = this;
    }
    for (var i in from) {
        to[i] = from[i];
    }
    return to;
}

// :: {} -> string -> ({}|fn)? -> IO ({}|fn)
function namespace(host, id, obj) {
    //console.log('namespace:', arguments);
    var ids = id.split(namespace.delimiter),
        len = ids.length,
        target = host, 
        objId = ids[ids.length-1],
        last = host,
        name = id;
    for (var i = 0; i < len; i++) {
        name = ids[i];
        if (!target[name]) {
            //console.warn('The object `', name, '` hasn�t been declared yet! Shimming ...');
            target[name] = {};
        }
        last = target;
        target = target[name];
    }
    if (obj) {
        last[objId] = obj;
        target = last[objId];
    }
    return target;
}
namespace.delimiter = '.';

function nop() {}

function own(o, fn, ctx) {
    ctx = ctx || o;
    var i;
    if (isArray(o)) {
        o.forEach(fn, ctx);
    }
    else {
        for (i in o) {
            if (o.hasOwnProperty(i)) {
                fn.call(ctx, o[i], i, o);
            }
        }
        if (env.IS_DONTENUM_BUGGY) {
            for (j = DONT_ENUMS.length-1; j >=0; j--) {
                propName = DONT_ENUMS[j];
                if (o.hasOwnProperty(propName)) {
                    fn.call(ctx, o[propName], propName, o);
                }
            }
        }
    }
}

function raise(message) {
    error.apply(null, arguments);
    throw message;
}

function typeOf(o) {
    if (o === null) {
        return 'null';
    }
    if (o === undefined) {
        return UNDEFINED;
    }
    var t = toString.call(o)
        .replace(/\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1')
        .toLowerCase();
    
    if (t === 'object' && o.constructor === global.RegExp) {
        return 'regexp';
    }
    return t;
}

function warn() {
    logMessage.create('warn', YArray(arguments));
    if (console.warn.apply) {
        console.warn.apply(console, arguments);
    }
}

var YArray = define({
    constructor: function Array(arr) {
        return YArray.from((arguments.length > 1) ? arguments : arr);
    },
    base: NativeArray,
    statics: {
        from: function from(arr) {
            return NativeArray.prototype.slice.call(arr);
        },
        isArray: isArray,
        slice: function slice(arr, from, length) {
            return NativeArray.prototype.slice.call(arr, from, length);
        }
    }
});

var jeoBase = {
    Array:      YArray,
    define:     define,
    each:       each,
    error:      error,
    extend:     extend,
    info:       info,
    introduce:  introduce,
    isArray:    isArray,
    isFunction: isFunction,
    isNumber:   isNumber,
    isNumeric:  isNumeric,
    isString:   isString,
    log:        log,
    mix:        mix,
    //namespace:  namespace,
    nop:        nop,
    own:        own,
    raise:      raise,
    typeOf:     typeOf,
    warn:       warn
};

//////// JEO core

var internal = {};

(function () {

var config = {
    
};

var defaultModules = [];

mix(internal, {
    config: config,
    modules: {},
    syslog: []
});

var modules = internal.modules;

function JEO(config) {
    var J = NativeObject.create(JEO.prototype),
        userConfig = config;
    // ... apply configuration
    config = NativeObject.create(internal.config);
    if (userConfig) {
        config = mix(config, userConfig);
    }
    //J.config = config;
    return J;
}
JEO.prototype = {
    env: NativeObject.create(env),
    use: use
};

var BASE_REV = '$';
var HEAD_REV = 'HEAD';
var VERSION_SEP = '@';

function decompose(id) {
    //console.log('decompose:', id);
    var s = id.split(VERSION_SEP);
    return { name: s[0], version: s[1] || HEAD_REV };
}

function getModule(meta) {
    //console.log('getModule:', meta.name, meta.version);
    var mod = modules[meta.name](meta.version);
    //console.log('getModule:', mod);
    return mod;
}

function add(id, module) {
    var meta = decompose(id);
    name = meta.name;
    
    var get;
    //console.log(id);
    
    if (!modules[name]) {
        // Had to give the function a binding to a reference
        // due to a bug in IE7/8
        get = modules[name] = function getter(version) {
            //console.log('  ', version, get[HEAD_REV]);
            version = (version === HEAD_REV) ? get[HEAD_REV] : version;
            //console.log('  ', version);
            
            var module = get[version];
            if (!module) {
                raise('Dependency "' + name + '" not ready in version ' + version + '.');
            }
            return module;
        };
        get[HEAD_REV] = BASE_REV;
    }
 
    var ver = (meta.version === HEAD_REV) ? modules[name][HEAD_REV] : meta.version;
    modules[name][ver] = module;
    if (ver > modules[name].HEAD) {
        modules[name][HEAD_REV] = ver;
    }
}

function use(factory) {
    //console.log('use:', arguments);
    var args = YArray(arguments),
        len = args.length;
    //console.log('  args.length:', args.length);
    
    // Including base library 
    var baseImport = this;
        
    each(defaultModules, function (m) {
        //console.log(m);
        var mod = decompose(m);
        if (!modules[mod.name]) {
            raise('Module ' + mod.name + ' unknown.');
        }
        var imp = getModule(mod);
        
        //console.log('  ', module, imp);
        
        mix(baseImport, imp);
    });
    
    // Importing requested modules
    var imports = [baseImport];
    
    var meta;
    var imp;
    
    var i = 0;
    while (i < len-1) { // Last arg is factory!
        //console.log(args[i]);
        meta = decompose(args[i]);
        //console.log(meta);
        imp = getModule(meta);
        
        //console.log('  ', args[i], imp);
        
        imports.push(imp);
        i++;
    }
    
    factory = args[len-1];
    factory.apply(null, imports);
}

mix(JEO, {
    _: internal,
    add: add,
    use: function (factory) {
        // Using the default JEO instance
        return jeo.use.apply(jeo, arguments);
    },
    env: env
});

JEO.add('jeo-core@0.0.6', JEO);


//////// Base library

JEO.add('jeo@0.0.6', jeoBase);
defaultModules.push('jeo');

JEO.add('console', console);

// Default JEO instance
var jeo = JEO();


//////// Exposing library core

introduce('JEO', JEO);
//namespace(global, 'JEO', JEO);
if (env.isCommonJS) {
    module.exports = JEO;
}

}());

// /*???*/ Object.freeze(global);

}(typeof window !== 'undefined' ? window : global));

if (jQuery) {
    jQuery.noConflict();
    JEO.add('jquery@' + jQuery.fn.jquery, jQuery);
}

if (JEO.env.isAMD && require._ && require._.version) {
    JEO.add('define@' + require._.version, { define: define, require: require });
}


// Concepts

JEO.use(function (Y, undefined) {
'use strict';

var STORAGE = 'prototype';
    
function mergeConcepts(to, from) {
    //Y.log('mergeConcepts:', arguments);
    Y.each(from, function (_, i) {
        //Y.log('  merge:', i, '\t=', from[i], '->', to[i]);
        
        var fromReq = isRequired(from[i]),
            toReq = isRequired(to[i]),
            value = from[i],
            required = fromReq ? from[i] : (toReq ? to[i] : undefined);
        
        if (required) {
            value = toReq ? from[i] : to[i];
            if (!value) {
                value = required;
            }
        }
        else {
            value = from[i] ? from[i] : to[i];
            if (!value) {
                Y.raise('Impossible to overwrite property "' + i + '"');
            }
        }
        to[i] = value;
    });
    return to;
}

function Concept() {
    
}
Concept.prototype = {
    compose: function compose() {
        var c = Object.create(null),
            args = [this[STORAGE]].concat(Y.Array(arguments)),
            len = args.length;
        //Y.log('compose:', args);
        Y.each(args, function (val) {
            var arg = concept(val);
            mergeConcepts(c, arg[STORAGE]);
        });
        return concept(c);
    },
    resolve: function resolve(map) {
        var o = Object.create(null);
        Y.each(this[STORAGE], function (val, key) {
            var cmd = map[key];

            switch (Y.typeOf(cmd)) {
            case 'null':
                // Doing nothing to exclude the property
                break;
            case 'string':
                // Renaming property
                o[cmd] = val;
                break;
            default:
                // Just copying the property
                o[key] = val;
                break;
            }
        });
        return concept(o);
    }
};

function isConcept(o) {
    return (o instanceof Concept);
}

function concept(descr) {
    
    if (isConcept(descr)) {
        return descr;
    }
    
    var me = new Concept(),
        proto = Object.create(null),
        s;
  
    Y.each(descr, function (val, i) {
        s = i.split(':');
        if (s.length > 1) {
            proto[s[0]] = concept[s[1]](proto, s[0], val);
        }
        else {
            proto[i] = descr[i];
        }
    });
    
    me[STORAGE] = proto;
    return me;
}

function Required(name, value) {
    this.name = name;
    this.value = value;
}
Required.prototype = {
    toString: function () { 
        return '<required>';
    } 
};

function isRequired(p) {
    return p instanceof Required;
}

concept.required = function (concept, key, value) {
    //Y.log('required:', arguments);
    return new Required(key, value);
};

Y.define.concepts = function (Self, proto, concepts, allOpts) {
    //Y.info('create.concepts:', arguments);
    concepts = Y.isArray(concepts) ? concepts : [concepts];
    var c = concept(concepts[0]);
    if (concepts.length > 1) {
        c = c.compose.apply(c, Y.Array.slice(concepts, 1));
    }
    Y.each(c[STORAGE], function (val, key) {
        //Y.log('    ', key, ':', val);
        if (isRequired(val)) {
            Y.raise('The property "' + key + '" is required!');
        }
        proto[key] = val;
    });
}; 

JEO.add('concepts@0.0.3', concept());
 
});






























