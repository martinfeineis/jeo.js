// Base library of sharelost.de - The missing default environment for browsers.
// Credits to jQuery, PrototypeJS and Closure Library for the inspiration.
// The core library is optimized for source code optimizers like YUICompressor by providing
// shortcuts to commonly used property names
(function (global, console, Array, Date, Object, RegExp, String, XMLHttpRequest, undefined) {
'use strict';

// Common property names will be munged
// throughout the build process
var APPLY = 'apply',
    ARRAY = 'array',
    CALL = 'call',
    CONSTRUCTOR = 'constructor',
    CREATE = 'create',
    DEFINE = 'define',
    DEFINEPROPERTIES = 'defineProperties',
    DEFINEPROPERTY = 'defineProperty',
    EXTEND = 'extend',
    FOREACH = 'forEach',
    FREEZE = 'freeze',
    GETOWNPROPERTYDESCRIPTOR = 'getOwnPropertyDescriptor',
    HAS = 'hasOwnProperty',
    MIXINS = 'mixins',
    KEYS = 'keys',
    NOW = 'now',
    OBJECT = 'object',
    PROTO = 'prototype',
    TOSTRING = 'toString',
    TRIM = 'trim',
    UNDEFINED = 'undefined';

// Happy console world :)
console = (typeof console !== UNDEFINED) 
    ? console
    : { log: nop, info: nop, error: nop, warn: nop };
    
//// Detecting Asynchronous Module Definition compliant resource loaders
//var amdDefine = typeof global[DEFINE] !== UNDEFINED
//    ? global[DEFINE]
//    : null;

// Thanks to PrototypeJS
var DONT_ENUMS = [TOSTRING, 'toLocaleString', 'valueOf', HAS, 
        'isPrototypeOf', 'propertyIsEnumerable', CONSTRUCTOR],
       
    doc = global.document,
    env = {
        doc: doc, // Just for convenience; should not be necessary outside the core library itself
//        isAMD: !!(amdDefine && amdDefine['length'] && amdDefine['amd']),
//        isBrowser: (typeof window !== UNDEFINED),
//        isCommonJS: !!(typeof module !== UNDEFINED && module['exports'] && require),
    
        // Some versions of JScript fail to enumerate over properties, names of which 
        // correspond to non-enumerable properties in the prototype chain
        IS_DONTENUM_BUGGY: (function(){
            for (var p in { toString: 1 }) {
                // check actual property name, so that it works with augmented Object.prototype
                if (p === TOSTRING) {
                    return false;
                }
            }
            return true;
        }()),
        // Detects function decompilation capabilities
        SUPPORTS_DECOMPILATION: /xyz/.test(function(){xyz}),
        // Detects ES5 defineProperty capabilities
        SUPPORTS_DEFINEPROPERTY: (function () {
            var o = {};
            try {
                Object[DEFINEPROPERTY](o, 'one', { get: function () { return 1; } })
                return 1 === o.one;
            } catch(e) {
                return false;
            }
        }()),
        // IE8 implements Object.defineProperty and Object.getOwnPropertyDescriptor
        // only for DOM objects. These methods don't work on plain objects.
        // Hence, we need a more elaborate feature-test to see whether the
        // browser truly supports these methods:
        SUPPORTS_GETOWNPROPERTY: (function () {
            try {
                if (Object[GETOWNPROPERTYDESCRIPTOR]) {
                    var test = {x:0};
                    return !!Object[GETOWNPROPERTYDESCRIPTOR](test,'x');        
                }
            } 
            catch(e) {}
            return false;
        }()),
        // Detects whether or not strict mode is available
        SUPPORTS_STRICTMODE: (function () {
            'use strict';
            return !this;
        }()),
        win: global // Just for convenience; should not be necessary outside the core library itself
    },
    sandbox = {},
    slice = [].slice, // Native Array.prototype.slice
    toString = {}.toString; // Native Object.prototype.toString

// Detects whether a the property `name` is native
// to the given object `o`; only works properly in
// environments that support function decompilation
// :: {} -> string -> boolean
function isNative(o, name) {
    var ex = typeof o[name] !== UNDEFINED;
    return ex && isNative.fn(o[name]);
    //return false;
}
isNative.fn = env.SUPPORTS_DECOMPILATION 
  // :: ({} -> {}) -> boolean
  ? function (fn) {
        return /[\s*native\s+code\s*]/i.test(fn);
    }
  // :: () -> boolean
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        // and hope for the best
        // TODO: Maybe we should rely on our implementation?
        return true;
    };

if (!isNative(Array[PROTO], FOREACH)) {
    // :: ({} -> string|number -> {}) -> {}? -> ()
    Array[PROTO][FOREACH] = function (fn/*, ctx*/) {
        var ctx = arguments[1] || this,
            len = this.length;
        for (var i = 0; i < len; i++) {
            //console.log('each:', i, this[i]);
            fn[CALL](ctx, this[i], i, this);
        }
    };
}

if (!isNative(Object, CREATE)) {
    // :: {} -> {} -> {}
    Object[CREATE] = (function () {
        function F() {}
        return function create(proto, props) {
            F[PROTO] = proto;
            var o = new F;
            if (props) {
                Object[DEFINEPROPERTIES](o, props);
            }
            return o;
        };
    }());
}

if (!isNative(Date, NOW)) {
    // :: () -> number
    Date[NOW] = function now() {
        // Thanks to the Closure library
        // (unary+) calls getTime() on Date
        return +new Date; 
    };
}

if (!env.SUPPORTS_DEFINEPROPERTY) {
    // :: {} -> string -> { value: {} } -> ()
    Object[DEFINEPROPERTY] = function defineProperty(o, name, descr) {
        o[name] = descr.value;
    };
    // :: {} -> [{ value: {} }] -> ()
    Object[DEFINEPROPERTIES] = function defineProperties(object, descriptors) {
        own(descriptors, function (descriptor, property) {
            Object[DEFINEPROPERTY](object, property, descriptor);
        });
    };
}

if (!isNative(Object, FREEZE)) {
    // :: {} -> {}
    Object[FREEZE] = function (o) { return o; }; // Impossible in ES3
}

if (!env.SUPPORTS_GETOWNPROPERTY) {
    // :: {} -> string -> { value: {}, enumerable, writable, configurable: boolean }
    Object[GETOWNPROPERTYDESCRIPTOR] = function getOwnPropertyDescriptor(o, name) {
        return {
            value: o[name],
            enumerable: true,
            writable: true,
            configurable: true
        };
    };
}

if (!isNative(Object, KEYS)) {
    // :: {} -> [string]
    Object[KEYS] = function keys(o) {
        var result = [];
        own(o, function (_, name) {
            result.push(name);
        });
        return result;
    };
}

if (!isNative(String[PROTO], TRIM)) {
    // :: () -> string
    String[PROTO][TRIM] = function () {
        return this.replace(/^\s+|\s+$/, '');
    };
}

// :: string -> ((string...) -> ())
function wrapConsole(name) {
    var fn = console[name];
    return function () {
        try {
            fn[APPLY](console, arguments);
        }
        catch (e) {
            // Nothing ...
        }
    };
}

// X-Browser XmlHttpRequest
// :: new () -> {}
var Xhr = (function () {
    var Xhr,
        legacy = [
            'Msxml2.XMLHTTP.6.0',
            'Msxml2.XMLHTTP.3.0',
            'Microsoft.XMLHTTP',
            'Msxml2.XMLHTTP'
        ],
        len = legacy.length,
        i;

    // This is for all reasonable browsers
    if (XMLHttpRequest) {
        return XMLHttpRequest;
    } 

    // Testing for legacy http requests
    for (i = 0; i < len; i++) {
        Xhr = function () {
            var id = legacy[i];
            return new ActiveXObject(id);
        };
        try {
            new Xhr;
            return Xhr;
        } 
        catch (e) {} 
        finally {
            Xhr = null;
        }
    }

    info('XMLHttpRequest not available.');
}());

// Base library

// :: []|{ length: number } -> []
var YArray = function (arr) {
    return typeof arr !== UNDEFINED ? slice[CALL](arr) : [];
};

// :: { data?: {}, method?: string, sync: boolean, url: string } -> (string -> ())
function ajax(config, ok) {
    //console.log('net.get', arguments);
    if (isString(config)) {
        config = { url: config };
    }
    var xhr = new Xhr,
        url = config.url,
        async = !config.sync,
        data = config.data || null,
        method = config.method || 'GET',
        params = [];

    own(config.params || {}, function (val, key) {
        //log('param', key, val);
        params.push(escape(key) + '=' + escape(val));
    });
    if (params.length > 0) {
        url = url + '?' + params.join('&');
    }
    xhr.onreadystatechange = function (e) {
        //console.log('  xhr.readystatechange', xhr, arguments);
        if (xhr.readyState === 4) {
            switch (xhr.status) {
            case 200: // Net OK
                // Fall through
            case 0: // Local OK
                ok(xhr.responseText);
                break;
            default:
                raise('Request to "' + url + '" failed.');
            }
            xhr = null;
            return;
        }
    };
    try {
        //log(url);
        xhr.open(method.toUpperCase(), url, async);
        xhr.send(data);
        return true;
    }
    catch (e) {
        return false;
    }
}

// :: string -> boolean
function available(s) {
    return !!(global[s] || provide(s, null, 1));
}

// :: string -> { 
//      constructor: new (...) -> {},
//      extend: function,
//      mixins: [{}],
//      prototype: {},
//      statics: {}
// } -> function
function define(name, o) {
    var Type = o[CONSTRUCTOR] || function () {},
        Base = o[EXTEND] || Object,
        proto = o[PROTO] || {},
        statics = o.statics || {};
    Type[PROTO] = Object[CREATE](Base[PROTO]);
    extend(Type[PROTO], proto);
    each(o[MIXINS], function (m) {
        extend(Type[PROTO], m);
    });
    extend(Type, statics);
    return provide(name, Type);
}

// {} -> ({} -> string|number -> ()) -> {}? -> ()
function each(o, fn, ctx/*, internal justOwn*/) {
    ctx = ctx || o;
    var i, j, propName, 
        justOwn = arguments[3];
    
    //if (!o) {
    //    //Y.info('each', arguments);
    //    return;
    //}
    if (isArray(o)) {
        o[FOREACH](fn, ctx);
    }
    else {
        for (i in o) {
            if (!justOwn || o[HAS](i)) {
                fn[CALL](ctx, o[i], i, o);
            }
        }
        if (env.IS_DONTENUM_BUGGY) {
            for (j = DONT_ENUMS.length-1; j >= 0; j--) {
                propName = DONT_ENUMS[j];
                if (o[propName] && (!justOwn || o[HAS](j))) {
                    fn[CALL](ctx, o[propName], propName, o);
                }
            }
        }
    }
}

var error = wrapConsole('error');

// :: {0} -> {} -> {0}
function extend(to, from/*,internal level*/) {
    if (arguments.length < 2) {
        from = to;
        to = this;
    }
    //var level = arguments[2] || 0;
    //var tab = '';
    //for (var j = 0; j < level; j++) {
    //    tab += '  ';
    //}
    each(from, function (val, i) {
        //console.log(tab, 'extend: ', i, ': ', val, '=>', to[i]);
        var deep = false;
        switch (typeOf(val)) {
        case ARRAY:
            if (!to[i]) {
                to[i] = [];
            }
            deep = (val.length > 0); // No magic, just in case ...
            break;
        case OBJECT:
            if (!to[i]) {
                to[i] = {};
            }
            deep = true;
            break;
        }
        if (deep) {
            extend(to[i], val);//, level+1);
        }
        else {
            to[i] = val;
        }
    });
    //console.log(tab, 'ok:', to);
    return to;
}

var info = wrapConsole('info');

// :: {} -> boolean
function isArray(o) {
    return typeOf(o) === ARRAY;
}

// :: {} -> boolean
function isArrayLike(o) {
    switch (typeOf(o)) {
	case 'arguments': // Fall through
	case 'array': // Fall through
	case 'nodelist':
		return true;
	default:
		return false;
	}
}

// :: {} -> boolean
function isFunction(o) {
    return typeOf(o) === 'function';
}

// :: {} -> boolean
function isString(o) {
    return typeof o === 'string';
}

var log = wrapConsole('log');

// :: {0} -> {} -> {0}
function mix(to, from) {
    if (arguments.length < 2) {
        from = to;
        to = this;
    }
    for (var i in from) {
        to[i] = from[i];
    }
    return to;
}

// :: () -> ()
function nop() {}

// :: string -> {dom}
function one(id) {
    switch (id[0]) {
    case '#':
        return doc.getElementById(id.replace('#', ''));
    default:
        return doc.getElementsByTagName(id)[0];
    }
}

// :: {} -> ({} -> ()) -> {}? -> ()
function own(o, fn, ctx) {
    return each[CALL](this, o, fn, ctx, 1);
}

// :: string -> {0} -> {0}
function provide(name, o/*, internal justDetect*/) {
    o = o || {};
    var host = global,
        ls = name.split('.'),
        justDetect = arguments[2],
        failed = false,
        len = ls.length,
        i;
    for (i = 0; i < len-1; i++) {
        host = host[ls[i]];
    }
    if (justDetect) {
        //log('failed:', failed, ', host[ls[i]]:', !!host[ls[i]]);
        if (!host) {
            return false;
        }
        return failed || !!host[ls[i]];
    }
    if (failed) {
        raise('"' + name + '" is unknown.');
    }
    sandbox[name] = o;
    return (host[ls[i]] = o);
}

// :: string -> ()
function raise(message) {
    error[APPLY](console, arguments);
    throw message;
}

// :: {} -> string
function typeOf(o) {
    if (o === null) {
        return 'null';
    }
    if (o === undefined) {
        return UNDEFINED;
    }
    var t = toString[CALL](o)
        .replace(/\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1')
        .toLowerCase();
    
    return t;
}

// :: ({JEO} -> ()) -> ()
function use(fn) {
    var args = arguments,
        len = args.length,
        deps = slice[CALL](args, 0, len-1);

    deps[FOREACH](function (id) {
        if (!available(id)) {
            raise('Dependency "' + id + '" not met!');
        }
    });
    args[len-1][CALL](null, this);
}

var warn = wrapConsole('warn');

// Library core

// :: () -> {}
var Core = mix(function () {
    return Object[CREATE](Core[PROTO]);
}, {
    // :: () -> string
    toString: function toString() {
        return 'You are running JEO v' + this.version;
    },
    // :: ({JEO} -> ()) -> ()
    use: function () {
        core.use[APPLY](core, arguments);
    },
    // :: string
    version: '0.0.14'
});

// Exporting the base library as the Core's prototype
Core[PROTO] = Object[FREEZE]({
    Array: YArray,
    ajax: ajax,
    available: available,
    define: define,
    each: each,
    env: Object[FREEZE](env),
    error: error,
    extend: extend,
    info: info,
    isArray: isArray,
    isArrayLike: isArrayLike,
    isFunction: isFunction,
    isString: isString,
    log: log,
    mix: mix,
    nop: nop,
    one: one,
    own: own,
    provide: provide,
    raise: raise,
    typeOf: typeOf,
    use: use,
    warn: warn
});

var core = Core(); // Default core instance

provide('console', console); // Defining a console; IE doesn't define it outside webdev mode
provide('JEO', Core); // Exposing the core
provide('Y', core);

}(this, console, Array, Date, Object, RegExp, String, XMLHttpRequest));
// Handing in common language and host objects to easy enable minimization
































