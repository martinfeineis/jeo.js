JEO.use(function (Y) {
    
var CALL = 'call',
    CREATE = 'create',
    core = Y,
    log = Y.log,
    own = Y.own;
   
// :: string -> [] -> {} -> ()
function add(id, dependencies, factory) {
    //log('JEO.add:', arguments);
    var exports = Object[CREATE](core),
        returned = factory[CALL](null, exports);
    exports = returned ? returned : exports;
    log('JEO.add: returned =', returned, ', exports =', exports);
    own(exports, function (prop, name) {
        log('    ' + name + ' = ' + prop);
    });
}

Y.provide('JEO.add', add);
 
});

JEO.add('Blubb', ['Bla'], function (Y, Bla) {

    Y.mix({
        Blubb: function Blubb() {},
        plisch: 'platsch'
    });

    Y.BlubbManager = function BlubbManager() {
        
    };
});

JEO.add('Bla@0.0.1', [], function (Y) {
    return function Bla() {};
});
