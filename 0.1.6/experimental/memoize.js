JEO.add('experimental/memoize', function (Y) {
    
var memoize = Y.memoize = function (fn) {
    var cache = {};
    return function (arg) {
        var key = JSON.stringify(arguments),
            cached = cache[key];
        if (cached) {
            return cached;
        }
        return cache[key] = fn(arg);
    };
};    
    
});