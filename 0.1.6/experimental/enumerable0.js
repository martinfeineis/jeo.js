/*! JEO(experimental/enumerable) */
// * Work in progress!
// * For now we don't really need reactive extensions in the library core
// * Only makes sense if we decide to add a selector engine like sizzle

JEO.add('experimental/enumerable', ['experimental/lambda'], function (Y) {
'use strict';
Y.info('enumerable', Y.Array(arguments));
    
function makeLambda(f) {
    return Y.lambda(f);
}
    
function toEnumerable(s) {
    if (!Y.isString(s)) {
        return s;
    }
    var from = 0,
        count,
        by = 1;
    
    s.replace(/(\S+)\s*\.\.\s*(?:(\S*)(?:\s+by\s+(\S*))?)?/, function (m, f, c, b) {
        //Y.log('range:', arguments);
        from = parseFloat(f);
        if (c.length > 0) {
            count = parseFloat(c);
        }
        if (b.length > 0) {
            by = parseFloat(b);
        }
    });
    return Enumerable.range(from, count, by);
}

var Enumerable = Y.Enumerable = Y.define({
    constructor: function (src) {
        if (arguments.length < 1) {
            src = [];
        }
        else if (Y.isString(src)) {
            return toEnumerable(src);
        } 
        else if (Y.isFunction(src)) {
            this.enumerator = src;
        }
        else if (Y.isArray(src)) {
            this.enumerator = function () {
                var i = 0;
                return {
                    hasNext: function () {
                        return i < src.length;
                    },
                    next: function () {
                        var item = src[i];
                        i++;
                        return item;
                    }
                };
            };
        }
        else {
            Y.raise('No default enumerator for type "' + Y.typeOf(src) + '"');
        }
    },
    prototype: {
        aggregate: function aggregate(fn) {
            fn = makeLambda(fn);
            var it = this.enumerator(),
                val;
            if (it.hasNext()) {
                val = it.next();
            }
            while (it.hasNext()) {
                val = fn(val, it.next());
            }
            return val;
        },
        all: function all(predicate) {
            predicate = makeLambda(predicate);
            var it = this.enumerator(),
                index = 0,
                item;
            while (it.hasNext()) {
                item = it.next();
                if (!predicate(item, index)) {
                    return false;
                }
                index++;
            }
            return true;
        },
        any: function any(predicate) {
            predicate = makeLambda(predicate);
            var it = this.enumerator(),
                index = 0,
                item;
            while (it.hasNext()) {
                item = it.next();
                if (predicate(item, index)) {
                    return true;
                }
                index++;
            }
            return false;
        },
        concat: function concat(second) {
            return new Enumerable([this, toEnumerable(second)]).selectMany(function (x) {
                return x;
            });
        },
        contains: function contains(el, comparer) {
            comparer = makeLambda(comparer || '$x < $y ? -1 : ($x === $y) ? 0 : 1');
            var isContained = false;
            return this.any(function (x, index) {
                return comparer(x, el) === 0;
            });
        },
        count: function count(predicate) {
            predicate = makeLambda(predicate);
            var i = 0;
            function inc() {
                i++;
            }
            if (!Y.isFunction(predicate)) {
                this.each(inc);
                return i;
            }
            this.where(predicate).each(inc);
            return i;
        },
        distinct: function distinct() {
            var me = this,
                ls = [],
                names = {};
            this.toArray().forEach(function (val) {
                if (!names[val]) {
                    names[val] = true;
                    ls.push(val);
                }
            });
            return new Enumerable(ls);
        },
        each: function each(fn) {
            fn = makeLambda(fn);
            var it = this.enumerator(),
                index = 0;
            while (it.hasNext()) {
                fn(it.next(), index);
                index++;
            }
            return this;
        },
        elementAt: function elementAt(index) {
            var el, found = this.any(function(item) {
                el = item;
                var i = index;
                index--;
                return i == 0;
            });	
            return el;
        },
        first: function first(predicate) {
            predicate = makeLambda(predicate);
            if (!predicate) {
                predicate = makeLambda('true');
            }
            var el, found = this.any(function(item) {
                el = item;
                return true;
            });	
            return found ? el : null;
        },
        firstOrDefault: function firstOrDefault(value) {
            return this.first() || value;
        },
        last: function last(predicate) {
            return this.reverse().first(predicate);
        },
        lastOrDefault: function lastOrDefault(value) {
            return this.last() || value;
        },
        reverse: function reverse() {
            return new Enumerable(this.toArray().reverse());
        },
        select: function select(selector) {
            selector = makeLambda(selector);
            return this.selectMany(function (x) {
                return Enumerable.repeat(selector(x), 1);
            });
        },
        selectMany: function selectMany(selectCollection, selectResult) {
            if (!selectResult) {
                selectResult = function (x, y) { return y; };
            }
            var me = this;

            return new Enumerable(function () {
                var it = me.enumerator(),
                    index = 0,
                    item,
                    subit;

                var init = function () {
                    if (!item) {
                        item = it.next();
                    }
                    if (!subit) {
                        subit = selectCollection(item, index).enumerator();
                        index++;
                    }
                    init = Y.nop;
                };

                function advance() {
                    init();
                    if (!subit.hasNext() && it.hasNext()) {
                        item = it.next();
                        subit = selectCollection(item, index).enumerator();
                        index++
                        advance();
                    }
                }

                return {
                    hasNext: function () {
                        advance();
                        return subit.hasNext();
                    },
                    next: function () {
                        advance();
                        return selectResult(item, subit.next());
                    }
                };
            });
        },
        take: function take(count) {
            if (count < 1) {
                return Enumerable.empty();
            }
            return this.takeWhile(function (x, index) {
                //Y.log('take.takeWhile', x, index);
                return index < count;
            });
        },
        takeWhile: function takeWhile(predicate) {
            predicate = makeLambda(predicate);
            var me = this;
            return new Enumerable(function () {
                var it = me.enumerator(),
                    ok = true,
                    index = 0,
                    item = it.next();
                return {
                    hasNext: function () {
                        ok = ok && predicate(item, index);
                        return ok;
                    },
                    next: function () {
                        var oldItem = item;
                        item = it.next();
                        index++;
                        return oldItem;
                    }
                };
            });
        },
        toArray: function toArray() {
            var ls = [];
            this.each(function (x) {
                ls.push(x);
            });
            return ls;
        },
        where: function where(predicate) {
            predicate = makeLambda(predicate);
            var index = 0;
            return this.selectMany(function (x) {
                //Y.log('where.selectMany(', x, '), predicate:', predicate(x, index));
                return Enumerable.repeat(x, predicate(x, index) ? 1 : 0);
            }, function (x, y) {
                index++;
                return y;
            });
        },
        zip: function zip(b) {
            var a = this;
            b = toEnumerable(b);
            return new Enumerable(function () {
                var ait = a.enumerator(),
                    bit = b.enumerator(),
                    it = ait;
                return {
                    hasNext: function () {
                        return it.hasNext();
                    },
                    next: function () {
                        var val = it.next();
                        if (it === ait) {
                            if (bit.hasNext()) {
                                it = bit;
                            }
                        }
                        else {
                            if (ait.hasNext()) {
                                it = ait;
                            }
                        }
                        return val;
                    }
                };
            });
        }
    },
    statics: {
        empty: function () {
            return new Enumerable();
        },
        range: function (from, count, by) {
            //Y.log('range:', arguments);
            if (count < 1) {
                return Enumerable.empty();
            }
            if (!count) {
                // Supporting open ranges
                count = 1 << 30;
            }
            if (!by) {
                by = 1;
            }
            return new Enumerable(function () {
                var val = from,
                    i = 0;
                return {
                    hasNext: function () {
                        return i < count;
                    },
                    next: function () {
                        var x = val;
                        val += by;
                        i++;
                        return x;
                    }
                };
            });
        },
        repeat: function (el, amount) {
            //Y.log('  repeat:', amount, 'x', el);
            return new Enumerable(function () {
                var i = 0;
                return {
                    hasNext: function () {
                        return i < amount;
                    },
                    next: function () {
                        i++;
                        return el;
                    }
                };
            });
        }
    }
});

var list = Enumerable('1..5')
    //.range(1, 5)
    //.repeat(3, 5)
    .where('$x > 1 && $x < 5')
    .select('$x*$x')
    .each(function () {
        Y.log('Enum:', Y.Array(arguments));
    });

Y.log('all positive?', list.all(function (x) { return x >= 0; }));
Y.log('all > 5?', list.all(function (x) { return x > 5; }));
Y.log('any positive?', list.any(function (x) { return x >= 0; }));
Y.log('any > 5?', list.any(function (x) { return x > 5; }));
Y.log('count:', list.count());

var list2 = Enumerable
    .repeat(0, 3)
    .concat(list)
    .each(function () {
        Y.log('concat:', Y.Array(arguments));
    });
    
Y.log('aggregate', list2.aggregate('$a+$b'))

var list3 = Enumerable('0..5')
    .where('$x % 2 === 0')
    .select('$x*$x')
    .select('+1')
    .select('4+')
    .concat('1..5 by 2')
    .each(function (x) {
        Y.log('fun:', x);
    });

var list4 = Enumerable('1..10')
    .zip('6..5')
    .each(Y.log);

var list5 = new Enumerable(['one', 2, 3.0, 'four', 'five'])
    .where('typeof $x === "string"')
    .reverse()
    .take(2)
    .each(Y.log);

Y.log(list5.first());
    
var list6 = new Enumerable([1, 1, 2, 2, 3, 3])
    .distinct()
    .where('$x % 2 === 0')
    //.first('>2')
    .each(Y.log);
    
Y.log(list6.first('>2'));
    
var list7 = new Enumerable('1..4')
    //.last('x%2 == 0')
    .each(Y.log);
    
Y.log(list7.last('$x%2==0'));
    
Y.log('elementAt(1):', new Enumerable('1..3').elementAt(1));
    
var list8 = Enumerable('10.5 .. 3 by 2').each(Y.log);
var list9 = Enumerable('1..3').each(Y.log);
var list10 = Enumerable('1..').take(5).each(Y.log);
    
Y.log('contains(4)', list10.contains(4));
    
//var list11 = new Enumerable([{ val: 0 }, { val: 1}, { val: 2 }]);
//Y.log('contains(2)', list11.contains(2, 'x.val < y.val ? -1 : (x.val === y.val) ? 0 : 1'));

});























