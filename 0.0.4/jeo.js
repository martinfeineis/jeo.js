/*! JEO library (c) 2012 Martin Feineis */
// ES5 compatibility partly taken from traits.js (www.traitsjs.org)
(function (global, undefined) {
'use strict';
    
//////// Environment setup
    
var env = {
    isAMD: !!(typeof define !== 'undefined' && define.length && define.amd),
    isBrowser: (typeof window !== 'undefined'),
    isCommonJS: !!(typeof module !== 'undefined' && module.exports && require)
};

env.doc = global.document;
env.win = global;


// Console shim
var console = (typeof global.console !== 'undefined') ? global.console : {
    assert: nop,
    error: nop,
    info: nop,
    log: nop,
    trace: nop,
    warn: nop
};

// Taken from PrototypeJS
var DONT_ENUMS = ['toString', 'toLocaleString', 'valueOf',
   'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable', 'constructor'];

// Some versions of JScript fail to enumerate over properties, names of which 
// correspond to non-enumerable properties in the prototype chain
env.IS_DONTENUM_BUGGY = (function(){
    for (var p in { toString: 1 }) {
        // check actual property name, so that it works with augmented Object.prototype
        if (p === 'toString') return false;
    }
    return true;
}());

env.SUPPORTS_STRICTMODE = (function () {
    'use strict';
    return !this;
}());
env.SUPPORTS_DECOMPILATION = /z/.test(function(){z});
env.SUPPORTS_DEFINEPROPERTY = (function () {
    var o = {};
    try {
        Object.defineProperty(o, 'one', { get: function () { return 1; } })
        return o.one === 1;
    } catch(_) {
        return false;
    }
}());
// IE8 implements Object.defineProperty and Object.getOwnPropertyDescriptor
// only for DOM objects. These methods don't work on plain objects.
// Hence, we need a more elaborate feature-test to see whether the
// browser truly supports these methods:
env.SUPPORTS_GETOWNPROPERTY = (function () {
    try {
        if (Object.getOwnPropertyDescriptor) {
            var test = { x: 0 };
            return !!Object.getOwnPropertyDescriptor(test, 'x');        
        }
    } catch(e) {}
    return false;
}());


var NativeArray = global.Array;

//////// ES5 compatibility polyfills

// :: {} -> "" -> bool
function isNative(o, name) {
    var ex = typeof o[name] !== 'undefined';
    return ex && isNative.fn(o[name]);
}
isNative.fn = env.SUPPORTS_DECOMPILATION ? function (fn) {
        return /\s*[\s*native\s+code\s*]\s*/i.test(fn);
    }
  : function () {
        // We don't have any means of knowing whether
        // or not the thingy is native so we assume it is
        return true;
    };
    
if (!isNative(NativeArray.prototype, 'forEach')) {
    NativeArray.prototype.forEach = function (fn/*, ctx*/) {
        ctx = arguments[1] || this;
        var len = this.length;
        for (var i = 0; i < len; i++) {
            //console.log('each:', i, this[i]);
            fn.call(ctx, this[i], i, ctx);
        }
    };
}
    
if (!isNative(Function.prototype, 'bind')) {
    Function.prototype.bind = function bind(ctx) {
        var fn = this;
        return function () {
            fn.apply(ctx, arguments);
        };
    };
}

if (!isNative(Object, 'create')) {
    Object.create = (function () {
        function F() {}
        return function create(proto, props) {
            F.prototype = proto;
            var o = new F();
            if (props) {
                Object.defineProperties(o, props);
            }
            return o;
        };
    }());
}

if (!isNative(Date, 'now')) {
    Date.now = function () {
        return (new Date).getTime();
    };
}

if (!env.SUPPORTS_DEFINEPROPERTY) {
    Object.defineProperty = function defineProperty(o, name, descr) {
        o[name] = descr.value;
    };
}

if (!env.SUPPORTS_DEFINEPROPERTY) {
    Object.defineProperties = function defineProperties(object, descriptors) {
        own(descriptors, function (descriptor, property) {
            Object.defineProperty(object, property, descriptor);
        });
    };
}

if (!isNative(Object, 'freeze')) {
    Object.freeze = function (o) { return o; }; // Impossible in ES3
}

if (!env.SUPPORTS_GETOWNPROPERTY) {
    Object.getOwnPropertyDescriptor = function getOwnPropertyDescriptor(o, name) {
        return {
            value: o[name],
            enumerable: true,
            writable: true,
            configurable: true
        };
    };
}

if (!isNative(Object, 'keys')) {
    Object.keys = function keys(o) {
        var result = [];
        own(o, function (_, name) {
            result.push(name);
        });
        return result;
    };
}

if (!isNative(String.prototype, 'trim')) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/, '');
    };
}

var logMessage = {
    create: function (type, message) {
        var o = Object.create(logMessage);
        o.message = message;
        o.time = Date.now();
        o.type = type;
        internal.syslog.push(o);
        return o;
    },
    toString: function toString() {
        return this.type + ': ' + this.message;
    }
};

//////// Base functionality

var SELF_NAME = 'constructor',
    BASE_NAME = 'base';

function define(name, opt) {
    var asNamespace = true;
    if (arguments.length < 2) {
        opt = name;
        name = 'noname';
        asNamespace = false;
    }
    var Self = opt[SELF_NAME] || function () {},
        proto = Object.create(isFunction(opt[BASE_NAME]) ? opt[BASE_NAME].prototype : null),
        plugins = define.plugins;
        
    own(opt, function (val, key) {
        var plugin = plugins[key];
        if (plugin) {
            plugin(Self, proto, val, opt);
        }
    });
    proto[SELF_NAME] = Self;
    Self.prototype = proto;
    
    if (asNamespace) {
        namespace(global, name, Self);
    }
    return Self;
}
define.plugins = mix(Object.create(null), {
    alias: function (Self, proto, cmd, all) {
        var ls = isArray(cmd) ? cmd : [cmd];
        each(ls, function (val) {
            introduce(val, Self);
        });
    },
    members: function (Self, proto, cmd, all) {
        mix(proto, cmd);
    },
    statics: function (Self, proto, cmd, all) {
        mix(Self, cmd);
    }
});
define.plugins[BASE_NAME] = nop; // Already included
define.plugins[SELF_NAME] = nop; // Already included

function each(o, fn, ctx) {
    ctx = ctx || o;
    var i, j, propName;
    if (isArray(o)) {
        o.forEach(fn, ctx);
    }
    else {
        for (i in o) {
            fn.call(ctx, o[i], i, o);
        }
        if (env.IS_DONTENUM_BUGGY) {
            for (j = DONT_ENUMS.length-1; j >=0; j--) {
                propName = DONT_ENUMS[j];
                if (o[propName]) {
                    fn.call(ctx, o[propName], propName, o);
                }
            }
        }
    }
}

function error() {
    logMessage.create('error', YArray(arguments));
    if (console.error.apply) {
        console.error.apply(console, arguments);
    }
}

function info() {
    logMessage.create('info', YArray(arguments));
    if (console.info.apply) {
        console.info.apply(console, arguments);
    }
}
    
function introduce(id, o) {

    if (env.isAMD) {
        // AMD compliant define
        define(id.toLowerCase(), [], function () { return o; });
    }
    if (env.isCommonJS) {
        // CommonJS environment
        module.exports = o;
    }
    if (env.isBrowser) {
        // Browser
        global[id] = o;
    }
}

function isArray(o) {
    return typeOf(o) === 'array';
}

function isFunction(o) {
    return typeOf(o) === 'function';
}

function isNumber(o) {
    return typeOf(o) === 'number';
}

function isString(o) {
    return typeof o === 'string';
}

var MIN_TIMER_MS = 15;

function later(fn, time, recurring) {
    time = time || MIN_TIMER_MS;
    if (time < MIN_TIMER_MS) {
        time = MIN_TIMER_MS;
    }
    var binding;
    if (recurring) {
        binding = global.setInterval(fn, time);
    } 
    else {
        binding = global.setTimeout(fn, time);
    }
    return {
		clear: function () {
			if (recurring) {
				global.clearInterval(binding);
			}
			else {
				global.clearTimeout(binding);
			}
		}
	};
}

function log() {
    logMessage.create('log', YArray(arguments));
    if (console.log.apply) {
        console.log.apply(console, arguments);
    }
}

function mix(to, from) {
    if (from === undefined) { // Only if it's REALLY undefined :)
        from = to;
        to = this;
    }
    for (var i in from) {
        to[i] = from[i];
    }
    return to;
}

// :: {} -> string -> ({}|fn)? -> IO ({}|fn)
function namespace(host, id, obj) {
    //console.log('namespace:', arguments);
    var ids = id.split(namespace.delimiter),
        len = ids.length,
        target = host, 
        objId = ids[ids.length-1],
        last = host,
        name = id;
    for (var i = 0; i < len; i++) {
        name = ids[i];
        if (!target[name]) {
            //console.warn('The object `', name, '` hasn�t been declared yet! Shimming ...');
            target[name] = {};
        }
        last = target;
        target = target[name];
    }
    if (obj) {
        last[objId] = obj;
        target = last[objId];
    }
    return target;
}
namespace.delimiter = '.';

function nop() {}

function own(o, fn, ctx) {
    ctx = ctx || o;
    var i;
    if (isArray(o)) {
        o.forEach(fn, ctx);
    }
    else {
        for (i in o) {
            if (o.hasOwnProperty(i)) {
                fn.call(ctx, o[i], i, o);
            }
        }
        if (env.IS_DONTENUM_BUGGY) {
            for (j = DONT_ENUMS.length-1; j >=0; j--) {
                propName = DONT_ENUMS[j];
                if (o.hasOwnProperty(propName)) {
                    fn.call(ctx, o[propName], propName, o);
                }
            }
        }
    }
}

function raise(message) {
    error.apply(null, arguments);
    throw message;
}

var toString = {}.toString;

function typeOf(o) {
    if (o === null) {
        return 'null';
    }
    if (o === undefined) {
        return 'undefined';
    }
    var t = toString.call(o)
        .replace(/\s*\[\s*\w+\s+([^\]\s\n]+)\s*\]\s*/im, '$1')
        .toLowerCase();
    
    if (t === 'object' && o.constructor === RegExp) {
        return 'regexp';
    }
    return t;
}

function warn() {
    logMessage.create('warn', YArray(arguments));
    if (console.warn.apply) {
        console.warn.apply(console, arguments);
    }
}

var YArray = define({
    constructor: function Array(arr) {
        return YArray.from((arguments.length > 1) ? arguments : arr);
    },
    statics: {
        from: function from(arr) {
            return NativeArray.prototype.slice.call(arr);
        },
        isArray: isArray,
        slice: function slice(arr, from, length) {
            return NativeArray.prototype.slice.call(arr, from, length);
        }
    }
});


//////// JEO core

var internal = {};

(function () {

var config = {
    
};

var defaultModules = [];

var modules = {};

mix(internal, {
    config: config,
    modules: modules,
    syslog: []
});

function JEO(config) {
    var J = Object.create(JEO.prototype),
        userConfig = config;
    // ... apply configuration
    config = Object.create(internal.config);
    if (userConfig) {
        config = mix(config, userConfig);
    }
    //J.config = config;
    return J;
}
JEO.prototype = {
    env: Object.create(env),
    use: use
};

var BASE_REV = '$';
var HEAD_REV = 'HEAD';
var VERSION_SEP = '@';

function decompose(id) {
    //console.log('decompose:', id);
    var s = id.split(VERSION_SEP);
    return { name: s[0], version: s[1] || HEAD_REV };
}

function getModule(meta) {
    //console.log('getModule:', meta.name, meta.version);
    var mod = modules[meta.name](meta.version);
    //console.log('getModule:', mod);
    return mod;
}

function add(id, module) {
    var meta = decompose(id);
    name = meta.name;
    
    var get;
    //console.log(id);
    
    if (!modules[name]) {
        // Had to give the function a binding to a reference
        // due to a bug in IE7/8
        get = modules[name] = function getter(version) {
            //console.log('  ', version, get[HEAD_REV]);
            version = (version === HEAD_REV) ? get[HEAD_REV] : version;
            //console.log('  ', version);
            
            var module = get[version];
            if (!module) {
                raise('Module ' + name + ' not ready in version ' + version + '.');
            }
            return module;
        };
        get[HEAD_REV] = BASE_REV;
    }
 
    var ver = (meta.version === HEAD_REV) ? modules[name][HEAD_REV] : meta.version;
    modules[name][ver] = module;
    if (ver > modules[name].HEAD) {
        modules[name][HEAD_REV] = ver;
    }
}

function use(factory) {
    //console.log('use:', arguments);
    var args = YArray(arguments),
        len = args.length;
    //console.log('  args.length:', args.length);
    
    // Including base library 
    var baseImport = this;
        
    each(defaultModules, function (m) {
        //console.log(m);
        var mod = decompose(m);
        if (!modules[mod.name]) {
            raise('Module ' + mod.name + ' unknown.');
        }
        var imp = getModule(mod);
        
        //console.log('  ', module, imp);
        
        mix(baseImport, imp);
    });
    
    // Importing requested modules
    var imports = [baseImport];
    
    var meta;
    var imp;
    
    var i = 0;
    while (i < len-1) { // Last arg is factory!
        //console.log(args[i]);
        meta = decompose(args[i]);
        //console.log(meta);
        imp = getModule(meta);
        
        //console.log('  ', args[i], imp);
        
        imports.push(imp);
        i++;
    }
    
    factory = args[len-1];
    factory.apply(null, imports);
}

mix(JEO, {
    _: internal,
    add: add,
    use: function (factory) {
        // Using the default JEO instance
        return jeo.use.apply(jeo, arguments);
    },
    env: env
});

JEO.add('jeo-core@0.0.4', JEO);


//////// Base library

JEO.add('jeo@0.0.4', {
    Array: YArray,
    define: define,
    each: each,
    error: error,
    info: info,
    introduce: introduce,
    isArray: isArray,
    isFunction: isFunction,
    isNumber: isNumber,
    isString: isString,
    later: later,
    log: log,
    mix: mix,
    namespace: namespace,
    nop: nop,
    own: own,
    raise: raise,
    typeOf: typeOf,
    warn: warn
});
defaultModules.push('jeo');

JEO.add('console', console);

// Default JEO instance
var jeo = JEO();


//////// Exposing library core

introduce('JEO', JEO);

}());

// /*???*/ Object.freeze(global);

}(typeof window !== 'undefined' ? window : global));

(function () {

var jQuery = JEO.env.win.jQuery;
if (jQuery) {
    jQuery.noConflict();
    JEO.add('jquery@' + jQuery.fn.jquery, jQuery);
}

}());
























