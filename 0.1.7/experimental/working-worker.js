importScripts('../jeo-dev.js');

JEO.use(function (Y) {

var worker = Y.env.worker;
if (worker) {
    worker.onmessage = function (ev) {
        //Y.log('worker<-', ev.data);
        worker.postMessage(ev.data);
    };
}

});
