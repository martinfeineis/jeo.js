echo "Building JEO production version ..."
cp ./jeo-dev.js ./tmp.js

echo "* Stripping debugging info"
cat ./tmp.js | awk '$0 !~ /;;;/' > undebugged.js

echo "* Munging property names and string literals"
node munger.js undebugged.js tmp.js

echo "* Minifying"
java -jar ../../yuicompressor/2.4.7/build/yuicompressor-2.4.7.jar -o ./jeo.js ./tmp.js

echo "* Cleaning up"
rm ./tmp.js
rm ./undebugged.js
