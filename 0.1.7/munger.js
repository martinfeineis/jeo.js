var fs = require('fs');

function usage() {
    console.log('usage: node munge.js infile outfile');
    process.exit(1);
}

if (process.argv.length !== 4) {
    usage();
}

fs.readFile(process.argv[2], function (err, f) {
    if (err) usage();
    var result = munge(f.toString());
    //console.log('#####################\n' + result);
    fs.writeFile(process.argv[3], result);
});

function fromTo(a, b) {
    return function (text) {
        var s = text,
            tmp = s,
            sthDone = true,
            len,
            p1 = 0,
            p2 = 0;
        while (sthDone) {
            sthDone = false;
            s.replace(a, function (m, pos) {
                //console.log(arguments);
                //console.log('from: ' + pos);
                //console.log('from: ' + s[pos]);
                len = m.length;
                p1 = pos;
                p2 = pos;
                s.substring(p1+len).replace(b, function (m2, pos2) {
                    p2 = len + pos + pos2 + m2.length - 1;
                    //console.log('to: ' + a);
                    //console.log('to: ' + s[p2]);

                    sthDone = true;
                });
            });
            //console.log(s.substr(p1, p2));
            if (sthDone) {
                //console.log(s[p1] + s[p1+1] + s[p1+2] + '...' + s[p2-2] + s[p2-1] + s[p2]);
                tmp = s.substring(0, p1) + s.substring(p2+1);
            }
            s = tmp;
        }
        //console.log('fromTo ' + a + ' - ' + b + ':' + s);
        return s;
    };
}

function removeLineComments(text) {
    return text.replace(/[^\\]\/\/[^\n]*/gm, '');
}

var removeBlockComments = fromTo(/\/\*/, /\*\//);

var removeRegExps = fromTo(/\//, /[^\\]\/[gmi]{0,3}/);

function removeStringLiterals(text) {
    var s = text.replace(/\\"/g, '');
    s = s.replace(/"[^"]*"/g, '""');
    return s;
}

function removeSingleQuoteStringLiterals(text) {
    var s = text.replace(/\\'/g, '');
    s = s.replace(/'[^']*'/g, "''");
    return s;
}

function munge(text) {
    
    // Munging comments doesn't make a difference
    var s = text;

    //console.log('xxx line comments');
    s = removeLineComments(s);

    //console.log('xxx block comments');
    s = removeBlockComments(s);

    var textWithoutComments = s;

    // Munging regexes is a baaad idea
    //console.log('xxx regex');
    s = removeRegExps(s);

    //// Munging string literals is a baaad idea
    //console.log('xxx \'...\'');
    s = removeSingleQuoteStringLiterals(s);

    //console.log('xxx "..."');
    s = removeStringLiterals(s);

    //console.log('#######################' + s);

    /* some block
    comment being done
    */
    
    var props = {};
    props.__proto__ = null;

    s.replace(/\.\s*([\w_]+)/g, function (m, name) {
        //console.log('  ', name);
        if (typeof props[name] === 'undefined') {
            props[name] = 0;
        }
        props[name]++;
    });

    var strLiterals = {};
    strLiterals.__proto__ = null;

    textWithoutComments.replace(/\'[\w_$]+\'/g, function (lit) {
        lit = lit.substring(1, lit.length-1);
        //console.log('literal:', lit);
        if (typeof strLiterals[lit] === 'undefined') {
            strLiterals[lit] = 0;
        }
        strLiterals[lit]++;
    });
    textWithoutComments.replace(/\"[\w_$]+\"/g, function (lit) {
        lit = lit.substring(1, lit.length-1);
        //console.log('literal:', lit);
        if (typeof strLiterals[lit] === 'undefined') {
            strLiterals[lit] = 0;
        }
        strLiterals[lit]++;
    });

    //console.log('# Properties ##################');
   
    ////console.log('=', props);
    //for (var i in props) {
    //    console.log(i, ':', props[i]);
    //}

    //console.log('# Literals ##################');

    //for (var i in strLiterals) {
    //    console.log(i, ':', strLiterals[i]);
    //}

    var aliases = {},
        fullDef = 'var ',
        defCount = 0,
        targetPropLength = 1,
        propGen = '_g$',
        gen,
        propAccessLen,
        newDef,
        allChars,
        strAmount,
        strLitsToReplace = {},
        mungeStrLit,
        amount,
        defLen,
        len;

    for (var p in props) {
        mungeStrLit = false;
        len = p.length; // Length of the property name
        amount = props[p]; // Amount of property accesses
        allChars = len*amount; // Overall amount of chars related to this property

        // We're injecting the stuff into the first var statement
        // so we don't have to account for this 4 chars
        //defLen = (defCount === 0 ? 'var '.length : 0);
        defLen = len + '=,'.length; // Necessary chars for var def
        defLen += ('"' + len + '"').length; // Actual string def

        // Total length of all accesses to the
        // current property
        propAccessLen = amount * ('[]'.length + targetPropLength);

        // String literals containing exactly our property name 
        if (p in strLiterals) {
            strAmount = strLiterals[p];

            // Covering more chars of the input
            allChars += strAmount * p.length;

            // Replacing the literals with the generated var
            propAccessLen += strAmount * targetPropLength;

            //strLitsToReplace["'"+p+"'"] = 1;
            mungeStrLit = true;
        }

        //console.log('.' + p + ' total:', allChars, ', munged:', (defLen+propAccessLen));

        // If we can save a few chars we munge the property
        // name into our generated var
        if (defLen + propAccessLen < allChars) {
            gen = propGen + p + defCount;
            newDef = gen + '="' + p + '",';
            //console.log('  ->' + newDef);
            aliases[p] = gen;
            if (mungeStrLit) {
                strLitsToReplace[p] = aliases[p];
            }

            fullDef += newDef;
            defCount++;
            continue;
        }
        //console.log('  x');
    }

    //console.log('Generated defs:', fullDef);

    if (defCount > 0) {
        //console.log('  there are munged defs so we inject them ...');
        for (var i in aliases) {
            text = text.replace(
                new RegExp('\\.\\s*' + i + '', 'g'), 
                '[' + aliases[i] + ']'
            );
        }
        ['true', 'false'].forEach(function (literal) {
            var repl = literal.toUpperCase();
            text = text.replace(new RegExp('\\b' + literal + '\\b', 'g'), repl);
            fullDef += repl + '=' + literal + ',';
        });
        for (var i in strLitsToReplace) {
            //console.log('lit.replace', i);
            // We're replacing both "..." and '...' string literal styles
            text = text.replace(new RegExp("'" + i + "'", 'g'), strLitsToReplace[i]);
            text = text.replace(new RegExp('"' + i + '"', 'g'), strLitsToReplace[i]);
        }
        //console.log('vars:', fullDef);
        text = text.replace(/\bvar\b/, fullDef);
    }

    return text;
}

















